'''Executed when run with `python -m turkbot`'''
from turkbot import TurkBot
from turkbot.cli import cli_main

args = cli_main()
TurkBot(args).run()
