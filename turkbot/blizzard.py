"""Utilities for interacting with the Blizzard API"""
import json
from time import time
from typing import Any, Optional

import aiohttp

from turkbot.internet import fetch
from turkbot.settings import get_config

# Turn in-game representation into what the Blizzard API wants (mainly spaces
# to hyphens)
REALM_REPLACEMENTS = {
    "aeriepeak": "aerie-peak",
    "blade'sedge": "blade's-edge",
    "echoisles": "echo-isles",
    "blackwinglair": "blackwing-lair",
    "darkiron": "dark-iron",
    "shatteredhalls": "shattered-halls",
    "blackwaterraiders": "blackwater-raiders",
    "cenarioncircle": "cenarion-circle",
    "silverhand": "silver-hand",
    "thescryers": "the-scryers",
    "wyrmrestaccord": "wyrmrest-accord",
    "theventureco": "the-venture-co",
    "shadowcouncil": "shadow-council",
    "boreantundra": "borean-tundra",
    "grizzlyhills": "grizzly-hills",
    "khazmodan": "khaz-modan",
    "kultiras": "kul-tiras",
    "bloodfurnace": "blood-furnace",
    "burninglegion": "burning-legion",
    "laughingskull": "laughing-skull",
    "theunderbog": "the-underbog",
    "kirintor": "kirin-tor",
    "moonguard": "moon-guard",
    "scarletcrusade": "scarlet-crusade",
    "sistersofelune": "sisters-of-elune",
    "thoriumbrotherhood": "thorium-brotherhood",
    "emeralddream": "emerald-dream",
    "twistingnether": "twisting-nether",
    "area52": "area-52",
    "altarofstorms": "altar-of-storms",
    "alteracmountains": "alterac-mountains",
    "blackdragonflight": "black-dragonflight",
    "bleedinghollow": "bleeding-hollow",
    "burningblade": "burning-blade",
    "demonsoul": "demon-soul",
    "lightning'sblade": "lightning's-blade",
    "shatteredhand": "shattered-hand",
    "theforgottencoast": "the-forgotten-coast",
    "argentdawn": "argent-dawn",
    "earthenring": "earthen-ring",
    "steamwheedlecartel": "steamwheedle-cartel",
}

# For some reason, while there are name fields for professions returned from
# the character profession API, the fields are usually blank. Another API call
# is needed to get the profession name then. This cache stores profession tier
# names for everything except Zandalari / Kul Tiran, since that's given.
# This maps profession tier IDs to their names.
PROFESSSION_NAME_CACHE: dict[int, str] = {}

class BlizzardAPICaller:
    """Caller for the Blizzard API"""
    __slots__ = ("access_token", "client_id", "client_secret", "expires_at")

    def __init__(self):
        self.access_token = ""
        self.client_id = get_config("BlizzardClientID")
        self.client_secret = get_config("BlizzardClientSecret")
        self.expires_at = 0 # as time() + "expires_in" from response

    async def check_token(self):
        """Refreshes the access token if it has expired."""
        if time() < self.expires_at: # token has not expired yet
            return

        # see https://develop.battle.net/documentation/guides/using-oauth/
        #     client-credentials-flow
        url = "https://us.battle.net/oauth/token"
        auth = aiohttp.BasicAuth(self.client_id, self.client_secret)
        form = {"grant_type": "client_credentials"}
        async with aiohttp.ClientSession() as session:
            response = await session.post(url, data=form, auth=auth)
        data = await response.json()

        self.access_token = data["access_token"]
        self.expires_at = time() + data["expires_in"]

    async def get_wow_profile(self, name: str,
                              given_realm: str) -> "WoWCharacterResponse":
        """Returns a WoWCharacterResponse for the character name-given_realm."""
        await self.check_token()

        name = name.lower()
        realm = REALM_REPLACEMENTS.get(given_realm.lower(), given_realm).lower()

        base = ('https://us.api.blizzard.com/profile/wow/character/{0}/{1}'
                '{extra}?namespace=profile-us&locale=en_US&access_token={2}'
                .format(realm, name, self.access_token, extra='{extra}'))

        char_url = base.format(extra='')
        prof_url = base.format(extra='/professions')
        media_url = base.format(extra='/character-media')

        async with aiohttp.ClientSession() as session:
            char_data = json.loads(await fetch(session, char_url))
            prof_data = json.loads(await fetch(session, prof_url))
            media_data = json.loads(await fetch(session, media_url))

        return WoWCharacterResponse(char_data, prof_data, media_data,
                                    self.access_token)

    async def get_token_price(self) -> int:
        '''Returns the current WoW Token price, in gold,'''
        await self.check_token()

        url = ('https://us.api.blizzard.com/data/wow/token/index'
               '?namespace=dynamic-us&locale=en_US&access_token={}'
               .format(self.access_token))

        async with aiohttp.ClientSession() as session:
            response = await fetch(session, url)

        return json.loads(response)['price'] // 10000 # copper -> gold

class WoWCharacterResponse:
    """Represents a WoW character from a result from the Blizzard API"""
    class_colors = {
        'Warrior': 0xC79C6E, # Warrior
        'Paladin': 0xF58CBA, # Paladin
        'Hunter': 0xABD473, # Hunter
        'Rogue': 0xFFF569, # Rogue
        'Priest': 0xFFFFFF, # Priest
        'Death Knight': 0xC41F3B, # Death Knight
        'Shaman': 0x0070DE, # Shaman
        'Mage': 0x69CCF0, # Mage
        'Warlock': 0x9482C9, # Warlock
        'Monk': 0x00FF96, # Monk
        'Druid': 0xFF7D0A, # Druid
        'Demon Hunter': 0xA330C9, # Demon Hunter
    }

    def __init__(self, char_data, prof_data, media_data, token: str):
        self.name: str = char_data['name']
        self.realm: str = char_data['realm']['name']
        self.class_: str = char_data['character_class']['name']
        self.race: str = char_data['race']['name']
        self.gender: str = char_data['gender']['name']
        self.level: int = char_data['level']
        self.achievement_points: int = char_data['achievement_points']
        self.thumbnail: str = media_data['avatar_url']
        self.faction: str = char_data['faction']['name']
        self.average_item_level: int = char_data['average_item_level']
        self.equipped_item_level: int = char_data['equipped_item_level']

        self.guild: Optional[str]
        try:
            self.guild = char_data['guild']['name']
        except KeyError:
            self.guild = None

        self.professions: list['Profession'] = []
        for prof in prof_data['primaries']:
            self.professions.append(Profession(prof, token))
        for prof in prof_data['secondaries']:
            self.professions.append(Profession(prof, token))

    def get_guild_name_with_brackets(self) -> str:
        """Returns a string with the character's guild name in brackets.

        If the character is not in a guild, an empty string is returned.
        """
        if self.guild is not None:
            return '<{0}>'.format(self.guild)

        return ''

    def get_class_color(self) -> int:
        """Returns an integer with the character's class color."""
        return self.class_colors.get(self.class_, 0x000000)

class Profession:
    '''A profession's name and ranks for a World of Warcraft character.'''
    def __init__(self, data: dict[str, Any], token: str):
        # data is a single object from "primaries" or "secondaries"
        self.id_: int = data['profession']['id']
        self.name: str = data['profession']['name']
        # no tiers for Archaeology
        self.tiers = [ProfessionTier(self.id_, tier, token)
                      for tier in data.get('tiers', [])]

        # only for Archaeology
        self.arch_skill_points: Optional[int] = None
        self.arch_max_skill_points: Optional[int] = None
        if self.name == 'Archaeology':
            self.arch_skill_points = data['skill_points']
            self.arch_max_skill_points = data['max_skill_points']

    def __str__(self):
        '''Get a list of profession tiers formatted for display.'''
        display_string: str

        if self.name == 'Archaeology':
            display_string = 'All: {}/{}'.format(self.arch_skill_points,
                                                 self.arch_max_skill_points)
        else:
            display_values: list[str] = []
            for tier in sorted(self.tiers, key=ProfessionTier.get_sort_value):
                display_values.append(str(tier))
            display_string = '\n'.join(display_values)

        return display_string

class ProfessionTier:
    '''A single tier for a World of Warcraft profession.'''
    base = ('https://us.api.blizzard.com/data/wow/profession/{}/skill-tier/{}'
            '?namespace=static-us&locale=en_US&access_token={}')

    # Used for display. Oldest comes first.
    sort_order = {
        'Outland': 1,
        'Northrend': 2,
        'Cataclysm': 3,
        'Pandaria': 4,
        'Draenor': 5,
        'Legion': 6,
        'Kul Tiran': 7,
        'Zandalari': 7,
    }

    def __init__(self, prof_id: int, data: dict[str, Any], token: str):
        self.prof_id: int = prof_id
        self.tier_id: int = data['tier']['id']
        self.skill_points: int = data['skill_points']
        self.max_skill_points: int = data['max_skill_points']
        self.token: str = token
        self._name: str = data['tier']['name']

    async def retrieve_name(self):
        '''Retrieve a profession's name.

        get_name() can't be used until this method is called. See the comment
        for PROFESSION_NAME_CACHE in this module.
        '''
        if self._name:
            return

        if self.tier_id in PROFESSSION_NAME_CACHE:
            self._name = PROFESSSION_NAME_CACHE[self.tier_id]
        else:
            async with aiohttp.ClientSession() as session:
                url = self.base.format(self.prof_id, self.tier_id, self.token)
                tier_name = json.loads(await fetch(session, url))['name']
                self._name = tier_name
                PROFESSSION_NAME_CACHE[self.tier_id] = tier_name

    @property
    def name(self):
        '''Get the profession tier's name.

        This wrapper forces the use of retrieve_name().
        '''
        if not self._name:
            raise RuntimeError('retrieve_name() was not called')
        return self._name

    def get_sort_value(self) -> int:
        '''Get the sorting order for displaying this tier.

        Oldest expansion comes first.
        '''
        # Default to 0 for Classic, which doesn't have a prefix.
        sort_value = 0

        for prefix, value in self.sort_order.items():
            if self.name.startswith(prefix):
                sort_value = value

        return sort_value

    def __str__(self):
        '''Display the tier as "Tier: Point/Max".'''
        tier_name: str

        if self.name.startswith('Kul Tiran'):
            tier_name = 'Kul Tiran'
        elif self.name.startswith(('Outland', 'Northrend', 'Cataclysm',
                                   'Pandaria', 'Draenor', 'Legion',
                                   'Zandalari')):
            tier_name = self.name.split()[0]
        else:
            tier_name = 'Vanilla'

        return '{}: {}/{}'.format(tier_name, self.skill_points,
                                  self.max_skill_points)
