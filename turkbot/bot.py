"""Initialization of the bot and command handling"""
from argparse import Namespace
import asyncio
import logging
import os
import re
import sqlite3
import time

import discord
import psutil

import turkbot.commands
import turkbot.web

from turkbot.context import Context
from turkbot.settings import get_config, set_config_path
from turkbot.exceptions import CommandError
from turkbot.games import PlayersInfo
from turkbot.blizzard import BlizzardAPICaller
from turkbot.util import get_prefix

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

USER_MENTION = re.compile(r'<@!?(\d+)>') # some user mentions have a !
CHANNEL_MENTION = re.compile(r'<#(\d+)>')

class TurkBot(discord.Client):
    """The bot itself.

    # Parameters
    - args: arguments gotten from user from command line

    # Attributes
    - players: users' game information
    - blizzapi: caller for the Blizzard API
    - database: database connection
    - load_time: how long the bot took to start, not counting login
    - reminders: all reminders from the `remind` command
    - reminder_loop_started: if the reminder loop has started or not
    """
    def __init__(self, args: Namespace):
        set_config_path(args.config_path)

        self.players = PlayersInfo.load(get_config("GamesSaveFilename"))
        self.blizzapi = BlizzardAPICaller()

        self.database = sqlite3.connect(get_config("DatabaseFilename"))
        self.database.isolation_level = None # autocommit mode
        self.database.row_factory = sqlite3.Row

        # Keys are the rowids of the reminders from the database.
        self.reminders: dict[int, turkbot.commands.remind.Reminder] = {}
        self.reminder_loop_started = False

        self.pin_loop_started = False

        self.emissary_loop_started = False

        if get_config("DebugEnabled"):
            # debug command doesn't exist unless it is enabled
            self.commands["debug"] = turkbot.commands.debug.command
            LOGGER.warning("`debug` command is enabled")

        intents = discord.Intents.default()
        intents.members = True

        super().__init__(intents=intents)
        self.load_time = \
            time.time() - psutil.Process(os.getpid()).create_time()

    # The commands dict will be populated on bot startup. Each key is the name
    # of a command, and the value is the command function for that command's
    # module. Commands with multiple names have multiple keys.
    commands = {}

    def replace_mentions(self, text: str) -> str:
        '''Replace mentions in a message with the name of the user or channel.

        Discord does not handle mentions in embed titles.

        If the user or channel doesn't exist, keep the broken mention.
        '''
        def _replace_user(match: re.Match) -> str:
            user = self.get_user(int(match.group(1)))
            return f'@{user.display_name}' if user else f'<@{match.group(1)}>'

        def _replace_channel(match: re.Match) -> str:
            channel = self.get_channel(int(match.group(1)))
            return f'#{channel.name}' if channel else f'<#{match.group(1)}>'

        text = re.sub(USER_MENTION, _replace_user, text)
        return re.sub(CHANNEL_MENTION, _replace_channel, text)

    async def on_ready(self):
        # Prevent the Login Time from being inaccurate in 'stats' if the bot
        # loses connection to Discord and has to reconnect
        LOGGER.info("Logged in as %s", self.user)

        await self.change_presence(activity=discord.Game(name=get_config(
            "PlayingMessage")))

        await turkbot.commands.remind.populate_reminders(self)

        loop = asyncio.get_event_loop()

        if not self.reminder_loop_started:
            loop.create_task(turkbot.commands.remind.reminder_loop(self))
            self.reminder_loop_started = True

        if not self.pin_loop_started:
            loop.create_task(turkbot.commands.pinlast.pinlast_loop(self))
            self.pin_loop_started = True

        if not self.emissary_loop_started:
            loop.create_task(turkbot.commands.emissary.emissary_loop(self))
            self.emissary_loop_started = True

    async def on_member_join(self, member):
        # This is the role autoassignment system, controlled by the `autoassign`
        # command.
        cursor = self.database.cursor()
        cursor.execute("""
            SELECT role_id
            FROM autoassign
            WHERE guild_id = ?
        """, (member.guild.id,))

        for row in cursor.fetchall():
            role = member.guild.get_role(row['role_id'])
            if role:
                await member.add_roles(role, reason="Autoassigned on joining.")

    async def on_member_remove(self, member):
        await turkbot.commands.leavelog.log(member, self)

    async def on_message(self, message: discord.Message):
        prefix = get_prefix(self.database.cursor(), message.guild)
        if (self.user.id != message.author.id
                and message.content.startswith(prefix)
                and len(message.content) > len(prefix)):

            # Ignore command prefix when getting command name
            command_requested = (
                message.content[len(prefix):]
                .split()[0]
                .lower() # Make command name case insensitive
            )

            # The prefix command can still be called with the bot's default
            # prefix in case the guild's prefix is set to something bad.
            command_func = self.commands.get(command_requested)

            if command_func:
                try:
                    ctx = Context(self, message, message.content)
                    await command_func(ctx.sub())
                except CommandError as exc:
                    await message.channel.send(exc.message)
        elif message.content.startswith(get_config('CommandPrefix') + 'prefix'):
            # The prefix command can still be called with the bot's default
            # prefix in case the guild's prefix is set to something bad.
            try:
                ctx = Context(self, message, message.content)
                await self.commands['prefix'](ctx.sub())
            except CommandError as exc:
                await message.channel.send(exc.message)
        else:
            # If the message is not a command and not sent by the bot, use the
            # automatic reaction system to apply reactions based on phrases in
            # the message (case-insensitive). Also, don't perform @someone
            # emulation if a command will be triggered.
            if self.user.id != message.author.id:
                await turkbot.commands.autoreact.match(self, message)
                await turkbot.commands.someone.match(self, message)

    def populate_commands(self):
        """Populates the self.commands dict"""
        for name in dir(turkbot.commands):
            if name.startswith("_"):
                continue

            cmd = getattr(turkbot.commands, name)

            if not hasattr(cmd, "ALIASES"):
                continue

            # if there is only one name for the command, allow ALIASES
            # to just be a string instead of needing to be wrapped in
            # an iterable
            if isinstance(cmd.ALIASES, str):
                aliases = (cmd.ALIASES,)
            else:
                aliases = cmd.ALIASES

            for alias in aliases:
                # Don't add an alias if it is already in use
                if alias not in self.commands:
                    self.commands[alias] = cmd.command
                else:
                    LOGGER.warning("%s (from %s) in use by %s", alias, name,
                                   self.commands[alias].__module__)

            if callable(getattr(cmd, 'init', None)):
                cmd.init(self)

    def run(self):
        '''Launch the bot and its website.'''
        self.populate_commands()
        loop = asyncio.get_event_loop()
        web_app = turkbot.web.TurkbotWeb(self)
        try:
            loop.run_until_complete(asyncio.gather(
                # turkbot web must start after turkbot because it needs the
                # bot active in order to function.
                super().start(get_config('DiscordAPIToken')),
                web_app.launch(),
            ))
            super().run(get_config("DiscordAPIToken"))
        except discord.LoginFailure:
            LOGGER.critical("The bot could not log in to Discord because the "
                            "API token is missing or invalid. Check the value "
                            "of DiscordAPIToken in config.ini.")
        except KeyboardInterrupt:
            loop.run_until_complete(web_app.stop())
            loop.run_until_complete(self.logout())
