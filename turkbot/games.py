"""Storage of user data for the bot's games"""
import json
import logging
from typing import Any, Optional

import discord # for typing

from turkbot.settings import get_config

LOGGER = logging.getLogger(__name__)

class PlayersInfo:
    """Object that stores all Players.

    # Attributes
    - _players (Dict[str, Player]): Internal storage of players. For JSON
      serialization reasons, the key is the stringified id of the user.
    """
    def __init__(self, _players: dict[str, "Player"] = None):
        self._players = {} if _players is None else _players

    def get(self, user_id: int) -> "Player":
        """Returns the requested Player and makes it if it doesn't exist."""
        if str(user_id) not in self._players:
            self._players[str(user_id)] = Player()

        return self._players[str(user_id)]

    @classmethod
    def load(cls, filename: Optional[str] = None):
        """Load all players' information from a JSON file.

        from_dict() is called recursively on all objects used.
        """
        if filename is None:
            filename = get_config('GamesSaveFilename')

        try:
            with open(filename, 'r') as saves:
                # Load the JSON into a dict, then proccess the data.
                raw = json.load(saves)
        except (FileNotFoundError, json.JSONDecodeError):
            LOGGER.info('could not open %s', filename)

            return cls(_players={})
        else:
            players = {}

            for user, data in raw.items():
                players[user] = Player.from_dict(data)

            return cls(_players=players)

    def save(self, filename: Optional[str] = None) -> None:
        """Save all players' information to a JSON file.

        to_dict() is called recursively on all objects used.
        """
        if filename is None:
            filename = get_config('GamesSaveFilename')

        with open(filename, 'w') as saves:
            json.dump(self.to_dict(), saves, separators=(',', ':'))

    def to_dict(self) -> dict[str, Any]:
        """Convert PlayersInfo to dict."""
        return {user: data.to_dict() for user, data in self._players.items()}

class Player:
    """Represents a player of the bot's games.

    # Attributes
    - money (int): Currency the user has
    - don (DoubleOrNothingPlayer): The user's DoubleOrNothing game data
    - ul (UpperLowerPlayer): The user's UpperLower game data
    """
    def __init__(self, money: int = 0,
                 don: Optional["DoubleOrNothingPlayer"] = None,
                 ul: Optional["UpperLowerPlayer"] = None):
        self.money = money

        if don is None:
            self.don = DoubleOrNothingPlayer()
        else:
            self.don = don

        if ul is None:
            self.ul = UpperLowerPlayer()
        else:
            self.ul = ul

    @classmethod
    def from_dict(cls, raw: dict[str, Any]):
        """Load Player information from a raw dict of loaded JSON data."""
        if "don" in raw:
            don = DoubleOrNothingPlayer.from_dict(raw["don"])
        else:
            don = None

        if "ul" in raw:
            ul = UpperLowerPlayer.from_dict(raw["ul"])
        else:
            ul = None

        return cls(money=raw['money'], don=don, ul=ul)

    def to_dict(self) -> dict[str, Any]:
        """Convert Player to dict."""
        return {
            "money": self.money,
            "don": self.don.to_dict(),
            "ul": self.ul.to_dict(),
        }

class DoubleOrNothingPlayer:
    """Represents the DoubleOrNothing information for a Player.

    # Attributes
    - playing (bool): If there is a game in progress or not
    - pot (int): The amount of money on the table
    - played (int): DoubleOrNothing games played counter
    - best (int): User's best payout from this game
    """
    def __init__(self, playing: bool = False, pot: int = 0, played: int = 0,
                 best: int = 0):
        self.playing = playing
        self.pot = pot
        self.played = played
        self.best = best

    @classmethod
    def from_dict(cls, raw: dict[str, Any]):
        """Load DoN information from a raw dict of loaded JSON data."""
        return cls(
            playing=raw["playing"],
            pot=raw["pot"],
            played=raw["played"],
            best=raw["best"],
        )

    def to_dict(self) -> dict[str, Any]:
        """Convert DoubleOrNothingPlayer to dict."""
        return self.__dict__

class UpperLowerPlayer:
    """Represents the UpperLower information for a Player.

    # Attributes
    - playing (bool): If there is a game in progress or not
    - current_card (CardAcesHigh): The current card on the table
    - cards_left (List[CardAcesHigh]): Cards left in the deck
    - played (int): UpperLower games played counter
    - best (int): User's best payout from this game
    """
    def __init__(self, playing: bool = False,
                 current_card: Optional["CardAcesHigh"] = None,
                 cards_left: list["CardAcesHigh"] = None, played: int = 0,
                 best: int = 0):
        self.playing = playing
        if current_card is None:
            self.current_card = CardAcesHigh()
        else:
            self.current_card = current_card
        self.cards_left = [] if cards_left is None else cards_left
        self.played = played
        self.best = best

    @classmethod
    def from_dict(cls, raw: dict[str, Any]):
        """Load UL information from a raw dict of loaded JSON data."""
        return cls(
            playing=raw["playing"],
            current_card=CardAcesHigh.from_dict(raw["current_card"]),
            cards_left=[CardAcesHigh.from_dict(card)
                        for card in raw["cards_left"]],
            played=raw["played"],
            best=raw["best"],
        )

    def to_dict(self) -> dict[str, Any]:
        """Convert UpperLowerPlayer to dict."""
        return {
            "playing": self.playing,
            "current_card": self.current_card.to_dict(),
            "cards_left": [card.to_dict() for card in self.cards_left],
            "played": self.played,
            "best": self.best,
        }

class Card:
    """Represents a standard playing card, with aces low.

    # Attributes
    - suit (str): One-letter representation of the Card's suit, "?" is unknown.
    - rank (int): Card's rank. Ace is 1, King is 13.
    """
    emoji = {
        "H": ":hearts:",
        "D": ":diamonds:",
        "C": ":clubs:",
        "S": ":spades:",
        "?": ":question:",
    }

    ranks = {
        1: "A",
        2: "2",
        3: "3",
        4: "4",
        5: "5",
        6: "6",
        7: "7",
        8: "8",
        9: "9",
        10: "10",
        11: "J",
        12: "Q",
        13: "K",
    }

    def __init__(self, suit: str = "?", rank: int = 1):
        self.suit = suit
        self.rank = rank

    @classmethod
    def from_dict(cls, raw: dict[str, Any]):
        """Load Card information from a raw dict of loaded JSON data."""
        return cls(suit=raw['suit'], rank=raw['rank'])

    def to_dict(self) -> dict[str, Any]:
        """Convert Card to dict."""
        return self.__dict__

    def __str__(self):
        return "{0}{1}".format(self.emoji[self.suit], self.ranks[self.rank])

    def __lt__(self, other):
        return self.rank < other.rank

    def __le__(self, other):
        return self.rank <= other.rank

    def __eq__(self, other):
        return self.rank == other.rank

    def __ne__(self, other):
        return self.rank != other.rank

    def __gt__(self, other):
        return self.rank > other.rank

    def __ge__(self, other):
        return self.rank >= other.rank

class CardAcesHigh(Card):
    """Represents a standard playing card, with aces high.

    # Attributes
    - suit (str): One-letter representation of the Card's suit, "?" is unknown.
    - rank (int): Card's rank. Ace is 1, King is 13.
    """
    def _cmp(self, other: "CardAcesHigh") -> tuple[int, int]:
        if self.rank == 1:
            self_rank = 14
        else:
            self_rank = self.rank

        if other.rank == 1:
            other_rank = 14
        else:
            other_rank = other.rank

        return (self_rank, other_rank)

    def __lt__(self, other):
        self_rank, other_rank = self._cmp(other)
        return self_rank < other_rank

    def __le__(self, other):
        self_rank, other_rank = self._cmp(other)
        return self_rank <= other_rank

    def __eq__(self, other):
        self_rank, other_rank = self._cmp(other)
        return self_rank == other_rank

    def __ne__(self, other):
        self_rank, other_rank = self._cmp(other)
        return self_rank != other_rank

    def __gt__(self, other):
        self_rank, other_rank = self._cmp(other)
        return self_rank > other_rank

    def __ge__(self, other):
        self_rank, other_rank = self._cmp(other)
        return self_rank >= other_rank

class EmbedGame:
    '''Parent class for games that feature a repeatedly-edited Embed.

    # Attributes
    - player (Player): the Player representing the user playing the game
    - message (discord.Message): the Message containing the Embed
    - reactor (discord.User): user allowed to interact with the Embed
    '''
    __slots__ = ('player', 'message', 'reactor')

    valid_reactions = {} # must be overriden

    def __init__(self, player: Player):
        if not self.valid_reactions:
            raise NotImplementedError("valid_reactions not overridden")

        self.player = player
        self.message = None
        self.reactor = None

    def _check(self, reaction: discord.Reaction, user: discord.Member) -> bool:
        # Make sure that:
        # 1. The person who can control the game is reacting
        # 2. The reaction is applied to the game's message
        # 3. The reaction is a valid control
        if (user.id != self.reactor.id
                or (self.message.id != reaction.message.id)
                or (reaction.emoji not in self.valid_reactions)):
            return False
        return True
