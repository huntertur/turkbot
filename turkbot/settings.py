"""Bot setting getter and default settings for the bot"""
from configparser import ConfigParser
import logging
from os import PathLike
import os.path
from typing import Union

LOGGER = logging.getLogger(__name__)

class _SettingsPrivate:
    def __init__(self):
        self.config = ConfigParser()
        self.config.add_section("General")

    def set_path(self, path: PathLike):
        self.config.clear()
        LOGGER.info('reading config from: %s', path)
        self.config.read(path)

PRIVATE = _SettingsPrivate()

def set_config_path(path: PathLike) -> None:
    '''Set the path to read `config.ini` from.'''
    PRIVATE.set_path(path)

def get_config(key: str) -> Union[bool, str, int]:
    """Return the config value for `key`.

    The value for `key` from config.ini is read and then converted from a str
    to the correct type. If `key` is not defined in config.ini or its value
    cannot be converted to the correct type, the default value is used.

    # Parameters
    - key (str): Key of the config value to get
    """
    config = PRIVATE.config

    if key not in config["General"]:
        return CONFIG_DEFAUTLS[key] # default value already in right type

    # Values in config.ini are always parsed as strings; conversion is needed.
    # Select default value and emit a warning if the value is unconvertible.
    if isinstance(CONFIG_DEFAUTLS[key], str):
        return config["General"][key] # no type conversion necessary

    if isinstance(CONFIG_DEFAUTLS[key], bool):
        # bool() doesn't convert to bool (bool("false") returns True), so
        # it has to be done manually.
        if config["General"][key].lower() in ("0", "no", "off", "false"):
            return False
        if config["General"][key].lower() in ("1", "yes", "on", "true"):
            return True

        LOGGER.warning("Invalid config value for %s, expected a bool, "
                       "defaulting to %s", key, CONFIG_DEFAUTLS[key])
        return CONFIG_DEFAUTLS[key]

    try:
        return int(config["General"][key])
    except ValueError:
        LOGGER.warning("Invalid config value for %s, expected an int, "
                       "defaulting to %s", key, CONFIG_DEFAUTLS[key])
        return CONFIG_DEFAUTLS[key]

CONFIG_DEFAUTLS = {
    "DiscordAPIToken": "",
    "CommandPrefix": "]",
    "PlayingMessage": "]help",
    "DebugEnabled": False,
    "TurkbotWebEnabled": False,
    "TurkbotWebBase": "",
    "TurkbotWebURL": "",
    "TurkbotWebPort": 8080,
    "HscardsFilename": "turkbot/resources/cards.collectible.json",
    "GamesSaveFilename": "gamesave.json",
    "BlizzardClientID": "",
    "BlizzardClientSecret": "",
    "WokeFont": "turkbot/resources/LiberationSerif-Regular.ttf",
    "WokeImage": "turkbot/resources/woke_base.png",
    "AiohttpTimeout": 10,
    "RandomCatSource": "https://aws.random.cat/meow",
    "RandomBirdSource": "https://random.birb.pw/tweet.json",
    "RandomDogSource": "https://random.dog/woof.json",
    "PaginatorMaxLength": 500,
    "PaginatorMaxLines": 10,
    "SharedJokes": "turkbot/resources/jokes.json",
    "DatabaseFilename": "database.db",
    "EmissaryHour": 15,
    "EmissaryMinute": 15,
    "NorrisJokeSource": "https://api.icndb.com/jokes/random",
    "ChangelogFilename": "CHANGELOG.md",
    "CageSource": "https://www.placecage.com",
    "DiscordClientID": "",
    "DiscordClientSecret": "",
    "TheCatApiSource": "https://api.thecatapi.com/v1/images/search",
    "TheCatApiKey": "",
    "CataasSource": "https://cataas.com/cat",
    "RandomBatSource": "https://huntertur.net/bats",
}
