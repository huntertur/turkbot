'''Command-line interface for turkbot'''
from argparse import ArgumentParser, Namespace

from turkbot import __version__

def cli_main(args: list = None) -> Namespace:
    '''Entry point for the command-line portion of the bot.'''
    parser = ArgumentParser(prog='turkbot')

    parser.add_argument('-V', '--version', action='version',
                        version="%(prog)s {}".format(__version__))
    parser.add_argument('-C', '--config-path', action='store',
                        default='config.ini', help='config file location')

    if args is not None:
        return parser.parse_args(args)

    return parser.parse_args() # get from sys.argv
