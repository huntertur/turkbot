"""Assorted functions shared by multiple commands"""
from sqlite3 import Cursor, Row
from typing import Coroutine, Iterable, Optional
from urllib.parse import ParseResult, urlparse

import discord # for typing

from turkbot.exceptions import CommandError, NoPermissionError
from turkbot.settings import get_config

class MatchFailure(Exception):
    '''Failed to find a user with the requested name. Used for fallthrough.'''

class MultipleMatches(CommandError):
    '''Multiple users found with similar-enough names.'''
    default = ('The user you want is too ambiguous. Please use a more complete '
               'username, DiscordTag, or mention.')

class NoMatches(CommandError):
    '''No users or messages found. Message replaced in Context if needed.'''
    default = 'Failed to find that user.'

def find_mention(name: str, members: list[discord.Member],
                 strict: bool) -> discord.Member:
    """Find the user with mention `name` in `members`.

    If `name` does not look like a mention string, fail immediately.
    """
    # Some users' mention strings start with '<@' instead of '<@!'
    if not name.startswith("<@") or not name.endswith(">"):
        raise MatchFailure

    for member in members:
        if member.mention == name:
            return member

    raise MatchFailure

def find_tag(name: str, members: list[discord.Member],
             strict: bool) -> discord.Member:
    """Find the user with DiscordTag `name` in `members`.

    If `name` does not look like a DiscordTag (person#0000), fail immediately.
    """
    if not len(name) >= 6 or not name[-5] == '#':
        raise MatchFailure

    try:
        int(name[-4:])
    except ValueError:
        raise MatchFailure

    for member in members:
        if str(member) == name:
            return member

    raise MatchFailure

def find_in_name(name: str, members: list[discord.Member],
                 strict: bool) -> discord.Member:
    """Find the user with `name` as part of the name in `members`.

    A dict of all members with `name` as part of their display name (case
    insensitive) is created. The member with the display name that is closest
    to `name` is then returned.
    """
    name = name.lower()
    name_len = len(name) # only calculate it once

    matches = {}

    for member in members:
        if name in member.display_name.lower():
            # The key of each pair in the dict is the member. The value is the
            # ratio of the partial name length to the display name length for
            # each member whose display name contains `name`.
            matches[member] = name_len / len(member.display_name)

    if not matches:
        raise MatchFailure

    # Return the member who has the highest `name`-to-display_name ratio. If
    # there is a tie, pick the first member that has this ratio unless strict.
    max_ratio = max(matches.values())
    max_matches = []

    for member, ratio in matches.items():
        if ratio == max_ratio:
            max_matches.append(member)

    if len(max_matches) > 1 and strict:
        raise MultipleMatches

    return max_matches[0]

def get_member(name: str, guild: discord.Guild,
               strict: bool = True) -> discord.Member:
    """Find a member in a guild.

    Search order:
    1. If `name` looks like a mention (<@...>), search by Member.mention.
    2. If `name` looks like a DiscordTag#0000, search by str(Member).
    3. Search by `name in Member.display_name` by closeness to the name.

    # Parameters
    - name (str): The string to search by
    - guild (discord.Guild): The guild to search in
    - strict (bool): If True, raise MultipleMatches if several users have the
        maximum closeness ratio. If False, return the first max closeness user.
    """
    for func in (find_mention, find_tag, find_in_name):
        try:
            user = func(name, guild.members, strict)
            break
        except MatchFailure:
            continue
    else:
        raise NoMatches

    return user

async def get_last_text_message(channel: discord.TextChannel,
                                skip_first: bool = True) -> discord.Message:
    """Gets the last Message sent in a Channel that has at least some text.

    Messages that have no textual content, such as an image upload without a
    caption, are skipped over. The textual content is returned.

    # Parameters
    - channel (discord.Channel): The Channel to scan
    - skip_first (bool): Skip the first Message, which would be a sent command

    # Raises
    - NoMatches: Could not find a suitable Message
    """
    messages = channel.history()

    if skip_first:
        try:
            await messages.__anext__()
        except StopAsyncIteration:
            raise NoMatches

    async for msg in messages:
        if msg.content:
            return msg

    raise NoMatches

def is_image_url(url: str) -> bool:
    """Check if a URL looks like it points to an image."""
    parse: ParseResult = urlparse(url)
    extensions = (".jpg", ".jpeg", ".png", ".gif", ".bmp", ".webp")
    schemes = ("http", "https")
    return parse.scheme in schemes and parse.path.lower().endswith(extensions)

def get_prefix(cursor: Cursor, guild: discord.Guild):
    '''Gets a guild's command prefix.

    If the guild does not have an entry in the `prefixes` database table,
    it will be created and set to use the default prefix.
    '''
    default: str = get_config('CommandPrefix')

    if guild is None: # Direct Messages, etc.
        return default

    row: Row = cursor.execute('''
        SELECT prefix
        FROM prefixes
        WHERE guild_id = ?
    ''', (guild.id,)).fetchone()

    if not row:
        cursor.execute('INSERT INTO prefixes VALUES (?, ?)', (guild.id, None))
        return default

    if row['prefix'] is None:
        return default

    return row['prefix']

def restrict(*,
             dms: bool = True,
             min_args: int = 0,
             max_args: Optional[int] = None,
             perms: Optional[Iterable[str]] = None,
             timezone: bool = False):
    '''Decorator for commands and subcommands that handles restrictions.

    # Parameters
    dms: Allow command to be used in Direct Messages.
    min_args: Minimum number of arguments.
    max_args: Maximum number of arguments. None for no limit.
    perms: Permissions the user requires. None for no requirements.
    timezone: If the guild needs its timezone set before using the command.
    '''
    from functools import wraps

    from discord import DMChannel

    from turkbot.context import Context

    dm_error = "This {type} is disabled in Direct Messages."
    min_error = "This {type} takes at least {min} argument{plural}."
    max_error = "This {type} takes at most {max} argument{plural}."
    both_error = "This {type} takes {min}-{max} argument{plural}."
    exact_error = "This {type} takes exactly {min} argument{plural}."
    tz_error = "This {type} needs the server's timezone set. See `help tz`."

    def get_type(coro: Coroutine) -> str:
        '''Determine if a command or a subcommand was used.'''
        if coro.__name__ == 'command':
            return 'command'
        return 'subcommand'

    def check_error(ctx: Context, what: str) -> str:
        '''Send an appropriate error message if a restriction was violated.'''
        # Check permissions and channel first, because if the user can't use
        # the command, errors in argument count won't matter.
        # However, break right away if it's a Direct Message issue.
        if not dms and isinstance(ctx.channel, DMChannel):
            raise CommandError(dm_error.format(type=what))
        if timezone and not ctx.timezone:
            raise CommandError(tz_error.format(type=what))
        if perms:
            for name in perms:
                if not getattr(ctx.author.permissions_in(ctx.channel), name):
                    raise NoPermissionError

        error = None
        plural = None

        if (min_args and max_args and min_args == max_args
                and len(ctx.args) != min_args):
            # Either min_args or max_args can be used here since they're equal
            error = exact_error
            plural = '' if min_args == 1 else 's'
        elif (min_args and max_args
              and (len(ctx.args) < min_args or len(ctx.args) > max_args)):
            error = both_error
            # 'arguments' will always be plural since there isn't exactly one
            # argument, but 0-1, 1-2, etc.
            plural = 's'
        elif min_args and len(ctx.args) < min_args:
            error = min_error
            plural = '' if min_args == 1 else 's'
        elif max_args and len(ctx.args) > max_args:
            error = max_error
            plural = '' if max_args == 1 else 's'

        if error:
            raise CommandError(error.format(type=what, min=min_args,
                                            max=max_args, plural=plural))

    def coro_wrapper(coro: Coroutine):
        @wraps(coro)
        async def cmd_wrapper(ctx: Context):
            check_error(ctx, get_type(coro))
            return await coro(ctx)
        return cmd_wrapper
    return coro_wrapper
