"""Command that allows users to add or remove themselves from specific roles"""
from sqlite3 import Cursor, IntegrityError
from typing import Optional

from discord import Guild, Role

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.util import restrict

def get_role_by_name(guild: Guild, name: str) -> Optional[Role]:
    """Select a role in a guild by its name. The inverse of Role.name."""
    for role in guild.roles:
        if role.name == name:
            return role
    return None

def get_registered_roles(cursor: Cursor, guild: Guild) -> list[Role]:
    '''Get all self-assignable roles registered in a guild.'''
    cursor.execute('''
        SELECT role_id
        FROM roles
        WHERE guild_id = ?
    ''', (guild.id,))

    roles: list[Role] = []

    for row in cursor.fetchall():
        role_id: int = row['role_id']
        role: Optional[Role] = guild.get_role(role_id)

        # Purge deleted roles
        if role is None:
            unregister_role(cursor, guild, role_id)
        else:
            roles.append(role)

    return roles

def register_role(cursor: Cursor, guild: Guild, role: Role) -> bool:
    '''Register a self-assignable role. Returns False if already registered.'''
    try:
        cursor.execute('''
            INSERT INTO roles
            VALUES (?, ?)
        ''', (guild.id, role.id))
        return True
    except IntegrityError: # role_id is UNIQUE
        return False

def unregister_role(cursor: Cursor, guild: Guild, role_id: int) -> bool:
    '''Deregister a self-assignable role. Returns False if not registered.'''
    cursor.execute('''
        DELETE FROM roles
        WHERE role_id = ?
        AND guild_id = ?
    ''', (role_id, guild.id))

    return cursor.rowcount != 0

async def subcommand_join(ctx: Context) -> None:
    name = str(ctx.args)
    role = get_role_by_name(ctx.guild, name)

    if role is None:
        raise CommandError("`{}` is not a role.".format(name))

    if role not in get_registered_roles(ctx.cursor, ctx.guild):
        raise CommandError("`{}` is not a registered role.".format(name))

    if role in ctx.author.roles:
        raise CommandError("You already have the `{}` role.".format(name))

    await ctx.author.add_roles(role, reason="Joined with `role` command.")
    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_leave(ctx: Context) -> None:
    name = str(ctx.args)
    role = get_role_by_name(ctx.guild, name)

    if role is None:
        raise CommandError("`{}` is not a role.".format(name))

    if role not in get_registered_roles(ctx.cursor, ctx.guild):
        raise CommandError("`{}` is not a registered role.".format(name))

    if role not in ctx.author.roles:
        raise CommandError("You do not have the `{}` role.".format(name))

    await ctx.author.remove_roles(role, reason="Left with `role` command.")
    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(perms=['manage_roles'])
async def subcommand_add(ctx: Context) -> None:
    name = str(ctx.args)
    role = get_role_by_name(ctx.guild, name)

    if role is None:
        raise CommandError("`{}` is not a role.".format(name))

    if not register_role(ctx.cursor, ctx.guild, role):
        raise CommandError("`{}` is already registered.".format(name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(perms=['manage_roles'])
async def subcommand_remove(ctx: Context) -> None:
    name = str(ctx.args)
    role = get_role_by_name(ctx.guild, name)

    if role is None:
        raise CommandError("`{}` is not a role".format(name))

    if not unregister_role(ctx.cursor, ctx.guild, role.id):
        raise CommandError("`{}` is not registered.".format(name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_list(ctx: Context) -> None:
    role_names = []

    for role in get_registered_roles(ctx.cursor, ctx.guild):
        role_names.append("- {}".format(role))

    if not role_names:
        raise CommandError("There are no roles in this server registered "
                           "with this command.")

    pag = Paginator.from_lines(
        lines=role_names,
        title="Registered Roles ({})".format(len(role_names)),
    )

    await pag.send(ctx.client, ctx.message)

SUBCOMMANDS = {
    "join": subcommand_join,
    "leave": subcommand_leave,
    "add": subcommand_add,
    "remove": subcommand_remove,
    "list": subcommand_list,
}

ALIASES = ("role",)

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS roles (
            guild_id INTEGER NOT NULL,
            role_id INTEGER NOT NULL UNIQUE
        )
    """)

@restrict(dms=False, min_args=1)
async def command(ctx: Context) -> None:
    """`role`: Subscribe or unsubscribe from a role.

    **Usage:** `role subcommand [name]`

    `role join ROLE`: Subscribe to role ROLE.
    `role leave ROLE`: Unsubscribe from role ROLE.
    `role add ROLE`: Register ROLE as a joinable/leavable role. User must have
    Manage Roles permission.
    `role remove ROLE`: Unregister ROLE as a joinable/leavable role. User must
    have Manage Roles permission.
    `role list`: List the registered roles for the server.
    """
    sub = ctx.args.pop().lower()

    if sub not in SUBCOMMANDS:
        raise CommandError("Unknown subcommand, use `help role` for a list.")

    await SUBCOMMANDS.get(sub)(ctx)
