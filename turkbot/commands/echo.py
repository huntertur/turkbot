"""Command that causes the bot to repeat a message."""
from turkbot.context import Context

ALIASES = ("echo",)

async def command(ctx: Context) -> None:
    """`echo`: Repeat a message.

    **Usage:** `echo message`

    If you don't give this command a message, it will find the most recently
    sent message and echo it instead.

    **Example:**
        >>> `echo Lizards are neat.`
        Lizards are neat.

    The bot does not interpret anything it echos as a command.
    """
    await ctx.send(await ctx.get_content())
