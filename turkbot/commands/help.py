"""Command that provides help on using the bot's commands"""
import re

import turkbot.bot

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator

ALIASES = ("help", "?")

class CommandHelp:
    '''A command and its associated help text.'''
    name_extractor = re.compile(r'`([^`]+)`')

    def __init__(self, cmd_func):
        # The first line of every command's docstring is a one-line description
        # that starts with the full name of the command, followed by every
        # alias that command has.
        #
        # `foo`, `bar`: do something
        self.short_help_full = cmd_func.__doc__.splitlines()[0]
        split_result = self.short_help_full.split(sep=': ')
        self.short_help = split_result[1]
        self.long_help = cmd_func.__doc__
        self.names = [re.match(self.name_extractor, wrapped_name)[1]
                      for wrapped_name in split_result[0].split()]

def get_command_help(wanted_name: str) -> CommandHelp:
    '''Find a CommandHelp based on its name.'''
    for cmd in COMMANDS_HELP:
        for name in cmd.names:
            if wanted_name == name:
                return cmd
    raise KeyError('command not found: ' + wanted_name)

COMMANDS_HELP: list[CommandHelp] = []

def populate_help(bot: 'turkbot.bot.TurkBot'):
    '''Populate the list of CommandHelps.

    This cannot be made an init() because otherwise only commands that have
    been loaded before `help` will be tracked.
    '''
    for cmd_func in set(bot.commands.values()):
        COMMANDS_HELP.append(CommandHelp(cmd_func))

    # The order of the commands seems to be different every time the bot
    # starts up, so sort them alphabetically for consistency.
    COMMANDS_HELP.sort(key=lambda cmd: cmd.names[0])

async def command(ctx: Context) -> None:
    """`help`, `?`: Access help for the bot's commands.

    **Usage:** `help [command]`, *command* is optional

    If a specific command to get help on isn't given, a list of all commands
    will be sent, along with a summary of what each command does. If you give
    a specific command to look up help on, detailed help for that command will
    be sent.
    """
    if COMMANDS_HELP == []:
        populate_help(ctx.client)

    if not ctx.args:
        lines = [f'- {cmd.short_help_full}' for cmd in COMMANDS_HELP]

        pag = Paginator.from_lines(
            lines=lines,
            title="Available commands (`help cmd` for specific help)",
        )

        await pag.send(ctx.client, ctx.message)
    else:
        wanted = ctx.args.pop().lower()

        try:
            await ctx.send(get_command_help(wanted).long_help)
        except KeyError:
            raise CommandError(f'Could not find help for `{wanted}`.')
