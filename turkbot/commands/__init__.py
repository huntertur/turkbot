"""All commands for the bot"""
import importlib
import logging
import pathlib

logging.basicConfig(level=logging.INFO)

# Import all commands.
#
# Each command resides in this directory.
#
# The process goes like this:
#   1. Get all the files in this directory.
#   2. Make sure all of them are Python files that aren't "private".
#   3. Make sure all of them are files, not directories.
#   4. Import each one, dropping the file extension.

# pylint: disable=E1101
DIR_ITEMS = list(pathlib.Path(__file__).parent.glob('[!_]*.py'))
FILES = [file for file in DIR_ITEMS if file.is_file()]

for file in FILES:
    importlib.import_module("turkbot.commands." + file.stem)
