'''Command to get or set a guild's timezone'''
import turkbot.bot # pylint: disable=unused-import
from turkbot.commands.remind import populate_reminders
from turkbot.context import Context
from turkbot.exceptions import CommandError, NoPermissionError
from turkbot.util import restrict

ALIASES = ('timezone', 'tz')

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS guild_timezones (
            guild_id INTEGER NOT NULL UNIQUE,
            timezone TEXT NOT NULL
        )
    ''')

@restrict(dms=False)
async def command(ctx: Context) -> None:
    """`timezone`, `tz`: Get or set the server's timezone.

    Before certain commands can be used, a timezone must be set. Anyone can see
    the server's timezone, but only people with the Manage Server permission can
    set it.

    **Usage:**
        `timezone`: Get the server's current timezone, if one is set.
        `timezone TIMEZONE`: Set the server's timezone to TIMEZONE.
    """
    if not ctx.args:
        # Show the current timezone.
        if not ctx.timezone:
            raise CommandError("This server's timezone is not set. Set it "
                               "using `timezone TIMEZONE_NAME`.")

        await ctx.send("This server's timezone is currently `{}`."
                       .format(ctx.timezone))
        return

    if not ctx.author.permissions_in(ctx.channel).manage_guild:
        raise NoPermissionError

    ctx.timezone = str(ctx.args)

    await populate_reminders(ctx.client)

    await ctx.message.add_reaction("✅") # :white_check_mark:
