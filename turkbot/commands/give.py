"""Command that allows users to give money to others"""
from discord import Embed

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.util import get_member, restrict

ALIASES = ("give",)

@restrict(dms=False, min_args=2)
async def command(ctx: Context) -> None:
    """`give`: Give money to another user.

    **Usage:** `give amount name`
        *name* can be the exact name of the user, a mention for the user, the
        user's DiscordTag, or part of their name if there is no ambiguity.

    You cannot give a user more money than you have, so you cannot go into debt.

    To check your money, use the `money` command or the `stats` subcommand for
    any game.
    """
    # Verify the amount of money is valid
    amount = ctx.args.pop()

    try:
        amount = int(amount)
    except ValueError:
        raise CommandError("{} is not a valid integer.".format(amount))

    if amount <= 0:
        raise CommandError("The amount of money to give must be positive.")

    author_player = ctx.player

    if author_player.money < amount:
        raise CommandError("You are ${} too short to perform the transaction."
                           .format(amount - author_player.money))

    # Get the member to transfer the money to. Stop if the target is ambiguous,
    # is the author, or is a bot.
    name = str(ctx.args)
    member = get_member(name, ctx.guild)

    if member == ctx.author:
        raise CommandError("You cannot give money to yourself.")

    if member.bot:
        raise CommandError("You cannot give money to bots.")

    # Actually perform the transaction

    member_player = ctx.client.players.get(member.id)
    old_balance = author_player.money
    author_player.money -= amount
    member_player.money += amount
    ctx.client.players.save()

    # Inform the user that the transaction was successful

    embed = Embed()

    embed.colour = 0x008000
    embed.title = "Transaction Successful"
    embed.description = ("{author} has transferred ${amount} to {target}."
                         .format(amount=amount,
                                 author=ctx.author.display_name,
                                 target=member.display_name))
    embed.add_field(name="Old Balance", value=old_balance)
    embed.add_field(name="New Balance", value=author_player.money)

    await ctx.send(embed=embed)
