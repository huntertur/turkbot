"""Convenience command for joining a role"""
from turkbot.context import Context
from turkbot.commands.role import subcommand_join
from turkbot.util import restrict

ALIASES = ("sub", "iam")

@restrict(dms=False, min_args=1)
async def command(ctx: Context) -> None:
    """`sub`, `iam`: Subscribe to a role.

    **Usage:** `sub ROLE`

    This command is functionally the same as `role join ROLE`.
    """
    await subcommand_join(ctx)
