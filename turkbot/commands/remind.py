"""Command to automatically send messages at certain times"""
import asyncio
from datetime import datetime
import sqlite3
from typing import Optional
from zoneinfo import ZoneInfo

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.util import restrict

async def populate_reminders(client: 'turkbot.bot.TurkBot') -> None:
    """Populate the client's reminders.

    This is called whenever the bot receives an on_ready event, such as when
    reconnecting to Discord. It is also called whenever a guild changes its
    timezone.

    # Parameters
    - client: the client to populate the reminders of
    """
    # Clear reminders before readding them
    client.reminders = {}

    cursor = client.database.cursor()
    cursor.execute("""
        SELECT rowid, guild_id, channel_id, name, minute, hour, day_of_month,
               month, day_of_week, message, enabled
        FROM reminders
    """)

    for row in cursor.fetchall():
        if row["enabled"] == 0: # disabled reminder
            continue

        client.reminders[row["rowid"]] = \
            Reminder.from_client_and_row(client, row)

async def reminder_loop(client: 'turkbot.bot.TurkBot') -> None:
    """Begin checking when reminders should be sent.

    This is called once when the bot initially starts up.

    Every 60 seconds, every Reminder registered with the client is checked. If
    the current time meets the requirements for any reminder, the reminder is
    sent.

    # Parameters
    - client: the client to check the reminders of
    """
    while True:
        for _, reminder in client.reminders.items():
            # A reminder might not have a channel if, say, the channel was
            # deleted without moving the reminder.
            if not reminder.channel:
                continue

            current_time = datetime.utcnow()

            # In the crontab-style format used for inputting reminders, for the
            # day of the week, 0 is Sunday and 6 is Saturday. weekday() returns
            # 0 for Monday and 6 for Sunday. However, isoweekday() returns 1 for
            # Monday and 7 for Sunday, allowing 7 to be easily treated as 0
            # instead of shifting all the numbers.
            current_day_of_week = current_time.isoweekday()
            if current_day_of_week == 7:
                current_day_of_week = 0

            if reminder.month is not None:
                if reminder.month != current_time.month:
                    continue

            if reminder.day_of_month is not None:
                if reminder.day_of_month != current_time.day:
                    continue

            if reminder.day_of_week is not None:
                if reminder.day_of_week != current_day_of_week:
                    continue

            if reminder.hour is not None:
                if reminder.hour != current_time.hour:
                    continue

            if reminder.minute is not None:
                if reminder.minute != current_time.minute:
                    continue

            # All time constraints passed; send the reminder!
            # A normal message will be used instead of an Embed because mentions
            # in Embeds do not actually ping their recipients.
            await reminder.channel.send("**Reminder ({0.name})**:\n{0.message}"
                                        .format(reminder))

        await asyncio.sleep(60)

class Reminder:
    """Represents a repeating message in a channel."""
    __slots__ = ("guild", "channel", "name", "minute", "hour", "day_of_month",
                 "month", "day_of_week", "message")

    # Keys are # of month, values are # of days
    DAYS_IN_MONTH = {
        1: 31,
        # There's no year information, so screw leap years. Better to set this
        # to 28 than screwing up three-fourths of the time.
        2: 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31,
    }

    def __init__(self, guild: discord.Guild, channel: discord.TextChannel,
                 name: str, minute: int, hour: int, day_of_month: int,
                 month: int, day_of_week: int, message: str):
        self.guild = guild
        self.channel = channel
        self.name = name
        self.minute = minute
        self.hour = hour
        self.day_of_month = day_of_month
        self.month = month
        self.day_of_week = day_of_week
        self.message = message

    @classmethod
    def from_client_and_row(cls, client: 'turkbot.bot.TurkBot',
                            row: sqlite3.Row):
        guild = client.get_guild(row["guild_id"])
        return cls(
            guild=guild,
            channel=client.get_channel(row["channel_id"]),
            name=row["name"],
            minute=row["minute"],
            hour=row["hour"],
            day_of_month=row["day_of_month"],
            month=row["month"],
            day_of_week=row["day_of_week"],
            message=row["message"],
        )

    def apply_timezone(self, name: str, invert: bool = False) -> "Reminder":
        """Adjust the Reminder for a timezone.

        This can affect the minute, hour, day of month, month, and day of week
        of a Reminder.

        # Parameters
        - name (str): The name of the timezone to use.
        - invert (bool): Modify time in the opposite direction.
        """
        timezone = ZoneInfo(name)
        offset_sec = datetime.now(timezone).utcoffset().total_seconds()
        if invert:
            # invert is used when writing to the database
            offset_sec *= -1
        offset_min, offset_sec = divmod(offset_sec, 60)
        offset_hour, offset_min = divmod(offset_min, 60)

        # Force integers for display with `remind list`
        offset_sec = int(offset_sec)
        offset_min = int(offset_min)
        offset_hour = int(offset_hour)

        # Use the offsets to change the hour and, if necessary, the other
        # elements of time.

        # Since None represents a wildcard (asterisk in the input string), it
        # means this can return early since there is no need to adjust further,
        # except for minutes since hours can still be affected by timezone.
        if self.minute is not None:
            self.minute += offset_min

            if self.minute > 59:
                self.minute -= 60
                if self.hour is None:
                    return self
                self.hour += 1
            elif self.minute < 0:
                self.minute += 60
                if self.hour is None:
                    return self
                self.hour -= 1

        if self.hour is None:
            return self

        self.hour += offset_hour

        if self.hour > 23:
            self.hour -= 24
            if self.day_of_week is not None:
                if self.day_of_week == 6: # Saturday
                    self.day_of_week = 0 # wraparound to Sunday
                else:
                    self.day_of_week += 1
            if self.day_of_month is None:
                return self
            self.day_of_month += 1
        elif self.hour < 0:
            self.hour += 24
            if self.day_of_week is not None:
                if self.day_of_week == 0: # Sunday
                    self.day_of_week = 6 # wraparound to Saturday
                else:
                    self.day_of_week -= 1
            if self.day_of_month is None:
                return self
            self.day_of_month -= 1

        if self.day_of_month is None:
            return self

        if self.month is None:
            # If there is no month, assume there are 31 days in the month (max)
            if self.day_of_month > 31:
                self.day_of_month -= 31
            elif self.day_of_month < 0:
                self.day_of_month += 31
            return self

        if self.day_of_month > self.DAYS_IN_MONTH[self.month]:
            self.day_of_month -= self.DAYS_IN_MONTH[self.month]
            self.month += 1
        elif self.day_of_month < 0:
            self.day_of_month += self.DAYS_IN_MONTH[self.month]
            self.month -= 1

        return self

def parse_time_args(args: list[str]) -> tuple[int, int, int, int, int]:
    """Get elements of time for a reminder from a list of five strings.

    This is used by `add` amd `updatetime` to validate and get the elements of
    time for a reminder before inputting them in a database. Since this is used
    for subcommands, the parameter for this function is an already-split list of
    strings rather than a raw string.

    An asterisk in a field represents a wildcard, which is returned as None.

    # Parameters
    - args (List[str]): five-string list
    """
    minute = None
    hour = None
    day_of_month = None
    month = None
    day_of_week = None

    if args[0] != '*':
        try:
            minute = int(args[0])
        except TypeError:
            raise CommandError("Invalid minute: `{}`".format(args[0]))

        if minute > 59 or minute < 0:
            raise CommandError("Minute must be between 0-59 or `*`.")

    if args[1] != '*':
        try:
            hour = int(args[1])
        except TypeError:
            raise CommandError("Invalid hour: `{}`".format(args[1]))

        if hour > 23 or hour < 0:
            raise CommandError("Hour must be between 0-23 or `*`.")

    if args[2] != '*':
        try:
            day_of_month = int(args[2])
        except TypeError:
            raise CommandError("Invalid day of month: `{}`".format(args[2]))

        if day_of_month > 31 or day_of_month < 1:
            raise CommandError("Day of month must be between 1-31 or `*`.")

    if args[3] != '*':
        try:
            month = int(args[3])
        except TypeError:
            raise CommandError("Invalid month: `{}`".format(args[3]))

        if month > 12 or month < 1:
            raise CommandError("Month must be between 1-12 or `*`.")

    if args[4] != '*':
        try:
            day_of_week = int(args[4])
        except TypeError:
            raise CommandError("Invalid day of week: `{}`".format(args[4]))

        if day_of_week > 6 or day_of_week < 0:
            raise CommandError("Day of week must be 0-6 (Sun-Sat) or `*`.")

    return (minute, hour, day_of_month, month, day_of_week)

@restrict(min_args=7)
async def subcommand_add(ctx: Context) -> None:
    # input format: name min hr day mo weekday text -- seven args
    name = ctx.args.pop()

    # Make sure the reminder name isn't already in use
    ctx.cursor.execute("""
        SELECT 1
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (ctx.guild.id, name))
    if ctx.cursor.fetchone() is not None:
        raise CommandError("The reminder `{}` already exists.".format(name))

    minute, hour, day_of_month, month, day_of_week = \
        parse_time_args([ctx.args.pop() for _ in range(5)])

    text = str(ctx.args)

    # The reminder is stored in UTC in the database, so invert the timezone
    # adjustment before storing it.
    reminder = Reminder(ctx.guild, ctx.channel, name, minute, hour,
                        day_of_month, month, day_of_week, text) \
                        .apply_timezone(ctx.timezone, invert=True)


    ctx.cursor.execute("INSERT INTO reminders VALUES (?,?,?,?,?,?,?,?,?,?)",
                       (reminder.guild.id, reminder.channel.id, reminder.name,
                        reminder.minute, reminder.hour, reminder.day_of_month,
                        reminder.month, reminder.day_of_week, reminder.message,
                        1))

    # Right after storing the reminder in the database, it needs to get pulled
    # out right away to get the rowid for the reminders dict in client.
    r_id = ctx.cursor.execute("""
        SELECT rowid
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (reminder.guild.id, reminder.name)).fetchone()["rowid"]
    ctx.client.reminders[r_id] = reminder

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1, max_args=1)
async def subcommand_remove(ctx: Context) -> None:
    name = ctx.args.pop()

    # If the reminder is enabled, update client.reminders since the reminder
    # no longer exists and should not be triggered
    row = ctx.cursor.execute("""
        SELECT rowid, enabled
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (ctx.guild.id, name)).fetchone()

    if not row:
        raise CommandError("The reminder `{}` does not exist.".format(name))

    if row["enabled"] == 1:
        del ctx.client.reminders[row["rowid"]]

    # Now it can be deleted from the database
    ctx.cursor.execute("""
        DELETE FROM reminders
        WHERE guild_id = ? AND name = ?
    """, (ctx.guild.id, name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_list(ctx: Context) -> None:
    # Each reminder gets its own page. The pages look like this:
    # __**NAME**__
    # In channel CHANNEL_NAME
    # Enabled: :white_check_mark: OR :x:
    # \n
    # Minute: MINUTE (or "None")
    # Hour:
    # Day of month:
    # Month:
    # Day of week:
    #
    # MESSAGE
    def get_channel_link(channel: Optional[discord.TextChannel]):
        '''Get the channel mention string, or say there isn't one.

        Directly constructing the mention string in the template using
        <#{0.channel.id}> fails if the channel for the reminder no longer
        exists, if, say, the channel was deleted without moving the reminder.
        '''
        return f'<#{channel.id}>' if channel else '**None** (use `remind move`)'

    pages = []
    template = ("__**{0.name}**__\n"
                "In channel {channel_link}\n"
                "Enabled: {emoji}\n"
                "\n"
                "Minute: {0.minute}\n"
                "Hour: {0.hour}\n"
                "Day of month: {0.day_of_month}\n"
                "Month: {0.month}\n"
                "Day of week: {0.day_of_week}\n"
                "\n"
                "{0.message}")


    ctx.cursor.execute("""
        SELECT guild_id, channel_id, name, minute, hour, day_of_month, month,
               day_of_week, message, enabled
        FROM reminders
        WHERE guild_id = ?
    """, (ctx.guild.id,))

    for row in ctx.cursor.fetchall():
        rem = Reminder.from_client_and_row(ctx.client, row) \
                      .apply_timezone(ctx.timezone)
        pages.append(template.format(
            rem, channel_link=get_channel_link(rem.channel),
            emoji=":x:" if row["enabled"] == 0 else ":white_check_mark:",
        ))

    if not pages:
        raise CommandError("There are no reminders in this server.")

    pag = Paginator(pages=pages,
                    title="Reminders in {}".format(ctx.guild.name))

    await pag.send(ctx.client, ctx.message)

@restrict(min_args=1, max_args=1)
async def subcommand_move(ctx: Context) -> None:
    name = ctx.args.pop()

    # If the reminder is enabled, it is in client.reminders, so the rowid will
    # be used to update that Reminder's channel after updating the database.
    row = ctx.cursor.execute("""
        SELECT rowid, channel_id, enabled
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (ctx.guild.id, name)).fetchone()

    if not row:
        raise CommandError("The reminder `{}` does not exist.".format(name))

    if row["channel_id"] == ctx.channel.id:
        raise CommandError("That reminder is already in this channel.")

    ctx.cursor.execute("""
        UPDATE reminders
        SET channel_id = ?
        WHERE guild_id = ? AND name = ?
    """, (ctx.channel.id, ctx.guild.id, name))

    if row["enabled"] == 1: # not in client.reminders if disabled
        ctx.client.reminders[row["rowid"]].channel = ctx.channel

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=2, max_args=2)
async def subcommand_rename(ctx: Context) -> None:
    old_name = ctx.args.pop()
    new_name = ctx.args.pop()

    # Make sure the new tag name is not in use.
    ctx.cursor.execute("""
        SELECT 1
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (ctx.guild.id, new_name))
    if ctx.cursor.fetchone() is not None:
        raise CommandError("The name `{}` is already taken.".format(new_name))

    ctx.cursor.execute("""
        UPDATE reminders
        SET name = ?
        WHERE guild_id = ? AND name = ?
    """, (new_name, ctx.guild.id, old_name))

    # Update the Reminder in client.reminders, if enabled
    row = ctx.cursor.execute("""
        SELECT rowid, enabled
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (ctx.guild.id, new_name)).fetchone()

    if row["enabled"] == 1:
        ctx.client.reminders[row["rowid"]].name = new_name

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=6, max_args=6)
async def subcommand_updatetime(ctx: Context) -> None:
    # six args, not seven, since there is no `text` field
    name = ctx.args.pop()

    # Make sure the reminder exists, but also get the rowid and enabled status
    # for editing client.reminders, as well as the message and channel id
    row = ctx.cursor.execute("""
        SELECT rowid, enabled, message, channel_id
        FROM reminders
        WHERE guild_id = ? AND name = ?
    """, (ctx.guild.id, name)).fetchone()
    if row is None:
        raise CommandError("The reminder `{}` does not exist.".format(name))

    minute, hour, day_of_month, month, day_of_week = \
        parse_time_args([ctx.args.pop() for _ in range(5)])

    tz_name = ctx.timezone

    reminder = Reminder(ctx.guild, ctx.client.get_channel(row["channel_id"]),
                        name, minute, hour, day_of_month, month, day_of_week,
                        row["message"]).apply_timezone(tz_name, invert=True)

    # Only the elements of time need to be updated
    ctx.cursor.execute("""
        UPDATE reminders
        SET minute = ?, hour = ?, day_of_month = ?, month = ?, day_of_week = ?
        WHERE guild_id = ? AND name = ?
    """, (reminder.minute, reminder.hour, reminder.day_of_month, reminder.month,
          reminder.day_of_week, ctx.guild.id, name))

    # Only update client.reminders if the reminder is enabled
    if row["enabled"] == 1:
        ctx.client.reminders[row["rowid"]] = reminder

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=2)
async def subcommand_updatetext(ctx: Context) -> None:
    name = ctx.args.pop()
    text = str(ctx.args)

    # Make sure the reminder exists, but also get the rowid and enabled status
    # for editing client.reminders
    row = ctx.cursor.execute("""
        SELECT rowid, enabled
        FROM reminders
        WHERE guild_id = ? AND name = ?
    """, (ctx.guild.id, name)).fetchone()
    if row is None:
        raise CommandError("The reminder `{}` does not exist.".format(name))

    ctx.cursor.execute("""
        UPDATE reminders
        SET message = ?
        WHERE guild_id = ? AND name = ?
    """, (text, ctx.guild.id, name))

    # Only update client.reminders if the reminder is enabled
    if row["enabled"] == 1:
        ctx.client.reminders[row["rowid"]].message = text

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1, max_args=1)
async def subcommand_enable(ctx: Context) -> None:
    name = ctx.args.pop()

    # All fields are needed -- rowid for adding the reminder to the client's
    # dict, enabled for checking enabled status, and everything else for the
    # Reminder creation.
    ctx.cursor.execute("""
        SELECT rowid, guild_id, channel_id, name, minute, hour, day_of_month,
               month, day_of_week, message, enabled
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (ctx.guild.id, name))

    row = ctx.cursor.fetchone()

    if row is None:
        raise CommandError("The reminder `{}` does not exist.".format(name))

    if row["enabled"] == 1:
        raise CommandError("This reminder is already enabled.")

    ctx.cursor.execute("""
        UPDATE reminders
        SET enabled = 1
        WHERE guild_id = ? AND name = ?
    """, (ctx.guild.id, name))

    ctx.client.reminders[row["rowid"]] = \
        Reminder.from_client_and_row(ctx.client, row)

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1, max_args=1)
async def subcommand_disable(ctx: Context) -> None:
    name = ctx.args.pop()

    ctx.cursor.execute("""
        SELECT rowid, enabled
        FROM reminders
        WHERE guild_id = ? AND name = ?
        LIMIT 1
    """, (ctx.guild.id, name))

    row = ctx.cursor.fetchone()

    if row is None:
        raise CommandError("The reminder `{}` does not exist.".format(name))

    if row["enabled"] == 0:
        raise CommandError("This reminder is already disabled.")

    ctx.cursor.execute("""
        UPDATE reminders
        SET enabled = 0
        WHERE guild_id = ? AND name = ?
    """, (ctx.guild.id, name))

    del ctx.client.reminders[row["rowid"]]

    await ctx.message.add_reaction("✅") # :white_check_mark:

SUBCOMMANDS = {
    "add": subcommand_add,
    "remove": subcommand_remove,
    "list": subcommand_list,
    "move": subcommand_move,
    "rename": subcommand_rename,
    "updatetime": subcommand_updatetime,
    "updatetext": subcommand_updatetext,
    "enable": subcommand_enable,
    "disable": subcommand_disable,
}

ALIASES = ("remind",)

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS reminders (
            guild_id INTEGER NOT NULL,
            channel_id INTEGER NOT NULL,
            name TEXT NOT NULL,
            minute INTEGER,
            hour INTEGER,
            day_of_month INTEGER,
            month INTEGER,
            day_of_week INTEGER,
            message TEXT NOT NULL,
            enabled INTEGER NOT NULL -- 1 = true, 0 = false
        )
    """)

@restrict(dms=False, min_args=1, perms=['manage_messages'], timezone=True)
async def command(ctx: Context) -> None:
    """`remind`: Send a message in a channel at certain times.

    You must have the Manage Messages permission to interact with this command.

    Reminders are defined in a `crontab`-style format that allows control over
    which minute, hour, day of the month, month, and/or day of the week the
    reminder is sent. A reminder also has a name, used to identify it for
    editing; a channel, where it is sent; and a message, which is the text that
    is sent in the reminder. For maximum effectiveness, consider mentioning a
    relevant role in the reminder's message.

    **Note:** Before reminders can be added in your server, you must set your
    server's timezone using `timezone`.

    **Subcommands:**
        `remind add NAME FORMAT TEXT`: Add a reminder to this channel.
        `remind remove NAME`: Delete a reminder from this channel.
        `remind list`: List all reminders in the server.
        `remind move NAME`: Move a reminder to this channel.
        `remind rename OLD NEW`: Rename a reminder.
        `remind updatetime NAME FORMAT`: Change when a reminder triggers.
        `remind updatetext NAME TEXT`: Change the message a reminder sends.
        `remind enable NAME`: Enable a disabled reminder.
        `remind disable NAME`: Stop a reminder from triggering.

    **Reminder input format:**
        The input format for a reminder is similar to a `crontab` entry. There
        are five parts, separated by a space:
        - Minute (0-59)
        - Hour (0-23)
        - Day of month (1-31)
        - Month (1-12)
        - Day of week (0-6) (Sunday to Saturday)
        Any of these can be replaced by an asterisk (`*`), which means there is
        no restriction for that element of time.
    """
    sub = ctx.args.pop().lower()

    if sub in SUBCOMMANDS:
        await SUBCOMMANDS.get(sub)(ctx)
    else:
        raise CommandError("Unknown subcommand, see `help remind` for help.")
