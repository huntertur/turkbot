"""Command that assigns new users certain roles when joining the guild"""
import sqlite3

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.commands.role import get_role_by_name
from turkbot.util import restrict

async def subcommand_add(ctx: Context) -> None:
    name = str(ctx.args)
    role = get_role_by_name(ctx.message, name)

    if role is None:
        raise CommandError("`{}` is not a role.".format(name))

    try:
        ctx.cursor.execute("INSERT INTO autoassign VALUES (?, ?)",
                           (ctx.guild.id, role.id))
    except sqlite3.IntegrityError: # role_id is UNIQUE
        raise CommandError("`{}` is already registered.".format(name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_remove(ctx: Context) -> None:
    name = str(ctx.args)
    role = get_role_by_name(ctx.message, name)

    if role is None:
        raise CommandError("`{}` is not a role.".format(name))

    ctx.cursor.execute("""
        DELETE FROM autoassign
        WHERE guild_id = ? AND role_id = ?
    """, (ctx.guild.id, role.id))

    if ctx.cursor.rowcount == 0:
        raise CommandError("`{}` is not registered.".format(name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_list(ctx: Context) -> None:
    role_names = []
    ctx.cursor.execute("""
        SELECT role_id
        FROM autoassign
        WHERE guild_id = ?
    """, (ctx.guild.id,))

    for row in ctx.cursor.fetchall():
        role_names.append("- {}".format(ctx.guild.get_role(row["role_id"])))

    if not role_names:
        raise CommandError("There are no roles in this server registered "
                           "with this command.")

    pag = Paginator.from_lines(
        lines=role_names,
        title="Autoassigned Roles ({})".format(len(role_names)),
    )

    await pag.send(ctx.client, ctx.message)

SUBCOMMANDS = {
    "add": subcommand_add,
    "remove": subcommand_remove,
    "list": subcommand_list,
}

ALIASES = ("autoassign",)

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS autoassign (
            guild_id INTEGER NOT NULL,
            role_id INTEGER NOT NULL UNIQUE
        )
    """)

@restrict(dms=False, min_args=1, perms=['manage_roles'])
async def command(ctx: Context) -> None:
    """`autoassign`: Add roles to new users that join the server.

    You and the bot must have the Manage Roles permission to use this command.

    **Usage:**
        `autoassign add ROLE`: Add ROLE to the autoassignment list.
        `autoassign remove ROLE`: Remove ROLE from the autoassignment list.
        `autoassign list`: List all autoassigned roles in the server.
    """
    sub = ctx.args.pop().lower()

    if sub not in SUBCOMMANDS:
        raise CommandError("Unknown subcommand, see `help autoassign`.")

    await SUBCOMMANDS.get(sub)(ctx)
