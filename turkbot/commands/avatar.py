"""Command for getting the highest-resolution avatar for a User"""
from discord import Embed

from turkbot.context import Context
from turkbot.util import get_member

ALIASES = ("avatar", "icon")

async def command(ctx: Context) -> None:
    """`avatar`, `icon`: Get the highest-resolution avatar for a user.

    **Usage:** `avatar [name]`
        `name` can be a mention for the user, their name in the server, or part
        of the person's name in the server.

        If you don't give a `name`, you will see your own avatar.
    """
    if ctx.args:
        name = str(ctx.args)

        with ctx.channel.typing():
            user = get_member(name, ctx.guild, strict=False)
    else:
        user = ctx.author

    embed = Embed()
    embed.title = "{}'s avatar".format(user.display_name)
    embed.set_image(url=user.avatar_url)
    embed.colour = user.color

    await ctx.send(embed=embed)
