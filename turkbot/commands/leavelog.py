'''Command for logging when people leave the guild'''
from datetime import datetime

from discord import Embed, Member
from discord.utils import escape_markdown

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.util import restrict

ALIASES = 'leavelog'

async def log(member: Member, client: 'turkbot.bot.TurkBot') -> None:
    '''Logs the member who left, if leave logging is enabled.

    This is called by TurkBot.on_member_leave.
    '''
    row = client.database.cursor().execute('''
        SELECT channel_id
        FROM leavelog
        WHERE guild_id = ?
    ''', (member.guild.id,)).fetchone()

    if not row:
        return

    channel = client.get_channel(row['channel_id'])

    output = Embed()
    # Displays as Nickname (Tag)
    output.title = escape_markdown(f"{member.display_name} ({member}) left")
    output.set_thumbnail(url=member.avatar_url)
    output.colour = member.color

    output.add_field(
        name="User For",
        value=f"{datetime.utcnow() - member.joined_at}",
    )

    output.add_field(
        name="Joined At",
        # ex: Monday, January 1, 1900, 9:00:05 AM
        value="{:%A, %B %-d, %Y, %-I:%M:%S %p}".format(member.joined_at),
    )

    await channel.send(embed=output)

async def subcommand_disable(ctx: Context) -> None:
    row = ctx.cursor.execute('''
        SELECT 1
        FROM leavelog
        WHERE guild_id = ? AND channel_id = ?
    ''', (ctx.guild.id, ctx.channel.id)).fetchone()

    if row is None:
        raise CommandError("Leave logging is already disabled.")

    ctx.cursor.execute('''
        DELETE FROM leavelog
        WHERE guild_id = ? AND channel_id = ?
    ''', (ctx.guild.id, ctx.channel.id))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_enable(ctx: Context) -> None:
    row = ctx.cursor.execute('''
        SELECT 1
        FROM leavelog
        WHERE guild_id = ? AND channel_id = ?
    ''', (ctx.guild.id, ctx.channel.id)).fetchone()

    if row is not None:
        raise CommandError("Leave logging is already enabled.")

    ctx.cursor.execute('INSERT INTO leavelog VALUES (?, ?)',
                       (ctx.guild.id, ctx.channel.id))

    await ctx.message.add_reaction("✅") # :white_check_mark:

SUBCOMMANDS = {
    'disable': subcommand_disable,
    'enable': subcommand_enable,
}

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS leavelog (
            guild_id INTEGER NOT NULL,
            channel_id INTEGER NOT NULL
        )
    ''')

@restrict(dms=False, min_args=1, perms=['manage_guild'])
async def command(ctx: Context) -> None:
    """`leavelog`: Log whenever someone leaves the server.

    The log contains the former member's DiscordTag, username they had been
    using in the server, and how long they had been in the server.

    You must have the Manage Server permission in order to enable or disable
    the leave log.

    **Usage:**
        `leavelog enable`: Start logging in this channel.
        `leavelog disable`: Stop logging.
    """
    sub = ctx.args.pop().lower()
    if sub not in SUBCOMMANDS:
        raise CommandError("Unknown subcommand; see `help leavelog`.")
    await SUBCOMMANDS.get(sub)(ctx)
