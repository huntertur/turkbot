"""Command for creating a random League of Legends team composition"""
import random

from discord import Embed

from turkbot.context import Context
from turkbot.util import restrict

ROLES = [
    "Top / Tank",
    "Middle",
    "Bottom / Carry",
    "Jungle",
    "Support",
]

ALIASES = ("5manlol", "lolteam")

@restrict(min_args=5, max_args=5)
async def command(ctx: Context) -> None:
    """`5manlol`, `lolteam`: Create a League of Legends team.

    **Usage:** `5manlol person1 person2 person3 person4 person5`

    **Example:**
        person1: Top / Tank
        person2: Bottom / Carry
        person3: Jungle
        person4: Middle
        person5: Support
    """
    output = Embed()
    output.add_field(name="Person", value="\n".join(ctx.args))
    output.add_field(name="Role",
                     value="\n".join(random.sample(ROLES, len(ROLES))))
    output.colour = 0x004488 # similar to LoL icon background

    await ctx.send(embed=output)
