"""Command that measures the bot's latency to Discord"""
from turkbot.context import Context

ALIASES = ("ping",)

async def command(ctx: Context) -> None:
    """`ping`: Measure the bot's latency to Discord.

    **Usage:** `ping`
    """
    await ctx.send(":signal_strength: Ping is {:.0f} ms."
                   .format(ctx.client.latency * 1000))
