'''Command to get or set a guild's default WoW realm'''
import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError, NoPermissionError
from turkbot.util import restrict

ALIASES = 'wowrealm'

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS default_realms (
            guild_id INTEGER NOT NULL UNIQUE,
            realm TEXT NOT NULL
        )
    ''')

@restrict(dms=False)
async def command(ctx: Context) -> None:
    '''`wowrealm`: Get or set the server's default WoW realm.

    When the realm suffix is omitted from a character name when using the `wow`
    command, the default realm set using this command will be used.

    You must have the Manage Server permission to set the default realm.

    **Usage:**
        `wowrealm`: Get the server's default WoW realm, if it is set.
        `wowrealm REALM`: Set the server's default WoW realm to REALM.
    '''
    if not ctx.args:
        # Show the current realm.
        if not ctx.default_realm:
            raise CommandError("This server's default WoW realm is not set."
                               "Set it using `wowrealm REALM_NAME`.")
        await ctx.send("This server's default WoW realm is currently {}."
                       .format(ctx.default_realm))
        return

    if not ctx.author.permissions_in(ctx.channel).manage_guild:
        raise NoPermissionError

    # Blizzard's API expects realms to not have spaces in them.
    ctx.default_realm = str(ctx.args).replace(' ', '')

    await ctx.message.add_reaction("✅") # :white_check_mark:
