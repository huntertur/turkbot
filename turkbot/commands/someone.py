'''Command to emulate the behavior of @someone.'''
import random
from sqlite3 import Cursor

from discord import Message, User

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.util import restrict

# pylint: disable=anomalous-backslash-in-string
TEMPLATES = (
    '(◕‿◕✿) ***({name})***',
    '(∩ ͡° ͜ʖ ͡°)⊃━✿✿✿✿✿✿ ***({name})***',
    r'¯\\(°_o)/¯ ***({name})***',
    'ヽ༼ ಠ益ಠ ༽ﾉ ***({name})***',
    '༼ つ ◕_◕ ༽つ ***({name})***',
    '(∩ ͡° ͜ʖ ͡°)⊃━☆ﾟ. o ･ ｡ﾟ ***({name})***',
    '（✿ ͡◕ ᴗ◕)つ━━✫・o。 ***({name})***',
    '(╯°□°）╯︵ ┻━┻ ***({name})***',
    r'¯\\\_(ツ)_/¯ ***({name})***',
)

ALIASES = 'someone'

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS at_someone_disable (
            user_id INTEGER NOT NULL UNIQUE
        )
    ''')

def is_enabled(cursor: Cursor, user: User) -> bool:
    '''Check if @someone emulation is enabled for a user.'''
    row = cursor.execute('''
        SELECT 1
        FROM at_someone_disable
        WHERE user_id = ?
    ''', (user.id,)).fetchone()

    return row is None

def enable(cursor: Cursor, user: User) -> None:
    '''Enable @someone emulation for a user.'''
    cursor.execute('''
        DELETE FROM at_someone_disable
        WHERE user_id = ?
    ''', (user.id,))

def disable(cursor: Cursor, user: User) -> None:
    '''Disable @someone emulation for a user.'''
    cursor.execute('''
        INSERT INTO at_someone_disable
        VALUES (?)
    ''', (user.id,))

async def match(bot: 'turkbot.bot.TurkBot', message: Message) -> None:
    '''Send an @someone response, if appropriate.'''
    if (not '@someone' in message.content
            or not is_enabled(bot.database.cursor(), message.author)):
        return
    name = random.choice(message.guild.members).display_name
    response = random.choice(TEMPLATES).format(name=name)
    await message.channel.send(response)

async def subcommand_disable(ctx: Context) -> None:
    if is_enabled(ctx.cursor, ctx.author):
        disable(ctx.cursor, ctx.author)
        await ctx.message.add_reaction("✅") # :white_check_mark:
    else:
        raise CommandError('@someone emulation is already disabled for you.')

async def subcommand_enable(ctx: Context) -> None:
    if not is_enabled(ctx.cursor, ctx.author):
        enable(ctx.cursor, ctx.author)
        await ctx.message.add_reaction("✅") # :white_check_mark:
    else:
        raise CommandError('@someone emulation is already enabled for you.')

async def subcommand_query(ctx: Context) -> None:
    status = 'enabled' if is_enabled(ctx.cursor, ctx.author) else 'disabled'
    await ctx.send(f'@someone emulation is {status} for you.')

SUBCOMMANDS = {
    'disable': subcommand_disable,
    'enable': subcommand_enable,
    'query': subcommand_query,
}

@restrict(dms=False, min_args=1, max_args=1)
async def command(ctx: Context):
    '''`someone`: Toggle personal @someone emulation.

    Emulate the 2018 Discord April Fools' Day @someone feature. Enabled by
    default, but can be disabled on a per-user basis.

    **Usage:**
        `someone disable`: Disable @someone emulation for yourself.
        `someone enable`: Enable @someone emulation for yourself.
        `someone query`: Check if you have @someone emulation enabled.
    '''
    sub = ctx.args.pop().lower()

    if sub in SUBCOMMANDS:
        await SUBCOMMANDS.get(sub)(ctx)
    else:
        raise CommandError('Unknown subcommand; see `help someone` for help.')
