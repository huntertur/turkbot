"""Command for consulting a Magic 8-Ball"""
from enum import Enum
import random

from discord import Embed
from discord.errors import HTTPException

from turkbot.context import Context
from turkbot.exceptions import MessageTooLargeError

class Color(Enum):
    """Defines colors used for the command's output's embed's color.

    GOOD represents the color for a positive resposne.
    UNSURE represents the color for a "hazy" or otherwise unsure response.
    BAD represents the color for a negative resposne.
    """
    GOOD = 0x008000 # green
    UNSURE = 0x808000 # olive
    BAD = 0x800000 # maroon

SAYINGS = [
    ("It is certain.", Color.GOOD),
    ("It is decidedly so.", Color.GOOD),
    ("Without a doubt.", Color.GOOD),
    ("Yes, definitely.", Color.GOOD),
    ("You may rely on it.", Color.GOOD),
    ("As I see it, yes.", Color.GOOD),
    ("Most likely.", Color.GOOD),
    ("Outlook: good.", Color.GOOD),
    ("Yes.", Color.GOOD),
    ("Signs point to yes.", Color.GOOD),
    ("Reply hazy; try again.", Color.UNSURE),
    ("Ask again later.", Color.UNSURE),
    ("Better not tell you now.", Color.UNSURE),
    ("Cannot predict now.", Color.UNSURE),
    ("Concentrate and ask again.", Color.UNSURE),
    ("Don't count on it.", Color.BAD),
    ("My reply is no.", Color.BAD),
    ("My sources say no.", Color.BAD),
    ("Outlook: not so good.", Color.BAD),
    ("Very doubtful.", Color.BAD),
]

ALIASES = ("8ball", "8b")

async def command(ctx: Context) -> None:
    """`8ball`, `8b`: Consults the Magic 8-Ball.

    **Usage:** `8ball [question]`, *question* is optional.

    **Example Output:**
        **Hunter consults the Magic 8-Ball...**
        :8ball: Reply hazy; try again.
    """
    if ctx.args:
        title = ('{} asks, "{}"'
                 .format(ctx.author.display_name,
                         ctx.client.replace_mentions(str(ctx.args))))
    else:
        title = ("{} consults the Magic 8-Ball..."
                 .format(ctx.author.display_name))

    phrase, color = random.choice(SAYINGS)
    embed = Embed()
    embed.description = ":8ball: {}".format(phrase)
    embed.colour = color.value
    embed.set_author(name=title, icon_url=ctx.author.avatar_url)

    try:
        await ctx.send(embed=embed)
    except HTTPException:
        raise MessageTooLargeError
