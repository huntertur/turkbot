"""Command that displays stats about the bot"""
import os
import sys
import time

import discord
import psutil

import turkbot.bot

from turkbot.context import Context
from turkbot.settings import get_config

def _get_file_size(filename: str) -> str:
    """Used to generate file size output for the command's embed"""
    try:
        size = os.stat(filename).st_size
    except FileNotFoundError:
        return ":x: *not loaded*"
    else:
        if size < 1024:
            return "{} B".format(size)
        if size < 1024 * 1024:
            return "{:.2f} KiB".format(size / 1024)

        return "{:.2f} MiB".format(size / 1024 / 1024)

ALIASES = ("stats",)

async def command(ctx: Context) -> None:
    """`stats`: Display stats about the bot.

    This command displays stats about the bot at that time, including:
    - Python version
    - Memory usage
    - Time for bot to launch
    - Bot uptime
    - Size of various bot files
    """
    async with ctx.channel.typing():
        await ctx.send(embed=generate_embed(ctx.client))

def generate_embed(client: 'turkbot.bot.TurkBot') -> discord.Embed:
    '''Generate the stats embed.'''
    from turkbot import __version__

    embed = discord.Embed()
    embed.title = "turkbot {}".format(__version__)
    embed.colour = 0xFFCC4D # color of turkbot logo (:thinkeyes:)
    embed.set_thumbnail(url=client.user.avatar_url)

    bot_process = psutil.Process(os.getpid())

    embed.add_field(name="Python Version", value=sys.version, inline=False)

    embed.add_field(name="Bot Startup Time",
                    value="{0.load_time:.2f} seconds\n".format(client))

    # Memory Usage field
    rss_bytes = bot_process.memory_info().rss

    embed.add_field(name="Memory Usage",
                    value="{:.2f} MiB".format(rss_bytes / 1024 ** 2)) # in MiB

    # Bot Uptime field
    uptime = time.time() - bot_process.create_time() # in seconds

    uptime_min, uptime_sec = divmod(uptime, 60)
    uptime_hours, uptime_min = divmod(uptime_min, 60)
    uptime_days, uptime_hours = divmod(uptime_hours, 24)

    uptime_str = "{} {}, {:02}:{:02}:{:02}".format(
        int(uptime_days),
        "day" if int(uptime_days) == 1 else "days",
        int(uptime_hours),
        int(uptime_min),
        int(uptime_sec)
    )

    embed.add_field(name="Bot Uptime", value=uptime_str)

    # File sizes
    embed.add_field(name="Games Save File Size",
                    value=_get_file_size(get_config("GamesSaveFilename")))

    embed.add_field(name="Hearthstone Cards Size",
                    value=_get_file_size(get_config("HscardsFilename")))

    embed.add_field(name="Database File Size",
                    value=_get_file_size(get_config("DatabaseFilename")))

    # turkbot web status
    if not get_config('TurkbotWebEnabled'):
        web_status = ':x: *disabled*'
    else:
        web_status = ':white_check_mark:'
        if get_config('TurkbotWebURL') != '':
            web_status += ' ' + get_config('TurkbotWebURL')
        else:
            web_status += ' *no URL specified*'

    embed.add_field(name='Web App Status', value=web_status)

    return embed
