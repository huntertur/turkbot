"""Command for rolling dice"""
import asyncio
from concurrent.futures import ProcessPoolExecutor
import functools
import random

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator

class RollError(CommandError):
    """Base class for exceptions in the 'roll' command"""

class RollDndError(RollError):
    """Exception in a XdY(+/-)Z roll."""
    def __init__(self, message: str):
        super().__init__(message + " *(d-notation roll)*")

class RollMinMaxError(RollError):
    """Exception in a min-max roll."""
    def __init__(self, message: str):
        super().__init__(message + " *(min-max roll)*")

class RollMaxError(RollError):
    """Exception in a 1-max roll."""
    def __init__(self, message: str):
        super().__init__(message + " *(1-max roll)*")

# This is not nested in roll_dnd because local functions can't be pickled.
def roll_dnd_accumulate(dice: int, sides: int) -> int:
    '''Roll `dice` amount of `sides`-sided dice.'''
    return sum(random.randrange(1, sides + 1) for die in range(dice))

async def roll_dnd(input_text: str) -> int:
    """Perform a XdY(+/-)Z roll and return the result."""
    dice, _, sides = input_text.partition('d')

    # If there aren't any dice given, imply there is one die. This allows users
    # to use `roll d20` to roll a d20 without needing to specify the 1 die.
    if dice == "":
        dice = 1

    if '+' in sides and '-' in sides:
        raise RollDndError("Both + and - are in the input.")
    if '+' in sides:
        alter_type = '+'
        sides, _, alter_amount = sides.partition('+')
    elif '-' in sides:
        alter_type = '-'
        sides, _, alter_amount = sides.partition('-')
    else:
        alter_type = None
        alter_amount = 0

    try:
        dice = int(dice)

        if dice < 1:
            raise ValueError
    except ValueError:
        raise RollDndError("{} is an invalid number of dice.".format(dice))
    try:
        sides = int(sides)

        if sides < 1:
            raise ValueError
    except ValueError:
        raise RollDndError("{} is an invalid number of sides.".format(sides))
    try:
        alter_amount = int(alter_amount)
    except ValueError:
        raise RollDndError("{} is an invalid number to add or subtract by."
                           .format(alter_amount))

    # Begin actual computation
    with ProcessPoolExecutor(1) as pool:
        loop = asyncio.get_event_loop()
        func = functools.partial(roll_dnd_accumulate, dice, sides)
        result = await loop.run_in_executor(pool, func)

    if alter_type == '+':
        result += alter_amount
    elif alter_type == '-':
        result -= alter_amount

    return result

async def roll_minmax(input_text: str) -> int:
    """Perform a min-max roll and return the result."""
    min_sides, _, max_sides = input_text.partition('-')
    try:
        min_sides = int(min_sides)
    except ValueError:
        raise RollMinMaxError("{} is not a valid minimum number of sides."
                              .format(min_sides))
    try:
        max_sides = int(max_sides)
    except ValueError:
        raise RollMinMaxError("{} is not a valid maximum number of sides."
                              .format(max_sides))
    if min_sides > max_sides:
        raise RollMinMaxError(
            "The minimum number of sides, {0}, is larger than the maximum " \
            "number of sides, {1}.".format(min_sides, max_sides)
        )

    return random.randrange(min_sides, max_sides + 1)

async def roll_max(input_text: str) -> int:
    """Perform a 1-max roll and return the result."""
    try:
        sides = int(input_text)
    except ValueError:
        raise RollMaxError("{} is not a valid number of sides."
                           .format(input_text))

    if sides < 1:
        raise RollMaxError("The number of sides to roll, {}, is less than 1."
                           .format(sides))

    return random.randrange(1, sides + 1)

def determine_roll_function(input_text: str):
    """Determine which type of roll the user wants and return a function.

    If 'd' in input_text, assume XdY(+/-)Z notation (old `dice`).
    If '-' in input_text and not 'd', assume min-max notation.
    Else, assume 1-max notation (old `roll`).
    """
    if 'd' in input_text:
        return roll_dnd
    if '-' in input_text:
        return roll_minmax

    return roll_max

ALIASES = ("roll", "dice")

async def command(ctx: Context) -> None:
    """`roll`, `dice`: Roll dice.

    There are three different ways to roll dice:
    `roll max`: Roll from 1 to max.
    `roll min-max`: Roll from min to max.
    `roll XdY[+/-Z]`: Roll X dice with Y sides, plus or minus Z (optional)

    If `roll` is used without specifying what to roll, 1-100 will be used.

    The final page of the output will state the input roll for the result.

    **Examples:**
        >>> `roll`
        :game_die: Rolling 1-100 for Hunter: **57**
        >>> `roll 10`
        :game_die: Rolling 1-10 for Hunter: **6**
        >>> `roll 7-11`
        :game_die: Rolling 7-11 for Hunter: **8**
        >>> `roll 4d6-1`
        :game_die: Rolling 4d6-1 for Hunter: **10**
    """
    # Default to a 1-100 roll
    roll = "100" if not ctx.args else ctx.args.pop()

    async with ctx.channel.typing():
        result = await determine_roll_function(roll)(roll)
        output = format(result, ',')

    pag = Paginator.from_text(
        text=output,
        title=":game_die: Roll Result",
        color=0xDD2E44, # color of :game_die:
        sep=',',
    )

    pag.add_page("Rolled {} for **{}**".format(roll, ctx.author.display_name))

    await pag.send(ctx.client, ctx.message)
