"""Command to look up color values"""
from typing import Optional

from discord import Embed

from turkbot.context import Context
from turkbot.exceptions import CommandError

class RGBColor:
    """A color value with red, green, and blue components.

    # Attributes
    - red (int): The red component (0-255)
    - green (int): The green component (0-255)
    - blue (int): The blue component (0-255)

    # Exceptions
    - ValueError: a color component was outside the valid range
    """
    names = {
        # These are the color names supported by web browsers when using them
        # for CSS colors.
        #
        # RGBColors cannot be instantiated here, so tuples will be used.
        "aliceblue": (0xF0, 0xF8, 0xFF),
        "antiquewhite": (0xFA, 0xEB, 0xD7),
        "aqua": (0x00, 0xFF, 0xFF),
        "aquamarine": (0x7F, 0xFF, 0xD4),
        "azure": (0xF0, 0xFF, 0xFF),
        "beige": (0xF5, 0xF5, 0xDC),
        "bisque": (0xFF, 0xE4, 0xC4),
        "black": (0x00, 0x00, 0x00),
        "blanchedalmond": (0xFF, 0xEB, 0xCD),
        "blue": (0x00, 0x00, 0xFF),
        "blueviolet": (0x8A, 0x2B, 0xE2),
        "brown": (0xA5, 0x2A, 0x2A),
        "burlywood": (0xDE, 0xB8, 0x87),
        "cadetblue": (0x5F, 0x9E, 0xA0),
        "chartreuse": (0x7F, 0xFF, 0x00),
        "chocolate": (0xD2, 0x69, 0x1E),
        "coral": (0xFF, 0x7F, 0x50),
        "cornflowerblue": (0x64, 0x95, 0xED),
        "cornsilk": (0xFF, 0xF8, 0xDC),
        "crimson": (0xDC, 0x14, 0x3C),
        "cyan": (0x00, 0xFF, 0xFF),
        "darkblue": (0x00, 0x00, 0x8B),
        "darkcyan": (0x00, 0x8B, 0x8B),
        "darkgoldenrod": (0xB8, 0x86, 0x0B),
        "darkgray": (0xA9, 0xA9, 0xA9),
        "darkgrey": (0xA9, 0xA9, 0xA9),
        "darkgreen": (0x00, 0x64, 0x00),
        "darkkhaki": (0xBD, 0xB7, 0x6B),
        "darkmagenta": (0x8B, 0x00, 0x8B),
        "darkolivegreen": (0x55, 0x6B, 0x2F),
        "darkorange": (0xFF, 0x8C, 0x00),
        "darkorchid": (0x99, 0x32, 0xCC),
        "darkred": (0x8B, 0x00, 0x00),
        "darksalmon": (0xE9, 0x96, 0x7A),
        "darkseagreen": (0x8F, 0xBC, 0x8F),
        "darkslateblue": (0x48, 0x3D, 0x8B),
        "darkslategray": (0x2F, 0x4F, 0x4F),
        "darkslategrey": (0x2F, 0x4F, 0x4F),
        "darkturquoise": (0x00, 0xCE, 0xD1),
        "darkviolet": (0x94, 0x00, 0xD3),
        "deeppink": (0xFF, 0x14, 0x93),
        "deepskyblue": (0x00, 0xBF, 0xFF),
        "dimgray": (0x69, 0x69, 0x69),
        "dimgrey": (0x69, 0x69, 0x69),
        "dodgerblue": (0x1E, 0x90, 0xFF),
        "firebrick": (0xB2, 0x22, 0x22),
        "floralwhite": (0xFF, 0xFA, 0xF0),
        "forestgreen": (0x22, 0x8B, 0x22),
        "fuchsia": (0xFF, 0x00, 0xFF),
        "gainsboro": (0xDC, 0xDC, 0xDC),
        "ghostwhite": (0xF8, 0xF8, 0xFF),
        "gold": (0xFF, 0xD7, 0x00),
        "goldenrod": (0xDA, 0xA5, 0x20),
        "gray": (0x80, 0x80, 0x80),
        "grey": (0x80, 0x80, 0x80),
        "green": (0x00, 0x80, 0x00),
        "greenyellow": (0xAD, 0xFF, 0x2F),
        "honeydew": (0xF0, 0xFF, 0xF0),
        "hotpink": (0xFF, 0x69, 0xB4),
        "indianred": (0xCD, 0x5C, 0x5C),
        "indigo": (0x4B, 0x00, 0x82),
        "ivory": (0xFF, 0xFF, 0xF0),
        "khaki": (0xF0, 0xE6, 0x8C),
        "lavender": (0xE6, 0xE6, 0xFA),
        "lavenderblush": (0xFF, 0xF0, 0xF5),
        "lawngreen": (0x7C, 0xFC, 0x00),
        "lemonchiffon": (0xFF, 0xFA, 0xCD),
        "lightblue": (0xAD, 0xD8, 0xE6),
        "lightcoral": (0xF0, 0x80, 0x80),
        "lightcyan": (0xE0, 0xFF, 0xFF),
        "lightgoldenrodyellow": (0xFA, 0xFA, 0xD2),
        "lightgray": (0xD3, 0xD3, 0xD3),
        "lightgrey": (0xD3, 0xD3, 0xD3),
        "lightgreen": (0x90, 0xEE, 0x90),
        "lightpink": (0xFF, 0xB6, 0xC1),
        "lightsalmon": (0xFF, 0xA0, 0x7A),
        "lightseagreen": (0x20, 0xB2, 0xAA),
        "lightskyblue": (0x87, 0xCE, 0xFA),
        "lightslategray": (0x77, 0x88, 0x99),
        "lightslategrey": (0x77, 0x88, 0x99),
        "lightsteelblue": (0xB0, 0xC4, 0xDE),
        "lightyellow": (0xFF, 0xFF, 0xE0),
        "lime": (0x00, 0xFF, 0x00),
        "limegreen": (0x32, 0xCD, 0x32),
        "linen": (0xFA, 0xF0, 0xE6),
        "magenta": (0xFF, 0x00, 0xFF),
        "maroon": (0x80, 0x00, 0x00),
        "mediumaquamarine": (0x66, 0xCD, 0xAA),
        "mediumblue": (0x00, 0x00, 0xCD),
        "mediumorchid": (0xBA, 0x55, 0xD3),
        "mediumpurple": (0x93, 0x70, 0xDB),
        "mediumseagreen": (0x3C, 0xB3, 0x71),
        "mediumturquoise": (0x48, 0xD1, 0xCC),
        "mediumvioletred": (0xC7, 0x15, 0x85),
        "midnightblue": (0x19, 0x19, 0x70),
        "mintcream": (0xF5, 0xFF, 0xFA),
        "mistyrose": (0xFF, 0xE4, 0xE1),
        "moccasin": (0xFF, 0xE4, 0xB5),
        "navajowhite": (0xFF, 0xDE, 0xAD),
        "navy": (0x00, 0x00, 0x80),
        "oldlace": (0xFD, 0xF5, 0xE6),
        "olive": (0x80, 0x80, 0x00),
        "olivedrab": (0x6B, 0x8E, 0x23),
        "orange": (0xFF, 0xA5, 0x00),
        "orangered": (0xFF, 0x45, 0x00),
        "orchid": (0xDA, 0x70, 0xD6),
        "palegoldenrod": (0xEE, 0xE8, 0xAA),
        "palegreen": (0x98, 0xFB, 0x98),
        "paleturquoise": (0xAF, 0xEE, 0xEE),
        "palevioletred": (0xDB, 0x70, 0x93),
        "papayawhip": (0xFF, 0xEF, 0xD5),
        "peachpuff": (0xFF, 0xDA, 0xB9),
        "peru": (0xCD, 0x85, 0x3F),
        "pink": (0xFF, 0xC0, 0xCB),
        "plum": (0xDD, 0xA0, 0xDD),
        "powderblue": (0xB0, 0xE0, 0xE6),
        "purple": (0x80, 0x00, 0x80),
        "rebeccapurple": (0x66, 0x33, 0x99),
        "red": (0xFF, 0x00, 0x00),
        "rosybrown": (0xBC, 0x8F, 0x8F),
        "royalblue": (0x41, 0x69, 0xE1),
        "saddlebrown": (0x8B, 0x45, 0x13),
        "salmon": (0xFA, 0x80, 0x72),
        "sandybrown": (0xF4, 0xA4, 0x13),
        "seagreen": (0x2E, 0x8B, 0x57),
        "seashell": (0xFF, 0xF5, 0xEE),
        "sienna": (0xA0, 0x52, 0x2D),
        "silver": (0xC0, 0xC0, 0xC0),
        "skyblue": (0x87, 0xCE, 0xEB),
        "slateblue": (0x6A, 0x5A, 0xCD),
        "slategray": (0x70, 0x80, 0x90),
        "slategrey": (0x70, 0x80, 0x90),
        "snow": (0xFF, 0xFA, 0xFA),
        "springgreen": (0x00, 0xFF, 0x7F),
        "steelblue": (0x46, 0x82, 0xB4),
        "tan": (0xD2, 0xB4, 0x8C),
        "teal": (0x00, 0x80, 0x80),
        "thistle": (0xD8, 0xBF, 0xD8),
        "tomato": (0xFF, 0x63, 0x47),
        "turquoise": (0x40, 0xE0, 0xD0),
        "violet": (0xEE, 0x82, 0xEE),
        "wheat": (0xF5, 0xDE, 0xB3),
        "white": (0xFF, 0xFF, 0xFF),
        "whitesmoke": (0xF5, 0xF5, 0xF5),
        "yellow": (0xFF, 0xFF, 0x00),
        "yellowgreen": (0x9A, 0xCD, 0x32),
    }

    def __init__(self, red: int, green: int, blue: int):
        if 0 <= red <= 255:
            self.red = red
        else:
            raise ValueError("red value outside 0-255: {}".format(red))

        if 0 <= green <= 255:
            self.green = green
        else:
            raise ValueError("green value outside 0-255: {}".format(green))

        if 0 <= blue <= 255:
            self.blue = blue
        else:
            raise ValueError("blue value outside 0-255: {}".format(blue))

    @classmethod
    def from_str(cls, what: str):
        """Create an RGBColor from a string.

        The string can be one of the following formats:
        - name
        - 0xRRGGBB
        - #RRGGBB
        - #RGB
        - RRGGBB

        # Parameters
        - what (str): the input string

        # Exceptions
        - ValueError: the string is in the wrong format
        """
        # Is `what` a color name?
        if what.lower() in cls.names:
            return cls(*cls.names[what.lower()])

        # Strip leading characters
        if what.startswith("0x"):
            what = what[2:]
        elif what.startswith("#"):
            what = what[1:]

        if len(what) == 6: # RRGGBB
            red = int(what[0:2], 16)
            green = int(what[2:4], 16)
            blue = int(what[4:6], 16)
        elif len(what) == 3: # RGB
            red = int(what[0], 16) * 0x11 # 0x3 -> 0x33
            green = int(what[1], 16) * 0x11
            blue = int(what[2], 16) * 0x11
        else:
            raise ValueError("invalid hex digit amount: {}".format(len(what)))

        return cls(red, green, blue)

    def __repr__(self):
        return ("RGBColor(red={0.red}, green={0.green}, blue={0.blue})"
                .format(self))

    def __str__(self):
        return "({0.red}, {0.green}, {0.blue})".format(self)

    def __int__(self):
        return (self.red << 16) + (self.green << 8) + self.blue # 0xRRGGBB

    def get_name(self, default: Optional[str] = None) -> str:
        """Get the RGBColor's name.

        If there is no name cooresponding to this RGBColor, `default` is
        returned.
        """
        for name, color in self.names.items():
            if color == (self.red, self.green, self.blue):
                return name

        return default

ALIASES = ("color",)

async def command(ctx: Context) -> None:
    """`color`: Look up a color by its value.

    An Embed will be sent with the color value in hex and decimal, the name of
    the color (if it has one), and having the color itself be the color of the
    Embed.

    **Usage:**
        in hex: `color 0xRRGGBB` or `#RRGGBB` or `#RGB` or `RRGGBB`
        in decimal: `color red green blue`
    """
    args = ctx.args
    try:
        if len(args) == 1: # hex
            color = RGBColor.from_str(args.pop())
        elif len(args) == 3: # decimal
            color = RGBColor(int(args.pop()), int(args.pop()), int(args.pop()))
        else:
            raise CommandError("You must supply either 1 or 3 arguments.")
    except ValueError as exc:
        raise CommandError(str(exc))

    embed = Embed()
    embed.colour = int(color)
    embed.add_field(name="Hex", value="0x{:06X}".format(int(color)))
    embed.add_field(name="Decimal", value=str(color))
    embed.add_field(name="Name", value=color.get_name(":x:"))

    await ctx.send(embed=embed)
