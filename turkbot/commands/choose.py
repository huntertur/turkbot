'''Command for making decisions'''
import random

from turkbot.util import restrict
from turkbot.context import Context
from turkbot.paginator import Paginator

ALIASES = ('choose',)

@restrict(min_args=2)
async def command(ctx: Context) -> None:
    """`choose`: Make a decision from a list of choices.

    **Usage:** `choose item1 item2 item3 [...] itemn`

    **Example:**
        >>> `choose apple orange banana grape kiwi watermelon`
        watermelon
    """
    options = list(ctx.args)
    result = random.choice(options)

    # example:
    # Available options:
    #
    # apple, orange, banana, grape, kiwi, watermelon
    choices_str = "Available options:\n\n{}".format(', '.join(options))

    pag = Paginator(pages=[result, choices_str],
                    title="{}'s decision".format(ctx.author.display_name))

    await pag.send(ctx.client, ctx.message)
