"""Convenience command for leaving a role"""
from turkbot.context import Context
from turkbot.commands.role import subcommand_leave
from turkbot.util import restrict

ALIASES = ("unsub", "iamnot")

@restrict(dms=False, min_args=1)
async def command(ctx: Context) -> None:
    """`unsub`, `iamnot`: Unsubscribe from a role.

    **Usage:** `unsub ROLE`

    This command is functionally the same as `role leave ROLE`.
    """
    await subcommand_leave(ctx)
