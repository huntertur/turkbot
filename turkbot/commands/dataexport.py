'''Command for exporting a user's data'''
from io import StringIO
from sqlite3 import Cursor
import json

from discord import Client, File, Member

from turkbot.context import Context
from turkbot.games import PlayersInfo

ALIASES = 'dataexport'

def get_pinlast(cursor: Cursor, user_id: int) -> list[dict[str]]:
    '''Get all `pinlast` data for a user.'''
    cursor.execute('SELECT * FROM pinlast WHERE pinner_id = ?', (user_id,))
    return [
        {
            'guild_id': row['guild_id'],
            'channel_id': row['channel_id'],
            'message_id': row['message_id'],
            'pinner_id': row['pinner_id'],
            'unpin_after': row['unpin_after'],
        }
        for row in cursor.fetchall()
    ]

def get_at_someone_disable(cursor: Cursor, user_id: int) -> list[dict[str]]:
    '''Get the `at_someone_disable` data for a user.'''
    cursor.execute('SELECT * FROM at_someone_disable WHERE user_id = ?',
                   (user_id,))
    return [
        {
            'user_id': row['user_id'],
        }
        for row in cursor.fetchall()
    ]

def get_tags(cursor: Cursor, user_id: int) -> list[dict[str]]:
    '''Get all `tags` data for a user.'''
    cursor.execute('SELECT * FROM tags WHERE author_id = ?', (user_id,))
    return [
        {
            'guild_id': row['guild_id'],
            'author_id': row['author_id'],
            'name': row['name'],
            'content': row['content'],
        }
        for row in cursor.fetchall()
    ]

def get_tags_bans(cursor: Cursor, user_id: int) -> list[dict[str]]:
    '''Get all `tags_bans` data for a user.'''
    cursor.execute('SELECT * FROM tags_bans WHERE user_id = ? OR issuer_id = ?',
                   (user_id, user_id))
    return [
        {
            'guild_id': row['guild_id'],
            'user_id': row['user_id'],
            'issuer_id': row['issuer_id'],
            'ban_time': row['ban_time'],
        }
        for row in cursor.fetchall()
    ]

def get_games_save(info: PlayersInfo, user_id: int) -> dict[str]:
    '''Get all games save data for a user.'''
    player = info.get(user_id)
    return player.to_dict()

def get_data(client: 'turkbot.TurkBot', cursor: Cursor,
             author_id: int) -> dict[str]:
    pinlast = get_pinlast(cursor, author_id)
    at_someone_disable = get_at_someone_disable(cursor, author_id)
    tags = get_tags(cursor, author_id)
    tags_bans = get_tags_bans(cursor, author_id)
    games_save = get_games_save(client.players, author_id)

    data = {
        'pinlast': pinlast,
        'at_someone_disable': at_someone_disable,
        'tags': tags,
        'tags_bans': tags_bans,
        'games_save': games_save,
    }

    return data

async def command(ctx: Context) -> None:
    '''`dataexport`: Export your data from the bot.

    The bot will send a JSON document containing any information linked to your
    account that is present in the bot's database.
    '''
    data = get_data(ctx.client, ctx.cursor, ctx.author.id)

    with StringIO() as tmp:
        json.dump(data, tmp)
        tmp.seek(0)
        await ctx.send(file=File(tmp, filename='dataexport.json'))
