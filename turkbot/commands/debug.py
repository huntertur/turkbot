"""Command that assists in debugging the bot, disabled by default"""
# This command is only enabled if DebugEnabled = 1 in config.ini
import os
import sys

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.util import restrict

async def subcommand_kill(ctx: Context) -> None:
    sys.exit()

async def subcommand_restart(ctx: Context) -> None:
    # XXX: terrible way to replace the bot process, but it kind of works
    if sys.platform.startswith("win"):
        os.execv(sys.executable, ["-3"] + sys.argv)
    else:
        os.execv(sys.executable, ["python3"] + sys.argv)

@restrict(min_args=1)
async def subcommand_eval(ctx: Context) -> None:
    # pylint: disable=W0123
    await ctx.send("```{0}```".format(eval(str(ctx.args))))

@restrict(min_args=1)
async def subcommand_aeval(ctx: Context) -> None:
    # pylint: disable=W0123
    await ctx.send("```{0}```".format(await eval(str(ctx.args))))

@restrict(min_args=1)
async def subcommand_pagetest(ctx: Context) -> None:
    what = ctx.args.pop()
    lines = ["- Line {}".format(i) for i in range(1, 300)]
    text = " ".join(["{:-^5}".format(i) for i in range(1, 300)])
    pages = ["Page 1", "Page 2", "Page 3", "Page 4", "Page 5"]

    if what == "lines":
        paginator = Paginator.from_lines(lines=lines, title="Test Paginator")
    elif what == "text":
        paginator = Paginator.from_text(text=text, title="Test Paginator")
    elif what == "pages":
        paginator = Paginator(pages=pages, title="Test Paginator")
    else:
        raise CommandError("Unknown Paginator test `{}`".format(what))

    await paginator.send(ctx.client, ctx.message)

SUBCOMMANDS = {
    "kill": subcommand_kill,
    "restart": subcommand_restart,
    "eval": subcommand_eval,
    "aeval": subcommand_aeval,
    "pagetest": subcommand_pagetest,
}

@restrict(min_args=1)
async def command(ctx: Context) -> None:
    """`debug`: Dangerous functions to assist with debugging the bot in testing

    This command is enabled *only* if `DebugEnabled = 1` in `config.ini`.

    **Usage:** `debug subcommand [args]`

    `debug kill`: Kill the bot's process.
    `debug restart`: Replace the bot's process.
    `eval [code]`: `eval` [code] and send the result.
    `aeval [code]`: `await eval` [code] and send the result.
    `pagetest lines/text/pages`: Test Paginator functionality.
    """
    sub = ctx.args.pop().lower()

    if sub not in SUBCOMMANDS:
        raise CommandError("Unknown subcommand, see `help debug` for help.")

    await SUBCOMMANDS.get(sub)(ctx)
