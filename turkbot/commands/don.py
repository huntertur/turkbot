"""Command that allows the user to play a Double or Nothing game"""
import random
from typing import Optional

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.games import EmbedGame

class DoubleOrNothingGame(EmbedGame):
    '''Helper class for creating and managing the `don` embed.'''
    __slots__ = ("player", "message", "reactor")

    def create_embed(self, preface: Optional[str] = None) -> discord.Embed:
        """Create the embed for the game based on the current game state.

        # Parameters
        preface (Optional[str]): if str, prepend this to the description
        """
        embed = discord.Embed()
        embed.colour = 0xFFD700 # gold
        embed.title = "Double or Nothing"
        embed.description = "" if preface is None else "{}\n\n".format(preface)

        if self.player.don.playing:
            embed.description += (
                ":moneybag: There is currently **${0}** on the table.\n"
                ":money_mouth: Take a chance at doubling the money to ${1}.\n"
                ":wave: Take the money and leave."
                .format(self.player.don.pot, self.player.don.pot * 2)
            )
        else:
            embed.description += (
                "Welcome to Double or Nothing.\n"
                ":star: Begin a new game."
            )

        embed.set_footer(
            text="{} can interact with this.".format(self.reactor.display_name),
            icon_url=self.reactor.avatar_url,
        )

        return embed

    valid_reactions = {
        # Reactions that act as controls
        "⭐", # :star: - Create new game if one isn't in progress
        "🤑", # :money_mouth: - Risk doubling the pot if game is in progress
        "👋", # :wave: - Leave and take the pot of money if game is in progress
        "🔄", # :arrows_counterclockwise: - Recreate the embed
    }

    async def refresh_embed(self) -> None:
        """Refresh the embed and its reactions."""
        await self.message.edit(embed=self.create_embed())

        # Fix reactions if they are not correct.
        # "Useless" try/except blocks needed to prevent adding or
        # removing already added or removed reactions from halting the
        # command from finishing
        if self.player.don.playing:
            try:
                await self.message.remove_reaction("⭐", self.message.author)
            except (discord.NotFound, discord.HTTPException):
                pass
            try:
                await self.message.add_reaction("🤑") # :money_mouth:
                await self.message.add_reaction("👋") # :wave:
            except (discord.NotFound, discord.HTTPException):
                pass
        else:
            try:
                await self.message.remove_reaction("🤑", self.message.author)
                await self.message.remove_reaction("👋", self.message.author)
            except (discord.NotFound, discord.HTTPException):
                pass
            try:
                await self.message.add_reaction("⭐") # :star:
            except (discord.NotFound, discord.HTTPException):
                pass

    async def send(self, client: 'turkbot.bot.TurkBot',
                   message: discord.Message) -> None:
        """Send the embed as a message and set up the reaction system.

        # Parameters
        client: the Discord client
        message: the message of the triggering command
        """
        self.reactor = message.author
        self.message = await message.channel.send(embed=self.create_embed())

        await self.message.add_reaction("🔄") # :arrows_counterclockwise:
        if self.player.don.playing:
            await self.message.add_reaction("🤑") # :money_mouth:
            await self.message.add_reaction("👋") # :wave:
        else:
            await self.message.add_reaction("⭐") # :star:

        while True:
            reaction, user = await client.wait_for(
                event="reaction_add",
                check=self._check,
            )

            # Edit the message, replacing the embed. Make sure the function
            # connected to the reaction is available according to the current
            # game state.

            if reaction.emoji == "⭐": # :star:
                # Create a new game if one isn't active.
                if self.player.don.playing:
                    # If the user reacted with this emoji even though a game
                    # already is in progress, the entire embed is probably out
                    # of date. Refresh it before continuing the loop.
                    await self.refresh_embed()
                    continue

                self.player.don.pot = 10
                self.player.money -= 10
                self.player.don.playing = True
                client.players.save()

                await self.message.edit(embed=self.create_embed(
                    preface=":star: Creating new game!",
                ))

                # Fix reactions
                await self.message.remove_reaction("⭐", self.message.author)
                await self.message.add_reaction("🤑")
                await self.message.add_reaction("👋")

            elif reaction.emoji == "🤑": # :money_mouth:
                # Risk doubling the pot or losing it all.
                if not self.player.don.playing:
                    await self.refresh_embed()
                    continue

                if random.randint(0, 1) == 0: # 0 = double, 1 = lose
                    self.player.don.pot *= 2

                    await self.message.edit(embed=self.create_embed(
                        preface=":ballot_box_with_check: Money doubled!",
                    ))
                else:
                    self.player.don.playing = False
                    self.player.don.played += 1
                    client.players.save()

                    await self.message.edit(embed=self.create_embed(
                        preface=":no_entry: The **${}** is forever lost!" \
                                .format(self.player.don.pot)
                    ))

                    # Fix reactions
                    await self.message.add_reaction("⭐")
                    await self.message.remove_reaction("🤑", self.message.author)
                    await self.message.remove_reaction("👋", self.message.author)

            elif reaction.emoji == "👋": # :wave:
                if not self.player.don.playing:
                    await self.refresh_embed()
                    continue

                self.player.don.playing = False
                self.player.don.played += 1
                self.player.money += self.player.don.pot

                if self.player.don.pot > self.player.don.best:
                    self.player.don.best = self.player.don.pot
                    best_msg = "Your best payout is now ${}!".format(
                        self.player.don.best,
                    )
                else:
                    best_msg = "You did not beat your best of ${}.".format(
                        self.player.don.best,
                    )

                client.players.save()

                await self.message.edit(embed=self.create_embed(
                    preface=":money_with_wings: You leave with ${}.\n{}".format(
                        self.player.don.pot,
                        best_msg
                    )
                ))

                # Fix reactions
                await self.message.add_reaction("⭐")
                await self.message.remove_reaction("🤑", self.message.author)
                await self.message.remove_reaction("👋", self.message.author)

            elif reaction.emoji == "🔄": # :arrows_counterclockwise:
                await self.refresh_embed()

            if message.guild.me.permissions_in(message.channel).manage_messages:
                # Remove the user's reaction if the bot has permission to make
                # it easier to choose another action without manually removing
                # it before.
                await reaction.message.remove_reaction(reaction.emoji, user)

ALIASES = ("don",)

async def command(ctx: Context) -> None:
    """`don`: Play a "Double-or-Nothing" game.

    **Usage:**
        `don` by itself: Play the game!
        `don stats`: Show your stats for the game.

    The game costs $10 to play, and this $10 acts as the initial pot. You can
    choose to risk doubling the pot, or walk away with the current pot. If
    you succeeded in doubling the pot, you get to decide again. If you fail,
    the whole contents of the pot will be lost!

    The game is controlled by a system of reactions. If you need to refresh the
    game if you, say, played the game in another channel and are returning to
    the older embed, react with :arrows_counterclockwise:.
    """
    if not ctx.args:
        game = DoubleOrNothingGame(ctx.player)
        await game.send(ctx.client, ctx.message)
    elif ctx.args.pop().lower() == "stats":
        await ctx.send(
            "You have **${0.money}**.\n"
            "Your best game had a pot of **${1.best}**.\n"
            "You have played this game **{1.played}** times."
            .format(ctx.player, ctx.player.don)
        )
    else:
        raise CommandError("Unknown subcommand, try `help don`.")
