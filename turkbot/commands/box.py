"""Command that creates a 3D ASCII art box from the input text"""
from turkbot.context import Context
from turkbot.exceptions import CommandError, MessageTooLargeError

ALIASES = ("box",)

async def command(ctx: Context) -> None:
    """`box`: Generate a 3D box from the input text.

    **Usage:** `box message`

    If you don't give this command a message, it will find the most recently
    sent message and process it instead.

    **Example:**
        >>> `box Apple`
        ```
            elppA
           l   pp
          p   p p
         p   l  l
        Apple   e
        p   l  l
        p   p p
        l   pp
        elppA
        ```
    """
    # newlines mess up output
    input_text = (await ctx.get_content()).replace("\n", " ")

    if "```" in input_text:
        # Discord does not allow the backtick to be escaped while inside a
        # code block, unlike in inline code (single backtick) where you can do
        # `` ` `` to escape a backrick, which is quite odd.
        raise CommandError("Having three backticks in a row is forbidden.")
    if len(input_text) > 22: # len(input_text) ** 2 > 2000 when it's >= 23
        raise MessageTooLargeError

    # Defining these variables increases performance by once calculating their
    # values once and makes me have to type less.
    inlen = len(input_text)
    dim = inlen * 2 - 1 # height and width of output
    dimrange = range(dim)

    outlist = [[" " for i in dimrange] for i in dimrange] # initialize
    output = []

    for i, char in enumerate(input_text):
        outlist[inlen - 1][i] = char # top of front
        outlist[dim - 1][inlen - 1 - i] = char # bottom of front
        outlist[inlen - 1 + i][0] = char # left of front
        outlist[dim - 1 - i][inlen - 1] = char # right of front
        outlist[0][dim - 1 - i] = char # top of back
        outlist[i][dim - 1] = char # right of back
        outlist[inlen - 1 - i][i] = char # top left connector
        outlist[i][dim - 1 - i] = char # top right connector
        outlist[dim - 1 -i][inlen - 1 + i] = char # bottom right connector

    for line in outlist:
        output.append("".join(line)) # list of lists -> list of strings

    await ctx.send("```{0}```".format("\n".join(output)))
