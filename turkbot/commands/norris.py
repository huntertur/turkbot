'''Command that fetches a random Chuck Norris joke'''
import html
import json

from aiohttp import ClientSession

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.internet import fetch
from turkbot.settings import get_config

ALIASES = 'chuck', 'norris'

async def command(ctx: Context) -> None:
    """`chuck`, `norris`: Fetch a random Chuck Norris joke.

    **Usage:** `chuck`
    """
    try:
        async with ctx.channel.typing(), ClientSession() as session:
            response = await fetch(session, get_config('NorrisJokeSource'))
            output = json.loads(response)
            if output['type'] != 'success':
                raise CommandError("The request to the server failed.")
            await ctx.send(html.unescape(output['value']['joke']))
    except (KeyError, json.JSONDecodeError):
        raise CommandError("The service did not provide a valid response.")
