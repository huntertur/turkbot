"""Command for pinning messages for a temporary duration"""
import asyncio
from datetime import datetime, timedelta
import time

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.util import get_last_text_message, NoMatches, restrict

async def pinlast_loop(client: 'turkbot.bot.TurkBot') -> None:
    """Begin checking temporary pins and unpin them if necessary.

    This is called once when the bot initially starts up.

    Every 60 seconds, the timestamp of each temporary pin is checked. If it is
    in the past, that message is unpinned (if it still exists) and is removed
    from the database.

    # Parameters
    - client: The Client to check the temporary pins of.
    """
    cursor = client.database.cursor()

    while True:
        current_time = time.time()

        cursor.execute("""
            SELECT rowid, guild_id, channel_id, message_id, unpin_after
            FROM pinlast
        """)

        for row in cursor.fetchall():
            if row["unpin_after"] > current_time:
                continue

            channel = client.get_channel(row["channel_id"])
            message = await channel.history().get(id=row["message_id"])

            if message is not None:
                try:
                    await message.unpin()
                except discord.HTTPException:
                    # The message exists since it is not None, and is still
                    # likely pinned. Try unpinning it again during the next
                    # minute and hope it works that time?
                    continue

            # Message unpinned or no longer exists, remove from database.
            cursor.execute("""
                DELETE FROM pinlast
                WHERE message_id = ?
            """, (row["message_id"],))

        await asyncio.sleep(60)

def create_timestamp(input_string: str) -> int:
    """Parse a string and add a duration of time to the current timestamp.

    The string format is a series of at least one space-separated fields
    consisting of an integer followed a by single letter denoting the unit of
    time for that field. At least one field must be present.

    For example, "5w 4d 3h 2m 1s" returns the current timestamp plus 3380521.

    # Field types
    | ? | Unit    |
    |---|---------|
    | w | Weeks   |
    | d | Days    |
    | h | Hours   |
    | m | Minutes |
    | s | Seconds |

    # Parameters
    - input_string (str): The string to parse.

    # Raises
    - ValueError: The input string was invalid.
    """
    weeks = 0
    days = 0
    hours = 0
    minutes = 0
    seconds = 0

    for field in input_string.split():
        # Proper fields are integers followed by a single letter denoting
        # the field's type.
        if len(field) < 2:
            raise ValueError("invalid field: `{}`".format(field))

        field_type = field[-1] # last character
        field_value = int(field[:-1]) # all but last character

        if field_value < 0: # negative
            raise ValueError("field value cannot be negative: `{}`"
                             .format(field_value))

        if field_type == 'w':
            weeks += field_value
        elif field_type == 'd':
            days += field_value
        elif field_type == 'h':
            hours += field_value
        elif field_type == 'm':
            minutes += field_value
        elif field_type == 's':
            seconds += field_value
        else:
            raise ValueError("invalid field type: `{}`".format(field_type))

    delta = timedelta(weeks=weeks, days=days, hours=hours, minutes=minutes,
                      seconds=seconds).total_seconds()

    if delta == 0:
        raise ValueError("total change in time was 0")

    return int(time.time() + delta)

@restrict(min_args=1)
async def subcommand_for(ctx: Context) -> None:
    # Each statement is wrapped in its own try/except block so the error message
    # sent to the channel is relevant to what happened. Unfortunately, it makes
    # the code ugly.

    try:
        pin_message = await get_last_text_message(ctx.channel)
    except NoMatches:
        raise CommandError("Could not find a text message in this channel.")

    try:
        timestamp = create_timestamp(str(ctx.args))
    except ValueError as exc:
        raise CommandError(exc.args[0]) # exception message

    try:
        await pin_message.pin()
    except discord.HTTPException:
        raise CommandError("Unable to pin message.")

    # Everything is OK, so start tracking this message. (The pin-checking loop
    # queries the database directly)

    ctx.cursor.execute("INSERT INTO pinlast VALUES (?, ?, ?, ?, ?)",
                       (ctx.guild.id, ctx.channel.id, pin_message.id,
                        ctx.author.id, timestamp))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_list(ctx: Context) -> None:
    lines = []
    current_time = datetime.now()
    ctx.cursor.execute("""
        SELECT rowid, channel_id, message_id, pinner_id, unpin_after
        FROM pinlast
        WHERE guild_id = ?
        ORDER BY rowid
    """, (ctx.guild.id,))

    for row in ctx.cursor.fetchall():
        when = None
        channel = ctx.client.get_channel(row["channel_id"])
        if current_time.timestamp() > row["unpin_after"]:
            when = "soon" # will be removed in < 60 sec
        else:
            when = str(
                datetime.fromtimestamp(row["unpin_after"]) - current_time
            )

        # <@pinner_id> makes a mention string
        # <#channel_id> makes a channel link
        lines.append("{r[rowid]}: Added by <@{r[pinner_id]}>. Sent in "
                     "<#{r[channel_id]}> by {m.author.mention} at "
                     "{m.created_at!s}. Expires {when}.".format(
                         r=row,
                         m=await channel.history().get(id=row["message_id"]),
                         when=when,
                     ))

    if not lines:
        raise CommandError("There are no temporary pins in this server.")

    pag = Paginator.from_lines(
        lines=lines,
        title="Temporary pins in {} ({})".format(ctx.guild.name, len(lines))
    )

    await pag.send(ctx.client, ctx.message)

@restrict(min_args=1)
async def subcommand_stop(ctx: Context) -> None:
    try:
        pin_id = int(ctx.args.pop())
    except ValueError:
        raise CommandError("Invalid pin ID. Try `pinlast list`.")

    # Need guild_id check to prevent stopping of other guilds' temporary pins.
    ctx.cursor.execute("""
        DELETE FROM pinlast
        WHERE rowid = ? AND guild_id = ?
    """, (pin_id, ctx.guild.id))

    if ctx.cursor.rowcount == 0:
        raise CommandError("There is no temporary pin with ID `{}`."
                           .format(pin_id))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=2)
async def subcommand_update(ctx: Context) -> None:
    try:
        pin_id = int(ctx.args.pop())
    except ValueError:
        raise CommandError("Invalid pin ID. Try `pinlast list`.")

    try:
        timestamp = create_timestamp(str(ctx.args))
    except ValueError as exc:
        raise CommandError(exc.args[0]) # exception message

    ctx.cursor.execute("""
        UPDATE pinlast
        SET unpin_after = ?
        WHERE rowid = ? AND guild_id = ?
    """, (timestamp, pin_id, ctx.guild.id))

    if ctx.cursor.rowcount == 0:
        raise CommandError("There is no temporary pin with ID `{}`."
                           .format(pin_id))

    await ctx.message.add_reaction("✅") # :white_check_mark:

SUBCOMMANDS = {
    "for": subcommand_for,
    "list": subcommand_list,
    "stop": subcommand_stop,
    "update": subcommand_update,
}

ALIASES = ("pinlast",)

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS pinlast (
            guild_id INTEGER NOT NULL,
            channel_id INTEGER NOT NULL,
            message_id INTEGER NOT NULL,
            pinner_id INTEGER NOT NULL,
            unpin_after INTEGER NOT NULL -- unix timestamp
        )
    """)

@restrict(dms=False, min_args=1, perms=['manage_messages'])
async def command(ctx: Context) -> None:
    """`pinlast`: Automatically unpin the previous message after some time.

    You must have the Manage Messages permission to use this command.

    Messages pinned using the `pinlast` command are automatically unpinned after
    a specified duration. Because the bot also handles the initial pinning as
    well, Discord will only state that the bot pinned the message, so the user
    who truly pinned the message is recorded by the bot.

    **Usage:**
        `pinlast for TIME`: Pin the last message for TIME amount of time.
        `pinlast list`: List all pinned messages tracked by the bot.
        `pinlast stop ID`: Stop tracking a pinned message. ID is from `list`.
        `pinlast update ID TIME`: Change when a message will be unpinned.

    **Time format:**
        Use numbers followed by a single letter for denoting the unit of time.
        `5w 4d 3h 2m 1s` represents 5 weeks, 4 days, 3 hours, 2 minutes, 1 sec
        from now. Any of these fields are optional, so you can just specify `1h`
        for 1 hour if you wish.
    """
    sub = ctx.args.pop().lower()

    if sub in SUBCOMMANDS:
        await SUBCOMMANDS.get(sub)(ctx)
    else:
        raise CommandError("Unknown subcommand, see `help pinlast` for help.")
