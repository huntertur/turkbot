"""Command that displays info about users not displayed in the client"""
import time

import discord

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.util import get_member

ALIASES = ("user", "userlookup")

async def command(ctx: Context) -> None:
    """`user`, `userlookup`: Look up a Discord user in the server.

    **Usage:** `user [name]`
        `name` can be a mention for the user, their name in the server, or part
        of the person's name in the server.

        If you don't give a `name`, you will look yourself up.
    """
    start_time = time.time()

    if ctx.args and isinstance(ctx.channel, discord.DMChannel):
        raise CommandError('Searching is disabled in Direct Messages.')
    if ctx.args:
        name = str(ctx.args)
        async with ctx.channel.typing():
            user = get_member(name, ctx.guild, strict=False)
    else:
        user = ctx.author

    embed = discord.Embed()

    embed.title = "{} ({})".format(user.display_name, user) # nickname then tag
    embed.set_thumbnail(url=user.avatar_url)
    embed.colour = user.color

    embed.add_field(
        name="User Since",
        # ex: Monday, January 1, 1900, 9:00:05 AM
        value="{:%A, %B %-d, %Y, %-I:%M:%S %p}".format(user.created_at),
    )

    if not isinstance(ctx.channel, discord.DMChannel):
        embed.add_field(
            name="Joined At",
            value="{:%A, %B %-d, %Y, %-I:%M:%S %p}".format(user.joined_at),
        )

    embed.add_field(name="Snowflake ID", value=str(user.id))

    # If a search doesn't need to be done since a name isn't given (the target
    # is the message author), don't show this.
    if ctx.args:
        embed.set_footer(text=("Search took {:.2f} ms"
                               .format((time.time() - start_time) * 1000)))

    await ctx.send(embed=embed)
