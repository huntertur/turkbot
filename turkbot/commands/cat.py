"""Command that fetches a random picture of a cat"""
from abc import ABC, abstractmethod
import json
from typing import Optional
import random
import time

from aiohttp import (ClientConnectionError, ClientConnectorError, ClientSession,
                     ContentTypeError)
from discord import Embed

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.settings import get_config

ALIASES = ("cat", "meow")

class CatProvider(ABC):
    '''A web service that provides random cat photos.'''
    def __init__(self, home_url: str, endpoint_config: str):
        self.home_url = home_url
        self.endpoint_url = get_config(endpoint_config)

    @abstractmethod
    async def get_next_url(self) -> Optional[str]:
        '''Fetch the next random image URL from the provider.'''
        raise NotImplementedError

class CataasProvider(CatProvider):
    '''Fetches pictures from https://cataas.com/.'''
    def __init__(self):
        super().__init__('https://cataas.com/', 'CataasSource')

    async def get_next_url(self) -> Optional[str]:
        # Due to caching, in order to prevent the same picture from appearing
        # over and over again, something unique needs to be appended to the
        # URL that won't affect the randomness.
        return f'{self.endpoint_url}?ignore_this={time.time()}'

class RandomCatProvider(CatProvider):
    '''Fetches pictures from https://random.cat/.'''
    def __init__(self):
        super().__init__('https://random.cat/', 'RandomCatSource')

    async def get_next_url(self) -> Optional[str]:
        async with ClientSession() as session:
            async with session.get(self.endpoint_url) as response:
                return (await response.json())['file']

class TheCatApiProvider(CatProvider):
    '''Fetches pictures from https://thecatapi.com/.'''
    def __init__(self):
        super().__init__('https://thecatapi.com/', 'TheCatApiSource')
        self.api_key: str = get_config('TheCatApiKey')

    async def get_next_url(self) -> Optional[str]:
        headers = {'X-API-Key': self.api_key}

        async with ClientSession() as session:
            async with session.get(self.endpoint_url,
                                   headers=headers) as response:
                return (await response.json())[0]['url']

PROVIDERS: list[CatProvider] = []

def create_providers() -> None:
    PROVIDERS.append(CataasProvider())
    PROVIDERS.append(RandomCatProvider())
    PROVIDERS.append(TheCatApiProvider())

async def get_image_and_source() -> tuple[str, str]:
    '''Randomly choose a working provider and get the next image.

    Returns: (Image URL, Provider home URL)

    Raises CommandError if all providers fail.
    '''
    if len(PROVIDERS) == 0:
        create_providers()

    random.shuffle(PROVIDERS)

    urls: Optional[tuple[str, str]] = None

    for provider in PROVIDERS:
        try:
            urls = (await provider.get_next_url(), provider.home_url)
            break
        except (ClientConnectionError, ClientConnectorError, ContentTypeError,
                KeyError, json.JSONDecodeError):
            continue

    if not urls:
        raise CommandError('No services returned a valid response. '
                           f'(Tried {len(PROVIDERS)} services.)')

    return urls

async def command(ctx: Context) -> None:
    """`cat`, `meow`: Fetch a random picture of a cat.

    **Usage:** `cat`
    """
    async with ctx.channel.typing():
        image_url, home_url = await get_image_and_source()

    output = Embed()
    output.set_image(url=image_url)
    output.set_footer(text=f'Meow! 🐱 Powered by {home_url}')
    output.colour = 0xFFCB4E # color of :cat: face

    await ctx.send(embed=output)
