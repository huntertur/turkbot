'''Command to get the bot's invite URL'''
import copy

from discord import Permissions
from discord.utils import oauth_url

from turkbot.context import Context

ALIASES = ('invite',)

PERMS = Permissions()
PERMS.add_reactions = True
PERMS.embed_links = True
PERMS.manage_messages = True
PERMS.manage_roles = True
PERMS.read_messages = True
PERMS.send_messages = True

MINI = copy.copy(PERMS)
MINI.manage_roles = False
MINI.manage_messages = False

async def command(ctx: Context) -> None:
    """`invite`: Get the bot's invite link.

    The link will have the necessary permissions embedded. However, in case the
    link does not grant all permissions, it is recommended to look at the list
    at https://gitlab.com/huntertur/turkbot.

    **Usage:** `invite`
    """
    msg = ("**turkbot invite links:**\n"
           "all permissions: <{}>\n"
           "minimum permissions: <{}>\n"
           "\n"
           "Sometimes, the invite links do not grant all requested "
           "permissions. Please check the table at "
           "<https://gitlab.com/huntertur/turkbot> to see if all permissions "
           "were granted.")

    await ctx.send(msg.format(oauth_url(ctx.client.user.id, PERMS),
                              oauth_url(ctx.client.user.id, MINI)))
