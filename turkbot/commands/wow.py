"""Command that looks up information about a World of Warcraft character"""
import json
import traceback

import discord

from turkbot.blizzard import WoWCharacterResponse
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.util import restrict

async def create_embed(char: WoWCharacterResponse) -> discord.Embed:
    """Create the embed for a character.

    # Parameters
    - char (WoWCharacterResponse): The character to create an embed for
    """
    output = discord.Embed()
    output.title = "{0}-{1} {2}".format(
        char.name,
        char.realm,
        char.get_guild_name_with_brackets(),
    )
    output.description = \
        "Level {0} {1} {2} {3}, iLvl {4}/{5}, {6} achievement points".format(
            char.level,
            char.gender,
            char.race,
            char.class_,
            char.equipped_item_level,
            char.average_item_level,
            char.achievement_points,
        )
    output.url = "https://worldofwarcraft.com/en-us/character/{0}/{1}".format(
        char.realm.replace(" ", "-"),
        char.name,
    )
    output.colour = char.get_class_color()
    output.set_thumbnail(url=char.thumbnail)

    for prof in char.professions:
        for tier in prof.tiers:
            await tier.retrieve_name()
        output.add_field(name=prof.name, value=str(prof))
    return output

ALIASES = ("wowarmory", "wow")

@restrict(min_args=1)
async def command(ctx: Context) -> None:
    """`wowarmory`, `wow`: Retrieve information about a WoW character.

    **Usage:** `wowarmory name[-realm]`
        `name` is the character's name.
        `realm` follows a dash to indicate the player's realm.
        If `realm` is not given, the default relam is used, if set.
    """
    char = ctx.args.pop()
    if "-" in char:
        name, _, realm = char.partition("-")
    else:
        name = char
        if isinstance(ctx.channel, discord.DMChannel):
            raise CommandError("No realm was specified.")
        realm = ctx.default_realm
        if not realm:
            raise CommandError('No default realm is set in this server.')

    try:
        async with ctx.channel.typing():
            character = await ctx.blizzapi.get_wow_profile(name, realm)
    except (KeyError, json.JSONDecodeError):
        traceback.print_exc()
        raise CommandError("Could not obtain data for that character.")
    else:
        await ctx.send(embed=await create_embed(character))
