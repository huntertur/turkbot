"""Command that fetches a random picture of a bird"""
import json

import aiohttp
from discord import Embed

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.internet import fetch
from turkbot.settings import get_config

ALIASES = ("bird", "tweet")

async def command(ctx: Context) -> None:
    """`bird`, `tweet`: Fetch a random picture of a bird.

    **Usage:** `bird`
    """
    try:
        async with ctx.channel.typing(), aiohttp.ClientSession() as session:
            # /tweet/random grabs a random image, but does not work when set as
            # the image in the embed, so it has to be done manually.
            response = await fetch(session, get_config("RandomBirdSource"))
            url = 'https://random.birb.pw/img/' + json.loads(response)['file']
    except (KeyError, json.JSONDecodeError):
        raise CommandError("The service did not provide a valid response.")

    output = Embed()
    output.colour = 0xDD2E44 # color of :bird:
    output.set_image(url=url)
    output.set_footer(text="Tweet! 🐦 Powered by https://random.birb.pw/")

    await ctx.send(embed=output)
