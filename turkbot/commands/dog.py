"""Command that fetches a random picture or video of a dog"""
import json

from aiohttp import ClientSession
from discord import Embed

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.internet import fetch
from turkbot.settings import get_config
from turkbot.util import is_image_url

ALIASES = ("dog", "woof")

async def command(ctx: Context) -> None:
    """`dog`, `woof`: Fetch a random picture or video of a dog.

    **Usage:** `dog`
    """
    try:
        async with ctx.channel.typing(), ClientSession() as session:
            response = await fetch(session, get_config("RandomDogSource"))
            file = json.loads(response)["url"]
    except (KeyError, json.JSONDecodeError):
        raise CommandError("The service did not provide a valid response.")

    output = Embed()
    output.set_footer(text="Woof! 🐶 Powered by https://random.dog/")
    output.colour = 0xCCD6DD # color of :dog: face

    if is_image_url(file):
        output.set_image(url=file)
    else:
        # While Embeds do have a video attribute, it does not look like
        # it can be set by a bot.
        output.description = ":play_pause: {}".format(file)

    await ctx.send(embed=output)
