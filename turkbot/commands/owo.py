"""Command that furry-izes a message."""
import random
import re

from turkbot.context import Context

# Matches a user mention string that contains an exclamation point and has been
# mutilated by the emoticon adder.
MENTION_PATTERN = re.compile(r"<@! (?:owo|uwu|oAo|o\/\/\/o|\^\\_\^|:3)(\d+)>")

EMOTES = [
    # The backslashes are for Discord.
    # pylint: disable=W1401
    "owo",
    "uwu",
    "oAo", # What even is this? A big, happy, smiling snout?
    "o///o",
    r"^\_^", # escape formatting
    ":3",
]

def furryize(text: str) -> str:
    """Furry-izes `text`.

    This includes adding emotes and replacing certain letter combinations.
    """
    output = ""

    # Replace letters with 'w's or other slurrings.
    for old, new in (('l', 'w'), ('r', 'w'), ('ui', 'wi'), ('ss', 'th'),
                     ('s', 'th'), ('x', 'ckth')):
        text = text.replace(old, new)

    # Have a chance of adding emotes after sentences or newlines. Don't double
    # up on emotes if there is a period followed by a newline.
    for char in text:
        if char != "\n":
            output += char

        if char in "\n.?!":
            if random.randint(1, 2) == 2: # 50% chance
                output += " {}".format(random.choice(EMOTES))

            if char == "\n":
                output += "\n"

    # Add an emote at the end all the time.
    output += " {}".format(random.choice(EMOTES))

    # Fix user mention strings
    output = re.sub(MENTION_PATTERN, r"<@!\1>", output)

    return output

ALIASES = ("owo", "uwu")

async def command(ctx: Context) -> None:
    """`owo`, `uwu`: Furry-ize a message.

    If you don't give this command a message, it will find the most recently
    sent message and furry-ize it instead.

    **Example:**
        >>> `The quick brown fox jumps over the lazy dog.`
        >>> `owo`
        The qwick bwown fox jumps ovew the wazy dog. oAo
    """
    await ctx.send(furryize(await ctx.get_content()))
