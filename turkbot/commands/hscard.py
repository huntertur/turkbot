"""Command that looks up a Hearthstone card by its name"""
import json
import logging
from typing import Optional

import discord

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.settings import get_config
from turkbot.util import restrict

LOGGER = logging.getLogger(__name__)

CLASS_COLORS = {
    # Like in turkbot/blizzard.py, but by name instead of number
    "WARRIOR": 0xC79C6E,
    "PALADIN": 0xF58CBA,
    "HUNTER": 0xABD473,
    "ROGUE": 0xFFF569,
    "PRIEST": 0xFFFFFF,
    "SHAMAN": 0x0070DE,
    "MAGE": 0x69CCF0,
    "WARLOCK": 0x9482C9,
    "DRUID": 0xFF7D0A,
}

class _HscardPrivate:
    '''Private data used by the `hscard` command.

    # Attributes
     - cards: dictionary of loaded cards
     - enabled: if the command is functional
    '''
    __slots__ = ('cards', 'enabled')

    def __init__(self):
        self.cards = []
        self.enabled = False

    def try_enable(self):
        if self.enabled:
            LOGGER.warning('call to re-enable HscardPrivate')
        try:
            with open(get_config('HscardsFilename')) as cards:
                for card in json.load(cards):
                    self.cards.append(HearthstoneCard(card))
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            LOGGER.error("cannot load HscardsFilename; hscard command disabled")
        else:
            self.enabled = True

class HearthstoneCard:
    '''A Hearthstone card.

    # Attributes
     - id_number: id of the card
     - name: name of the card
     - class_: what class the card belongs to
     - type_: type of the card
     - cost: mana cost of the card
     - attack: attack damage of the card
     - durability: durability of the card
     - health: health points of the card
     - text: description of the card
     - flavor: flavor text of the card
     - artist: artist of the card
    '''
    __slots__ = ('id_number', 'name', 'class_', 'type_', 'cost', 'attack',
                 'durability', 'health', 'text', 'flavor', 'artist')

    def __init__(self, card: dict):
        self.id_number: int = card['id']
        self.name: Optional[str] = (card['name']['enUS']
                                    if 'name' in card else None)
        self.class_: Optional[str] = (card['cardClass'].capitalize()
                                      if 'cardClass' in card else None)
        self.type_: Optional[str] = (card['type'].capitalize()
                                     if 'type' in card else None)
        self.cost: Optional[int] = card.get('cost')
        self.attack: Optional[int] = card.get('attack')
        self.durability: Optional[int] = card.get('durability')
        self.health: Optional[int] = card.get('health')
        self.text: Optional[str] = (card['text']['enUS']
                                    if 'text' in card else None)
        self.flavor: Optional[str] = (card['flavor']['enUS']
                                      if 'flavor' in card else None)
        self.artist: Optional[str] = card.get('artist')

PRIVATE = _HscardPrivate()

async def create_hearthstone_embed(cards: HearthstoneCard, key: str,
                                   value: str) -> discord.Embed:
    """Find the requested card and create an Embed with its information.

    # Parameters
    - cards (dict): the loaded cards information
    - key (str): what to search by (such as by the name of the card)
    - value (str): what to search for, based on key
    """
    card = None
    for possible_card in cards:
        if getattr(possible_card, key) == value:
            card = possible_card
            break
    if card is None:
        raise CommandError("Could not find Hearthstone card with {} {}"
                           .format(key, value))

    embed_title = ""
    embed_text = ""
    embed_footer = ""

    if card.name:
        embed_title += "{} - {} ".format(card.name, card.type_)
    if card.class_:
        embed_title += "({})".format(card.class_)
    if card.cost:
        embed_text += "{} :large_blue_diamond:. ".format(card.cost)
    if card.attack:
        embed_text += "{} :crossed_swords:. ".format(card.attack)
    if card.durability:
        embed_text += "{} :shield:. ".format(card.durability)
    if card.health:
        embed_text += "{} :heart:. ".format(card.health)
    if card.text:
        # [x]. $, and \n are oddities and removed. Formatting characters for
        # Markdown are escaped before coverting the HTML tags.
        text = card.text
        for old, new in (('[x]', ''), ('$', ''), ('\n', ' '),
                         ('*', r'\*'), ('_', r'\_'), ('`', r'\`'), ('~', r'\~'),
                         ('<b>', '**'), ('<i>', '*'), ('<u>', '__'),
                         ('</b>', '**'), ('</i>', '*'), ('</u>', '__')):
            text = text.replace(old, new)
        embed_text += text
    if card.flavor:
        embed_footer += "{} ".format(card.flavor)
    if card.artist:
        embed_footer += "Artist: {}".format(card.artist)

    embed = discord.Embed()
    embed.title = embed_title
    embed.description = embed_text
    embed.colour = CLASS_COLORS.get(card.class_, discord.Embed.Empty)
    embed.set_footer(text=embed_footer)
    embed.set_image(url=('https://art.hearthstonejson.com/v1/render/latest/'
                         'enUS/256x/{}.png').format(card.id_number))

    return embed

ALIASES = ("hscard", "hs")

@restrict(min_args=1)
async def command(ctx: Context) -> None:
    """`hscard`, `hs`: Find Hearthstone card by name.

    **Usage:** `hscard name`

    The bot sends an embed with the attributes, description, and picture of the
    requested card if it exists.
    """
    if not PRIVATE.enabled:
        PRIVATE.try_enable()
    if not PRIVATE.enabled:
        raise CommandError("This command is disabled because a required file "
                           "could not be loaded.")
    await ctx.channel.trigger_typing()
    await ctx.send(embed=await create_hearthstone_embed(PRIVATE.cards, 'name',
                                                        str(ctx.args)))
