"""Command to get the guild's icon"""
from discord import Embed

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.util import restrict

ALIASES = ("servericon", "sicon")

@restrict(dms=False)
async def command(ctx: Context) -> None:
    """`servericon`, `sicon`: Get a high-resolution version of the server icon.

    **Usage:** `servericon`
    """
    if ctx.guild.icon is None:
        raise CommandError("This server does not have an icon.")

    embed = Embed()
    embed.title = "{}'s icon".format(ctx.guild.name)
    embed.set_image(url=ctx.guild.icon_url)

    await ctx.send(embed=embed)
