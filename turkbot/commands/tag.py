"""Command for interacting with message macros"""
from copy import deepcopy

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError, BannedError, NoPermissionError
from turkbot.paginator import Paginator
from turkbot.util import get_member, is_image_url, restrict

def get_content(ctx: Context) -> str:
    '''Get the text or image to make as the content of a tag.

    There doesn't need to be a message or image URL given if an image is
    uploaded. However, along with the attachment being an image, in order to
    prevent ambiguity, there must only be one attachment, and that attachment
    must point to an image URL.
    '''
    if ctx.message.attachments:
        if ctx.args:
            raise CommandError("You cannot upload an image and add text at "
                               "the same time.")
        if len(ctx.message.attachments) > 1:
            raise CommandError("Only one image may be uploaded at a time.")

        content = ctx.message.attachments[0].url
        if not is_image_url(content):
            raise CommandError("The attachment was not a supported image type.")
    else:
        content = str(ctx.args)
        if not content:
            raise CommandError("No content was specified.")

    return content

def is_banned(client: 'turkbot.bot.TurkBot', message: discord.Message) -> bool:
    """Check if the message author is banned in the guild."""
    cursor = client.database.cursor()
    cursor.execute("""
        SELECT 1
        FROM tags_bans
        WHERE guild_id = ? AND user_id = ?
        LIMIT 1
    """, (message.guild.id, message.author.id))

    return cursor.fetchone() is not None

def is_author(client: 'turkbot.bot.TurkBot', message: discord.Message,
              name: str) -> bool:
    """Check if the message author is the author of the tag with name `name`."""
    cursor = client.database.cursor()
    cursor.execute("""
        SELECT 1
        FROM tags
        WHERE guild_id = ? AND author_id = ? AND name = ?
        COLLATE NOCASE
        LIMIT 1
    """, (message.guild.id, message.author.id, name))

    return cursor.fetchone() is not None

@restrict(min_args=1)
async def subcommand_add(ctx: Context) -> None:
    if is_banned(ctx.client, ctx.message):
        raise BannedError

    name = ctx.args.pop()

    if name in SUBCOMMANDS:
        raise CommandError("The keyword `{}` is in use by a subcommand."
                           .format(name))

    # Make sure the tag name isn't already in use
    ctx.cursor.execute("""
        SELECT 1
        FROM tags
        WHERE guild_id = ? AND name = ?
        COLLATE NOCASE
        LIMIT 1
    """, (ctx.guild.id, name))
    if ctx.cursor.fetchone() is not None:
        raise CommandError("The keyword `{}` is already registered."
                           .format(name))

    ctx.cursor.execute("INSERT INTO tags VALUES (?, ?, ?, ?)",
                       (ctx.guild.id, ctx.author.id, name, get_content(ctx)))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1, max_args=1)
async def subcommand_remove(ctx: Context) -> None:
    if is_banned(ctx.client, ctx.message):
        raise BannedError

    name = ctx.args.pop()

    if not ctx.author.permissions_in(ctx.channel).manage_messages \
            and not is_author(ctx.client, ctx.message, name):
        raise NoPermissionError

    ctx.cursor.execute("""
        DELETE FROM tags
        WHERE guild_id = ? AND name = ?
        COLLATE NOCASE
    """, (ctx.guild.id, name))

    if ctx.cursor.rowcount == 0:
        raise CommandError("The keyword `{}` is not registered.".format(name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1)
async def subcommand_update(ctx: Context) -> None:
    if is_banned(ctx.client, ctx.message):
        raise BannedError

    name = ctx.args.pop()

    if not ctx.author.permissions_in(ctx.channel).manage_messages \
            and not is_author(ctx.client, ctx.message, name):
        raise NoPermissionError

    ctx.cursor.execute("""
        UPDATE tags
        SET content = ?
        WHERE guild_id = ? AND name = ?
        COLLATE NOCASE
    """, (get_content(ctx), ctx.guild.id, name))

    if ctx.cursor.rowcount == 0:
        raise CommandError("The keyword `{}` is not registered.".format(name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=2, max_args=2)
async def subcommand_rename(ctx: Context) -> None:
    if is_banned(ctx.client, ctx.message):
        raise BannedError

    old_name = ctx.args.pop()
    new_name = ctx.args.pop()

    if not ctx.author.permissions_in(ctx.channel).manage_messages \
            and not is_author(ctx.client, ctx.message, old_name):
        raise NoPermissionError

    if new_name in SUBCOMMANDS:
        raise CommandError("The keyword `{}` is in use by a subcommand."
                           .format(new_name))

    # Make sure the new tag name isn't already in use
    ctx.cursor.execute("""
        SELECT 1
        FROM tags
        WHERE guild_id = ? AND name = ?
        COLLATE NOCASE
        LIMIT 1
    """, (ctx.guild.id, new_name))
    if ctx.cursor.fetchone() is not None:
        raise CommandError("The keyword `{}` is already registered."
                           .format(new_name))

    ctx.cursor.execute("""
        UPDATE tags
        SET name = ?
        WHERE guild_id = ? AND name = ?
        COLLATE NOCASE
    """, (new_name, ctx.guild.id, old_name))

    if ctx.cursor.rowcount == 0:
        raise CommandError("The keyword `{}` is not registered."
                           .format(old_name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1, perms=['manage_messages'])
async def subcommand_ban(ctx: Context) -> None:
    name = str(ctx.args)
    member = get_member(name, ctx.guild, strict=True)
    # Check if the member is already banned
    ctx.cursor.execute("""
        SELECT 1
        FROM tags_bans
        WHERE guild_id = ? AND user_id = ?
        LIMIT 1
    """, (ctx.guild.id, member.id))
    if ctx.cursor.fetchone() is not None:
        raise CommandError("The member `{}` is already banned."
                           .format(member.display_name))

    ctx.cursor.execute("INSERT INTO tags_bans VALUES (?,?,?, datetime('now'))",
                       (ctx.guild.id, member.id, ctx.author.id))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1, perms=['manage_messages'])
async def subcommand_unban(ctx: Context) -> None:
    name = str(ctx.args)
    member = get_member(name, ctx.guild, strict=True)

    ctx.cursor.execute("""
        DELETE FROM tags_bans
        WHERE guild_id = ? AND user_id = ?
    """, (ctx.guild.id, member.id))

    if ctx.cursor.rowcount == 0:
        raise CommandError("The member `{}` is not banned."
                           .format(member.display_name))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(perms=['manage_messages'])
async def subcommand_bans(ctx: Context) -> None:
    lines = []
    ctx.cursor.execute("""
        SELECT user_id, issuer_id, ban_time
        FROM tags_bans
        WHERE guild_id = ?
    """, (ctx.guild.id,))

    for row in ctx.cursor.fetchall():
        # Create mention strings from the IDs.
        lines.append("- <@{0[user_id]}> by <@{0[issuer_id]}> on {0[ban_time]}"
                     .format(row))

    if not lines:
        raise CommandError("There are no members banned from the tag system.")

    pag = Paginator.from_lines(
        lines=lines,
        title="Users banned from the tag system ({})".format(len(lines)),
    )

    await pag.send(ctx.client, ctx.message)

async def subcommand_list(ctx: Context) -> None:
    lines = []
    ctx.cursor.execute("""
        SELECT author_id, name
        FROM tags
        WHERE guild_id = ?
        ORDER BY name
    """, (ctx.guild.id,))

    for row in ctx.cursor.fetchall():
        # Make ID into a mention string to show the name
        lines.append("- **{0[name]}** added by <@{0[author_id]}>".format(row))

    if not lines:
        raise CommandError("There are no tags in this server.")

    pag = Paginator.from_lines(
        lines=lines,
        title="Tags in {} ({})".format(ctx.guild.name, len(lines)),
    )

    await pag.send(ctx.client, ctx.message)

@restrict(min_args=1, max_args=1)
async def subcommand_show(ctx: Context) -> None:
    name = ctx.args.pop()

    # Get the correct captialization of the tag name too.
    ctx.cursor.execute("""
        SELECT name, content
        FROM tags
        WHERE guild_id = ? AND name = ?
        COLLATE NOCASE
        LIMIT 1
    """, (ctx.guild.id, name))

    try:
        row = ctx.cursor.fetchone()
        content = row['content']
        name = row['name']
    except TypeError: # no rows returned
        raise CommandError("The keyword `{}` is not registered.".format(name))

    embed = discord.Embed()

    if is_image_url(content):
        embed.set_image(url=content)
        embed.title = ":frame_photo: " + name
    else:
        embed.description = content
        embed.title = ":speech_left: " + name

    await ctx.send(embed=embed)

SUBCOMMANDS = {
    "add": subcommand_add,
    "remove": subcommand_remove,
    "update": subcommand_update,
    "rename": subcommand_rename,
    "ban": subcommand_ban,
    "unban": subcommand_unban,
    "bans": subcommand_bans,
    "list": subcommand_list,
    "show": subcommand_show,
}

ALIASES = ("tag",)

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS tags (
            guild_id INTEGER NOT NULL,
            author_id INTEGER NOT NULL,
            name TEXT NOT NULL,
            content TEXT NOT NULL
        )
    """)
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS tags_bans (
            guild_id INTEGER NOT NULL,
            user_id INTEGER NOT NULL,
            issuer_id INTEGER NOT NULL,
            ban_time TEXT NOT NULL
        )
    """)

@restrict(dms=False, min_args=1)
async def command(ctx: Context) -> None:
    """`tag`: View and create message macros.

    A tag is a keyword that points to a larger message or an image. To view a
    tag, simply type `tag NAME`, where `NAME` is the name of the tag.

    Users who create a tag or have the Manage Messages permission are able to
    edit or delete it. Users can also be banned from the tag system, which
    prevents them from doing anything other than viewing tags or listing them.

    **Usage:**
        `tag add NAME CONTENT`: Add a tag with name `NAME`, where `CONTENT` is
            either a message or a URL to an image.
        `tag add NAME`: If an image is uploaded too, make the new tag point to
            that image.
        `tag remove NAME`: Remove tag with name `NAME`.
        `tag update NAME CONTENT`: Replace the content of tag `NAME`.
        `tag update NAME`: Replace `NAME`'s content, if a replacement image is
            uploaded too.
        `tag rename OLD NEW`: Rename tag `OLD` to `NEW`.
        `tag ban USER`: Prevent `USER` from editing or adding tags.
        `tag unban USER`: Re-allow `USER` to edit or add tags.
        `tag bans`: List all users banned from the tag system.
        `tag list`: View the names of all tags in this server.
        `tag show NAME` or `tag NAME`: View the tag with name `NAME`.
    """
    original_tokens = deepcopy(ctx.args.tokens)
    sub = ctx.args.pop().lower()

    if sub in SUBCOMMANDS:
        await SUBCOMMANDS.get(sub)(ctx)
    else: # implicit `show`
        ctx.args.tokens = original_tokens
        await subcommand_show(ctx)
