'''Command to generate QR codes'''
from io import BytesIO

from discord import File
import qrcode

from turkbot.context import Context

ALIASES = ('qrcode', 'qr')

async def command(ctx: Context) -> None:
    """`qrcode`, `qr`: Generate a QR (Quick Response) code.

    The code can be easily scanned with a phone camera. QR codes can contain
    text, URLs, product numbers, and much more.

    **Usage:** `qrcode input`
    """
    with BytesIO() as tmp:
        qrcode.make(await ctx.get_content()).save(tmp, 'PNG')
        tmp.seek(0)
        await ctx.send(file=File(tmp, "output.png"))
