'''Command for querying and changing the bot's command prefix per-server'''
import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.settings import get_config
from turkbot.util import restrict

ALIASES = ('prefix',)

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS prefixes (
            guild_id INTEGER NOT NULL UNIQUE,
            prefix TEXT -- NULL means use the default prefix
        )
    ''')

@restrict(dms=False, max_args=1)
async def command(ctx: Context) -> None:
    """`prefix`: Query or change the bot's command prefix in the server.

    You must have the Manage Server permission to change the bot's prefix.

    Without a prefix set, the bot will use the default command prefix in this
    server. Even with a custom prefix set, this command will always be available
    using the default prefix so that the prefix can be reset or changed to a
    new one if the one it is set to is forgotten or impractical.

    **Usage:**
        `prefix`: State the bot's prefix in this server.
        `prefix PREFIX`: Change the command prefix to `PREFIX`.
        `prefix reset`: Reset the prefix to the default.
    """
    arg = ctx.args.pop()

    if not arg: # state the prefix
        prefix = ctx.cursor.execute('''
            SELECT prefix
            FROM prefixes
            WHERE guild_id = ?
        ''', (ctx.guild.id,)).fetchone()['prefix']

        if not prefix:
            await ctx.send("This server is using the default prefix of `{}`."
                           .format(get_config('CommandPrefix')))
        else:
            await ctx.send(f"This server is using the prefix `{prefix}`.")
    else:
        if not ctx.author.permissions_in(ctx.channel).manage_guild:
            raise CommandError("You must have the Manage Server permission.")

        ctx.cursor.execute('''
            UPDATE prefixes
            SET prefix = ?
            WHERE guild_id = ?
        ''', (arg if arg != 'reset' else None, ctx.guild.id))

        await ctx.message.add_reaction("✅") # :white_check_mark:
