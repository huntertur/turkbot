'''Command that 'translates' a message to Nerglish'''
import random

from turkbot.context import Context

ALIASES = ('murloc',)

# adapted from https://wow.gamepedia.com/Nerglish
WORDS = {
    'move': 'flllurlog',
    'walk': 'flllurlog',
    'feral': 'furl',
    'gordunni': 'grrundee',
    'clan': 'klun',
    'night elf': 'kuldurrreee',
    'the murloc gods have blessed us': 'mglrmglmglmgl',
    'fishing nets': 'mgrrrl',
    'thirsty': 'mllgggrrrr',
    'with': 'mmgr',
    'tasty': 'mmm',
    'good': 'mmmm',
    'right': 'mmmml',
    'correct': 'mmmml',
    'magic': 'mrrrggk',
    'aunt': 'mmmrrggllm',
    'uncle': 'mmmrrgglm',
    'friend': 'mmmrrglllm',
    'murloc': 'mmmurlok',
    'yes': 'mrgle',
    'i will kill you': 'mrgllll glrrm gl',
    'sorry': 'mrrrgll',
    'ringworm': 'murguhlum',
    'and': 'n',
    'no': 'nk',
    'not': 'nk',
    'ogre': 'rrrgrrr',
    'sing': 'shng',
    'scar': 'skr',
    'spring': 'srng',
    'honor': 'uuua',
}

LETTERS = 'adefghklmnorsu'
GOOD_LETTERS = 'aghlmru' # generates better-looking Nerglish

def nerglish(text: str) -> str:
    ''''Translates' `text` to Nerglish.

    After replacing known phrases, it changes any non-Neglish letters to
    legal ones.
    '''
    text = text.lower()

    for eng, nerg in WORDS.items():
        text = text.replace(eng, nerg)

    output = []

    for char in text:
        # isalpha used to prevent replacing punctuation, whitespace, numbers
        if not char.isalpha() or char in LETTERS:
            output.append(char)
        else:
            output.append(random.choice(GOOD_LETTERS))

    lowercase = ''.join(output)

    # str.capitalize doesn't work on each sentence, but the whole string, so
    # the translated string will be scanned and captialized manually
    output = []
    capitalize_next = True

    for char in lowercase:
        if char.isalpha() and capitalize_next:
            output.append(char.upper())
            capitalize_next = False
            continue

        output.append(char)

        if char in '\n.?!':
            capitalize_next = True

    return ''.join(output)

async def command(ctx: Context) -> None:
    """`murloc`: 'Translate' a message to Nerglish.

    If you don't give this command a message, it will find the most recently
    sent message and 'translate' it instead.
    """
    await ctx.send(nerglish(await ctx.get_content()))
