'''Command that fetches a random picture of a bat'''
from aiohttp import ClientError, ClientSession
from discord import Embed

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.settings import get_config

ALIASES = 'bat', 'squeak'

async def command(ctx: Context) -> None:
    '''`bat`, `squeak`: Fetch a random picture of a bat.

    **Usage:** `bat`
    '''
    base = get_config('RandomBatSource')
    image_url: str

    try:
        async with ctx.channel.typing(), ClientSession() as session:
            async with session.get(f'{base}/api/random') as response:
                image_url = f'{base}/{await response.text()}'
    except ClientError:
        raise CommandError('The service did not provide a valid response.')

    output = Embed()
    output.set_image(url=image_url)
    output.set_footer(text=f'Squeak! 🦇 Powered by {base}/Index')
    output.colour = 0x94ABB5 # color of :bat: face

    await ctx.send(embed=output)
