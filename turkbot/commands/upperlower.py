"""Command that allows users to a play a simple card game"""
import random
from typing import Optional

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.games import CardAcesHigh, EmbedGame
from turkbot.paginator import Paginator

# Generate a normal deck of cards and payout table
CARDS = [CardAcesHigh(suit, rank)
         for suit in ("H", "D", "S", "C")
         for rank in range(1, 14)]
PAYOUTS = [int(x ** 1.8) + 13 for x in range(53)] # max payout: $1239

class UpperLowerGame(EmbedGame):
    '''Helper class for creating and managing the `upperlower` embed.'''
    __slots__ = ("player", "message", "reactor")

    def create_embed(self, preface: Optional[str] = None) -> discord.Embed:
        """Create the embed for the game based on the current game state.

        # Parameters
        preface (Optional[str]): if str, prepend this to the description
        """
        embed = discord.Embed()
        embed.colour = 0xFFD700 # gold
        embed.title = "Upper / Lower"
        embed.description = "" if preface is None else "{}\n\n".format(preface)

        if self.player.ul.playing:
            embed.description += (
                ":flower_playing_cards: Current card: {0!s}.\n"
                "This is card number {1}, with {2} remaining.\n"
                ":arrow_double_up: Guess the next card is higher or equal.\n"
                ":arrow_double_down: Guess the next card is lower or equal."
                .format(self.player.ul.current_card,
                        52 - len(self.player.ul.cards_left),
                        len(self.player.ul.cards_left))
            )
        else:
            embed.description += ("Welcome to Upper / Lower.\n"
                                  ":star: Begin a new game.")

        embed.set_footer(
            text="{} can interact with this.".format(self.reactor.display_name),
            icon_url=self.reactor.avatar_url,
        )

        return embed

    valid_reactions = {
        # Reactions that act as controls
        "🔄", # :arrows_counterclockwise: - Refresh the embed
        "⭐", # :star: - Create new game if one isn't in progress
        "⏫", # :arrow_double_up: - Guess next card is >= in value
        "⏬", # :arrow_double_down: - Guess next card is <= in value
    }

    async def refresh_embed(self) -> None:
        """Refresh the embed and its reactions."""
        await self.message.edit(embed=self.create_embed())

        # Fix reactions if they are not correct.
        # "Useless" try/except blocks needed to prevent adding or
        # removing already added or removed reactions from halting the
        # command from finishing
        if self.player.ul.playing:
            try:
                await self.message.remove_reaction("⭐", self.message.author)
            except (discord.NotFound, discord.HTTPException):
                pass
            try:
                await self.message.add_reaction("⏫") # :arrow_double_up:
                await self.message.add_reaction("⏬") # :arrow_double_down:
            except (discord.NotFound, discord.HTTPException):
                pass
        else:
            try:
                await self.message.remove_reaction("⏫", self.message.author)
                await self.message.remove_reaction("⏬", self.message.author)
            except (discord.NotFound, discord.HTTPException):
                pass
            try:
                await self.message.add_reaction("⭐") # :star:
            except (discord.NotFound, discord.HTTPException):
                pass

    async def finish(self, client: 'turkbot.bot.TurkBot', reason: str) -> None:
        """Handle finishing the game.

        # Parameters
        reason (str): reason for finishing (can be "empty deck", "wrong")
        """
        # XXX: use enum instead of str here
        if reason == "empty deck":
            preface = ":o There are no more cards left in the deck!\n\n"
        elif reason == "wrong":
            preface = ":no_entry: Incorrect! Next card was {!s}.\n\n" \
                      .format(self.player.ul.current_card)
        else:
            preface = ""

        self.player.ul.playing = False
        self.player.ul.played += 1
        cards_left = 51 - len(self.player.ul.cards_left)
        winnings = PAYOUTS[cards_left]
        self.player.money += winnings

        preface += ":money_with_wings: You leave with ${}.\n".format(winnings)

        if cards_left > self.player.ul.best:
            self.player.ul.best = cards_left
            preface += "Your best number of guesses is now {}!".format(
                self.player.ul.best,
            )
        else:
            preface += "You did not beat your best of {}.".format(
                self.player.ul.best,
            )

        client.players.save()

        await self.message.edit(embed=self.create_embed(preface=preface))

        # Fix reactions
        await self.message.remove_reaction("⏫", self.message.author)
        await self.message.remove_reaction("⏬", self.message.author)
        await self.message.add_reaction("⭐") # :star:

    async def send(self, client: 'turkbot.bot.TurkBot',
                   message: discord.Message) -> None:
        """Send the embed as a message and set up the reaction system.

        # Parameters
        client: the bot
        message: the message of the triggering command
        """
        self.reactor = message.author
        self.message = await message.channel.send(embed=self.create_embed())

        await self.message.add_reaction("🔄") # :arrows_counterclockwise:
        if self.player.ul.playing:
            await self.message.add_reaction("⏫") # :arrow_double_up:
            await self.message.add_reaction("⏬") # :arrow_double_down:
        else:
            await self.message.add_reaction("⭐") # :star:

        while True:
            reaction, user = await client.wait_for(event="reaction_add",
                                                   check=self._check)

            # Edit the message, replacing the embed. Make sure the function
            # connected to the reaction is available according to the current
            # game state.

            if reaction.emoji == "⭐": # :star:
                # Create a new game if one isn't active.
                if self.player.ul.playing:
                    # If the user reacted with this emoji even though a game
                    # already is in progress, the entire embed is probably out
                    # of date. Refresh it before continuing the loop.
                    await self.refresh_embed()
                    continue

                self.player.ul.cards_left = random.sample(CARDS, len(CARDS))
                self.player.ul.current_card = self.player.ul.cards_left.pop()
                self.player.ul.playing = True
                self.player.money -= 20
                client.players.save()

                await self.message.edit(embed=self.create_embed(
                    preface=":star: Creating new game!",
                ))

                # Fix reactions
                await self.message.remove_reaction("⭐", self.message.author)
                await self.message.add_reaction("⏫") # :arrow_double_up:
                await self.message.add_reaction("⏬") # :arrow_double_down:

            elif reaction.emoji == "⏫": # :arrow_double_up:
                # Guess if the next card is higher or equal in value.
                if not self.player.ul.playing:
                    await self.refresh_embed()
                    continue

                old_card = self.player.ul.current_card
                self.player.ul.current_card = self.player.ul.cards_left.pop()

                if self.player.ul.current_card >= old_card:
                    if self.player.ul.cards_left:
                        await self.message.edit(embed=self.create_embed(
                            preface=":ballot_box_with_check: Correct!",
                        ))
                    else:
                        await self.finish(client, reason="empty deck")
                else:
                    await self.finish(client, reason="wrong")

            elif reaction.emoji == "⏬": # :arrow_double_down:
                # Guess if the next card is lower or equal in value.
                if not self.player.ul.playing:
                    await self.refresh_embed()
                    continue

                old_card = self.player.ul.current_card
                self.player.ul.current_card = self.player.ul.cards_left.pop()

                if self.player.ul.current_card <= old_card:
                    if self.player.ul.cards_left:
                        await self.message.edit(embed=self.create_embed(
                            preface=":ballot_box_with_check: Correct!",
                        ))
                    else:
                        await self.finish(client, reason="empty deck")
                else:
                    await self.finish(client, reason="wrong")

            elif reaction.emoji == "🔄": # :arrows_counterclockwise:
                await self.refresh_embed()

            if message.guild.me.permissions_in(message.channel).manage_messages:
                # Remove the user's reaction if the bot has permission to make
                # it easier to choose another action without manually removing
                # it before.
                await reaction.message.remove_reaction(reaction.emoji, user)

ALIASES = ("upperlower", "ul")

async def command(ctx: Context) -> None:
    """`upperlower`, `ul`: Play a simple guessing card game.

    **Usage:**
        `ul` by itself: Play the game!
        `ul payouts`: Show the table of payouts for this game.
        `ul stats`: Show your stats for this game.

    This game costs $20 to play. Each turn, you are shown the current card that
    was drawn from the deck and must guess if the next card in the deck will be
    lower or equal, or higher or equal in value to the next card in the deck.
    Aces are considered higher in ranking than Kings.

    The game is controlled by a system of reactions. If you need to refresh the
    game if you, say, played the game in another channel and are returning to
    the older embed, react with :arrows_counterclockwise:.
    """
    sub = ctx.args.pop()

    if not sub:
        game = UpperLowerGame(ctx.player)
        await game.send(ctx.client, ctx.message)
    elif sub == "stats":
        await ctx.channel.send(
            "You have **${0.money}**.\n"
            "Your best game had **{0.ul.best}** guesses.\n"
            "You have played this game **{0.ul.played}** times."
            .format(ctx.player)
        )
    elif sub == "payouts":
        pag = Paginator.from_lines(
            lines=["{} correct: ${}".format(guesses, money)
                   for guesses, money in enumerate(PAYOUTS)],
            title="Upper / Lower Payouts",
            color=0xFFD700, # gold
        )
        await pag.send(ctx.client, ctx.message)
    else:
        raise CommandError("Unknown subcommand, try `help upperlower`.")
