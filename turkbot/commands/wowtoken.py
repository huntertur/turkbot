'''Command that queries the current WoW Token price'''
from json import JSONDecodeError

from turkbot.context import Context
from turkbot.exceptions import CommandError

ALIASES = 'wowtoken'

async def command(ctx: Context) -> None:
    """`wowtoken`: Query the current WoW Token price.

    **Usage:** `wowtoken`
    """
    try:
        async with ctx.channel.typing():
            price = await ctx.blizzapi.get_token_price()
    except (JSONDecodeError, KeyError):
        raise CommandError("The service did not provide a valid response.")
    else:
        await ctx.send("**Current price:** {:,} gold".format(price))
