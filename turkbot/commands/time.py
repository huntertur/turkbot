'''Command to get the current time in a guild.'''
from datetime import datetime
from zoneinfo import ZoneInfo

from turkbot.context import Context
from turkbot.util import restrict

ALIASES = ('time',)

@restrict(dms=False, timezone=True)
async def command(ctx: Context) -> str:
    """`time`: Get the server's current time.

    The server's timezone must be set using `timezone` before this command can
    be used.

    This command reports the guild's current date and time.

    **Example:**
        >>> `time`
        It is Saturday, October 5, 2019, 4:35:43 PM.
    """
    now = datetime.now(ZoneInfo(ctx.timezone))
    # See doctring for example
    await ctx.send("It is {:%A, %B %-d, %Y, %-I:%M:%S %p}.".format(now))
