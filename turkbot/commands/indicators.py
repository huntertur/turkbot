"""Command that converts text into its emoji eqivalent"""
import string

from turkbot.context import Context
from turkbot.exceptions import CommandError, MessageTooLargeError

ALIASES = ("indicators", "i")

# [a-z] is special-cased
OTHER_REPLACEMENTS = {
    '1': ":one: ",
    '2': ":two: ",
    '3': ":three: ",
    '4': ":four: ",
    '5': ":five: ",
    '6': ":six: ",
    '7': ":seven: ",
    '8': ":eight: ",
    '9': ":nine: ",
    '0': ":zero: ",
    ' ': "     ",
    '!': ":grey_exclamation: ",
    '?': ":grey_question: ",
    '$': ":heavy_dollar_sign: ",
    '-': ":heavy_minus_sign: ",
    '+': ":heavy_plus_sign: ",
    '\n': '\n',
}

async def command(ctx: Context) -> None:
    """`indicators`, `i`: Create blocky emoji text.

    **Usage:** `indicators message`

    If you don't give this command a message, it will find the most recently
    sent message and process it instead.

    **Example:**
        >>> `indicators ak47`
        :regional_indicator_a: :regional_indicator_k: :four: :seven:

    Alphanumeric characters, +, -, $, ?, !, and spaces are converted. Other
    characters are ignored.
    """
    phrase = (await ctx.get_content()).lower()
    outlist = []
    for i in phrase:
        if i in string.ascii_lowercase:
            outlist.append(":regional_indicator_{}: ".format(i))
        else:
            outlist.append(OTHER_REPLACEMENTS.get(i, ''))

    output = "".join(outlist)
    if not output:
        raise CommandError("Invalid text; see `help indicators`.")
    if len(output) > 2000:
        raise MessageTooLargeError
    await ctx.send(output)
