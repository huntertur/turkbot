'''Command to view the bot's changelog.'''
import logging
import re

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.settings import get_config

LOGGER = logging.getLogger(__name__)
ALIASES = ('changelog',)

class ChangelogPrivate:
    '''Private data used by the changelog command.

    # Attributes
     - enabled: if the command loaded correctly and is functional
     - log: processed list of changelog entries, one version per list item
    '''
    def __init__(self):
        self.enabled: bool = False
        self.log: list[str] = []

    def try_enable(self):
        if self.enabled:
            LOGGER.warning('re-enabling changelog')
        try:
            with open(get_config('ChangelogFilename')) as changelog:
                # Each version's entry in CHANGELOG.md starts with a Level 1
                # heading, which is a `# ` at the start of a line. Go through
                # the changelog, line-by-line, and separate it by version.
                buffer = ''
                for line in changelog:
                    if line.startswith('# '):
                        self.log.append(buffer.strip()) # trailing newline
                        buffer = ''
                    # Bold and underline the version and release date.
                    line = re.sub(r'^# (.+)', r'**__\1__**', line)
                    # Bold any subheadings.
                    line = re.sub(r'^#{2,6} (.+)', r'**\1**', line)
                    buffer += line
                self.log.append(buffer.strip()) # add last version entry
                del self.log[0] # remove blank first page
        except FileNotFoundError:
            LOGGER.error("cannot load ChangelogFilename; changelog disabled")
        else:
            self.enabled = True

PRIVATE = ChangelogPrivate()

async def command(ctx: Context) -> None:
    """`changelog`: View the bot's changelog.

    Use the arrow emoji reactions to browse through the changelog. Each version
    has its own page, with the latest version shown first.

    Also at: <https://gitlab.com/huntertur/turkbot/blob/master/CHANGELOG.md>

    **Usage:** `changelog`
    """
    if not PRIVATE.enabled:
        PRIVATE.try_enable()
    if not PRIVATE.enabled:
        raise CommandError("This command is disabled because a required file "
                           "could not be loaded.")

    pag = Paginator(pages=PRIVATE.log, title="turkbot changelog")
    await pag.send(ctx.client, ctx.message)
