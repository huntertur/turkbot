"""Command for telling jokes and configuring Guild-specific jokes"""
import json
import logging
import random
import string

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.settings import get_config
from turkbot.util import restrict

LOGGER = logging.getLogger(__name__)

def mix_jokes(client: 'turkbot.bot.TurkBot',
              message: discord.Message) -> list[str]:
    """Mix shared jokes with guild jokes and return the list"""
    cursor = client.database.cursor()
    cursor.execute("""
        SELECT joke
        FROM jokes
        WHERE guild_id = ?
    """, (message.guild.id,))

    guild_jokes = [row["joke"] for row in cursor.fetchall()]

    return client.shared_jokes + guild_jokes

@restrict(dms=False, perms=['manage_messages'])
async def subcommand_list(ctx: Context) -> None:
    # If the user does not have the Manage Messages permission, fail.
    # Letting anyone see the custom joke list ruins the fun
    # Paginate the output
    lines = []
    ctx.cursor.execute("""
        SELECT rowid, joke
        FROM jokes
        WHERE guild_id = ?
    """, (ctx.guild.id,))

    for row in ctx.cursor.fetchall():
        # The rowid is shown as it is used to delete a joke
        lines.append("{0[rowid]}. {0[joke]}".format(row))

    if not lines:
        raise CommandError("There are no custom jokes in this server.")

    pag = Paginator.from_lines(
        lines=lines,
        title="Custom Jokes in {} ({})".format(ctx.guild.name, len(lines))
    )

    await pag.send(ctx.client, ctx.message)

@restrict(dms=False, perms=['manage_messages'])
async def subcommand_add(ctx: Context) -> None:
    ctx.cursor.execute("INSERT INTO jokes VALUES (?, ?)",
                       (ctx.guild.id, str(ctx.args)))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(dms=False, perms=['manage_messages'])
async def subcommand_remove(ctx: Context) -> None:
    # The joke to remove is given by its rowid, which is shown when listing
    # them using `joke list`.
    ctx.cursor.execute("""
        DELETE FROM jokes
        WHERE guild_id = ? AND rowid = ?
    """, (ctx.guild.id, str(ctx.args)))

    if ctx.cursor.rowcount == 0:
        raise CommandError("The custom joke with ID `{}` does not exist in "
                           "this server.".format(str(ctx.args)))

    await ctx.message.add_reaction("✅") # :white_check_mark:

SUBCOMMANDS = {
    "list": subcommand_list,
    "add": subcommand_add,
    "remove": subcommand_remove,
}

ALIASES = ("joke",)

def init(client: 'turkbot.bot.TurkBot') -> None:
    try:
        with open(get_config("SharedJokes")) as shared_jokes_file:
            client.shared_jokes = json.load(shared_jokes_file)
    except (FileNotFoundError, json.JSONDecodeError):
        LOGGER.warning("Could not load %s", get_config("SharedJokes"))
        client.shared_jokes = []

    cursor = client.database.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS jokes (
            guild_id INTEGER NOT NULL,
            joke TEXT NOT NULL
        )
    """)

async def command(ctx: Context) -> None:
    """`joke`: Tell a joke.

    **Usage::**
        `joke` by itself: Tell a random joke
        `joke list`: Show all server-specific jokes
        `joke add [joke]`: Add server-specific joke
        `joke remove [id]`: Remove server-specific joke with its ID

    You must have the Manage Messages permission to add, remove, or look at the
    list of guild-specific jokes.

    When adding a joke, there are three "tokens" that are special:
        `$author` or `${author}`: The name of the person requesting a joke
        `$server` or `${server}`: The name of the server.
        `$channel` or `${channel}`: The name of the channel.

    When removing a joke, use the joke's ID from `joke list`.
    """
    if ctx.args:
        sub = ctx.args.pop().lower()
        if sub not in SUBCOMMANDS:
            raise CommandError("Invalid subcommand, see `help joke` for help.")
        await SUBCOMMANDS.get(sub)(ctx)
    else:
        # If there is no subcommand, just fetch a random joke.
        if isinstance(ctx.channel, discord.DMChannel):
            temp = string.Template(random.choice(ctx.client.shared_jokes))

            joke = temp.safe_substitute(author=ctx.author.display_name)
        else:
            temp = string.Template(random.choice(mix_jokes(ctx.client,
                                                           ctx.message)))

            joke = temp.safe_substitute(
                author=ctx.author.display_name,
                server=ctx.guild.name,
                channel=ctx.channel.name,
            )

        await ctx.send(joke)
