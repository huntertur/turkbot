'''Command for printing the current WoW emissary schedule each day'''
import asyncio
from datetime import datetime, time

from aiohttp import ClientSession
from bs4 import BeautifulSoup

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.internet import fetch, FetchTimeout
from turkbot.settings import get_config
from turkbot.util import restrict

ALIASES = 'emissary'

URL = 'https://www.wowhead.com/world-quests/bfa/na'

def to_sec(when: time) -> int:
    '''Returns the seconds representation of a time object'''
    return (when.hour * 60 * 60) + (when.minute * 60) + when.second

def get_reset_time_utc() -> time:
    return time(get_config('EmissaryHour'), get_config('EmissaryMinute'))

async def create_message() -> str:
    '''Fetch and format the emissary schedule. Returns warning on failure.'''
    try:
        async with ClientSession() as session:
            html = await fetch(session, URL)
    except FetchTimeout:
        return (":warning: Unable to get emissary schedule from <{}>."
                .format(URL))

    soup = BeautifulSoup(html, 'html.parser')

    # schedule is at: div$world-quests-header div dl
    # emissary name in text of: dt a
    # emissary duration in text of: dd
    schedule = soup.find('div', {'id': 'world-quests-header'})
    name_tags = schedule.find_all('dt')
    duration_tags = schedule.find_all('dd')

    message_list = []

    for name_tag, duration_tag in zip(name_tags, duration_tags):
        name = name_tag.a.get_text()
        duration = duration_tag.get_text()
        message_list.append("**{}**: {}".format(name, duration))

    message = "**Emissary list:**\n\n" + "\n".join(message_list)

    return message

async def emissary_loop(client: 'turkbot.bot.TurkBot') -> None:
    '''Send emissary notifications in every subscribed guild when it is time.

    The message is sent a few minutes after daily reset because the in-game
    emissary list is delayed by about 3-5 minutes.

    This time updates for DST changes.
    '''
    while True:
        # Wait for next reset.
        now_date = datetime.utcnow()
        now = time(hour=now_date.hour,
                   minute=now_date.minute,
                   second=now_date.second,
                   microsecond=now_date.microsecond)

        wait = to_sec(get_reset_time_utc()) - to_sec(now)
        if wait <= 0: # now is past the reset time
            wait += 60 * 60 * 24

        await asyncio.sleep(wait)

        # Get a list of all guilds and channels to send to.
        cursor = client.database.cursor()
        cursor.execute('SELECT guild_id, channel_id FROM emissary')
        rows = cursor.fetchall()
        text = await create_message()

        for row in rows:
            channel = client.get_channel(row['channel_id'])
            await channel.send(text)

async def subcommand_disable(ctx: Context) -> None:
    ctx.cursor.execute('''
        SELECT 1
        FROM emissary
        WHERE guild_id = ? AND channel_id = ?
    ''', (ctx.guild.id, ctx.channel.id))

    row = ctx.cursor.fetchone()
    if row is None:
        raise CommandError("Emissary notifications are already disabled.")

    ctx.cursor.execute('''
        DELETE FROM emissary
        WHERE guild_id = ? AND channel_id = ?
    ''', (ctx.guild.id, ctx.channel.id))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_enable(ctx: Context) -> None:
    ctx.cursor.execute('''
        SELECT 1
        FROM emissary
        WHERE guild_id = ? AND channel_id = ?
    ''', (ctx.guild.id, ctx.channel.id))

    row = ctx.cursor.fetchone()
    if row is not None:
        raise CommandError("Emissary notifications are already enabled.")

    ctx.cursor.execute('INSERT INTO emissary VALUES (?, ?)',
                       (ctx.guild.id, ctx.channel.id))

    await ctx.message.add_reaction("✅") # :white_check_mark:

SUBCOMMANDS = {
    "disable": subcommand_disable,
    "enable": subcommand_enable,
}

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS emissary (
            guild_id INTEGER NOT NULL,
            channel_id INTEGER NOT NULL
        )
    ''')

@restrict(dms=False, min_args=1, perms=['manage_channels'])
async def command(ctx: Context) -> None:
    """`emissary`: Post the current WoW emissary schedule each day.

    You must have the Manage Channels permission to use this command.

    **Usage:**
        `emissary enable`: Post the emissary schedule in this channel each day.
        `emissary disable`: Stop posting the emissary schedule.

    The emissary schedule will be posted shortly after the daily reset and is
    powered by Wowhead.
    """
    sub = ctx.args.pop().lower()
    if sub not in SUBCOMMANDS:
        raise CommandError("Unknown subcommand; see `help emissary`.")
    await SUBCOMMANDS.get(sub)(ctx)
