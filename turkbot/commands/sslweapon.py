"""Command for looking up a ShellShock Live weapon"""
import aiohttp
import bs4
import discord

from discord.utils import escape_markdown

from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.internet import fetch
from turkbot.util import restrict

ALIASES = ("sslweapon", "ssl")

REPLACEMENTS = {
    "twentyfive-ball": "TwentyFive-Ball",
    "splitterchain": "SplitterChain",
    "breakerchain": "BreakerChain",
    "rapidfire": "RapidFire",
    "megatunneler": "MegaTunneler",
    "ac-130": "AC-130",
    "megabuilder": "MegaBuilder",
    "megarainbow": "MegaRainbow",
    "bigboomerang": "BigBoomerang",
    "uzi": "UZI",
    "mp5": "MP5",
    "firestorm": "FireStorm",
    "penetrator v2": "Penetrator v2",
    "ak-47": "AK-47",
    "spotter xl": "Spotter XL",
    "spotter xxl": "Spotter XXL",
    "starfire": "StarFire",
    "bfg-1000": "BFG-1000",
    "bfg-9000": "BFG-9000",
    "turretmob": "TurretMob",
    "meganuke": "MegaNuke",
    "breakermadness": "BreakerMadness",
    "breakermania": "BreakerMania",
    "supercat": "SuperCat",
    "ufo": "UFO",
    "shockshell": "ShockShell",
    "shockshell trio": "ShockShell Trio",
    "ashpd": "ASHPD",
    "jack-o-lantern": "Jack-o-Lantern",
    "jack-o-vomit": "Jack-o-Vomit",
}

def create_embed(html: str, search_name: str) -> discord.Embed:
    """Create the embed for the weapon.

    This function handles parsing the wiki page along with generating the weapon
    embed.

    For the embed:
    - The description is the summary on the page.
    - The title is the name of the weapon.
    - Each entry in the stat box has its own field.

    # Parameters
    - html (str): The source of the wiki page
    - search_name (str): The name of the requested weapon
    """
    soup = bs4.BeautifulSoup(html, "html.parser")
    embed = discord.Embed()

    # All of the wiki page's content is in a <div id="mw-content-text">
    main_tag = soup.find("div", {"id": "mw-content-text"})

    # There are several <h2> headings on the page: A "Contents" heading,
    # followed by a heading for each weapon on this page. Ignore the first,
    # "Contents" heading.
    headings = main_tag.find_all("h2")[1:]

    # Find the heading for the weapon that is wanted.
    header = None
    for h2_tag in headings:
        if h2_tag.span.get_text().strip().lower() == search_name.lower():
            header = h2_tag
            break
    else:
        raise CommandError("Either the weapon `{}` does not exist, or there "
                           "was an error.".format(search_name))

    # Get rid of the "[edit source]"
    embed.title = header.find("span", {"class": "mw-headline"}).get_text()

    # The actual content for the heading is stored a <table> that comes after
    # the <h2> tag. However, two next_siblings are needed to get past a newline.
    table = header.next_sibling.next_sibling

    embed.description = table.tr.find("td", {"class": "weapondesc"}).get_text()

    # The names and values for each stat in the stat box are all stored next to
    # each other as <td>s, so each stat actually takes up two values in this
    # list: the name's <td>, and then the value's <td>.
    stat_entries = table.tr.find("td", {"style": "vertical-align:top;"}) \
                           .find_all("td")

    # Make the fields for each stat. See the comment above.
    for field_num in range(0, len(stat_entries), 2):
        embed.add_field(
            name=stat_entries[field_num].get_text(),
            value=escape_markdown(stat_entries[field_num + 1].get_text()),
        )

    embed.set_thumbnail(url=table.find("img")["src"])

    return embed

@restrict(min_args=1)
async def command(ctx: Context) -> None:
    """`sslweapon`, `ssl`: Look up a ShellShock Live weapon.

    **Usage:** `sslweapon [name]`

    The Official ShellShock Live Wiki at <https://shellshocklive.gamepedia.com/>
    is parsed, and the information for the weapon is extracted from its page.
    The intent is that this command can be used to quickly learn what an
    unfamiliar weapon does if you pick one up in the middle of a match, where
    you may not have enough time to consult the wiki or a video yourself.

    Available data may include:
     - a short description
     - type
     - explosion radius
     - if the terrain is damaged
     - if you can damage yourself
     - requirements
    """
    # There are some weapon names, such as the MegaRainbow, that cannot be
    # found with simple capitalization.
    search_name = REPLACEMENTS.get(str(ctx.args).lower(), str(ctx.args).title())

    url = "https://shellshocklive.gamepedia.com/{}".format(search_name)

    # All of the upgrades for a weapon are listed on the page for the base
    # weapon. The wiki will automatically redirect to the correct page.
    # For example, Big Shot redirects to Shot's page.

    async with ctx.channel.typing(), aiohttp.ClientSession() as session:
        html = await fetch(session, url)

    embed = create_embed(html, str(ctx.args))

    await ctx.send(embed=embed)
