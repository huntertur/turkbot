"""Command that flips a two-sided coin"""
import random

from discord import Embed

from turkbot.context import Context

ALIASES = ("coin", "flip")

async def command(ctx: Context) -> None:
    """`coin`, `flip`: Peform a coin flip.

    **Usage:** `coin [question]`, *question* is optional.

    **Example:**
        **Hunter flips a coin...**
        :no_mouth: **Heads**
    """
    embed = Embed()

    if not ctx.args:
        embed_name = "{} flips a coin...".format(ctx.author.display_name)
    else:
        embed_name = '{} flips a coin and asks, "{}"'.format(
            ctx.author.display_name, ctx.client.replace_mentions(str(ctx.args)))

    if random.randint(0, 1) == 0: # heads
        embed.description = ":no_mouth: **Heads**"
        embed.colour = 0xFFCC4D # color of :no_mouth:
    else: # tails
        embed.description = ":mouse2: **Tails**"
        embed.colour = 0xCCD6DD # color of :mouse2:

    embed.set_author(name=embed_name,
                     icon_url=ctx.author.avatar_url)

    await ctx.send(embed=embed)
