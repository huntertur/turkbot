'''Command for slapping another user'''
import asyncio
import random

from turkbot.context import Context

ALIASES = ('slap',)

OBJECTS = [
    ("an anchor", ":anchor:"),
    ("an apple", ":apple:"),
    ("a breadstick", ":french_bread:"),
    ("a cricket bat", ":cricket:"),
    ("a cooking pan", ":cooking:"),
    ("an egg", ":egg:"),
    ("their feet", ":feet:"),
    ("a flower", ":wilted_flower:"),
    ("a glove", ":boxing_glove:"),
    ("their hand", ":hand_splayed:"),
    ("a rainbow", ":rainbow:"),
    ("a trout", ":fish:"),
    ("a violin", ":violin:"),
]

async def command(ctx: Context) -> None:
    """`slap`: Try to slap someone silly.

    Success depends on your finesse at wielding a combat fish.

    **Usage:** `slap [person]`
    """
    # Fumbling and hitting nobody are not documented on purpose. You *try* to
    # hit someone, not for sure hit them. :P
    target_msg = "{user} slaps {victim} with {item[0]}. {item[1]}"
    fumble_msg = ("{user} fumbles while aiming for {victim} and hits themself "
                  "with {item[0]}. {item[1]}")
    single_msg = ("{user} forgot who they were going to slap and hit themself "
                  "with {item[0]}. {item[1]}")

    if ctx.args:
        if random.randint(1, 10) == 1: # 10% chance
            msg = fumble_msg
        else:
            msg = target_msg
    else:
        msg = single_msg


    await ctx.send(msg.format(user=ctx.author.mention,
                              victim=str(ctx.args),
                              item=random.choice(OBJECTS)))
    # fix issue where the deleted message will still appear on the client until
    # restarting it
    await asyncio.sleep(0.1)
    await ctx.message.delete() # because this is an emote-like command
