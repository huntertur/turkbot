'''Command that fetches a random picture of Nicolas Cage'''
import random

from discord import Embed

from turkbot.context import Context
from turkbot.settings import get_config

ALIASES = ('cage',)

def contruct_url() -> str:
    '''Construct the URL at which to fetch the random picture of Nicolas Cage.

    Some example URLs are:
     - https://www.placecage.com/640/480
     - https://www.placecage.com/g/500/600
     - https://www.placecage.com/c/438/547

    '' is calm, 'g/' is grayscale, 'c/' is crazy. One is selected at random.

    The service appears to provide the same image for every request that is
    made with the same dimensions, so they must be randomized -- within reason.
    '''
    return (get_config('CageSource')
            + random.choice(('/', '/g/', '/c/'))
            + str(random.randint(480, 640))
            + '/'
            + str(random.randint(480, 640)))

async def command(ctx: Context) -> None:
    """`cage`: Fetch a random picture of Nicolas Cage.

    **Usage:** `cage`
    """
    output = Embed()
    output.set_image(url=contruct_url())
    output.set_footer(text="Powered by https://www.placecage.com")
    await ctx.send(embed=output)
