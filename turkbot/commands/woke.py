"""Command that generates a meme-of-sorts about Awoken Titan Essence"""
from io import BytesIO
import logging

import discord
import PIL.Image
import PIL.ImageFont
import PIL.ImageDraw

from turkbot.context import Context
from turkbot.exceptions import CommandError, MessageTooLargeError
from turkbot.settings import get_config

LOGGER = logging.getLogger(__name__)
ALIASES = ("woke",)

class WokePrivate:
    '''Private data used by the woke command.

    # Attributes
     - image: template image
     - font: font for text to display on image
    '''
    def __init__(self):
        self.enabled = False
        self.font: PIL.ImageFont
        self.image: PIL.Image

    def try_enable(self):
        if self.enabled:
            LOGGER.warning('re-enabling woke')
        try:
            self.image = PIL.Image.open(get_config('WokeImage'))
        except IOError:
            LOGGER.error("unable to open WokeImage; woke command disabled")
            return

        try:
            self.font = PIL.ImageFont.truetype(get_config('WokeFont'), 17)
        except IOError:
            LOGGER.error("unable to open WokeFont; woke command disabled")
            return

        self.enabled = True

PRIVATE = WokePrivate()

async def command(ctx: Context) -> None:
    """`woke`: Show how woke your Awoken Titan Essence is.

    **Usage:** `woke message`
        `message` is the message to be displayed on the item.

    If you don't give this command a message, it will find the most recently
    sent message and process it instead.
    """
    if not PRIVATE.enabled:
        PRIVATE.try_enable()
    if not PRIVATE.enabled:
        raise CommandError("This command is disabled because a required "
                           "file could not be loaded.")

    image = PRIVATE.image.copy()
    text1 = (await ctx.get_content()).split()
    text2 = []

    while True:
        # Try to wrap the text to two lines. If the text exceeds the two lines,
        # stop and alert the user.
        if PRIVATE.font.getsize(" ".join(text1))[0] > 342:
            text2.insert(0, text1.pop())
        else:
            if PRIVATE.font.getsize(" ".join(text2))[0] > 342:
                raise MessageTooLargeError
            else:
                text1 = " ".join(text1)
                text2 = " ".join(text2)
                break

    PIL.ImageDraw.Draw(image).text((87, 105), text1, font=PRIVATE.font,
                                   fill=(0, 255, 0, 255)) # bright green
    PIL.ImageDraw.Draw(image).text((87, 128), text2, font=PRIVATE.font,
                                   fill=(0, 255, 0, 255))

    with BytesIO() as tmp:
        image.save(tmp, "PNG")
        tmp.seek(0)
        await ctx.send(file=discord.File(tmp, "output.png"))
