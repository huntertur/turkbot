'''Command that tracks the Renown cap in Shadowlands'''
from datetime import datetime

from turkbot.context import Context

ALIASES = 'renown'

# Shadowlands released on 2020-11-23, but weekly reset was less than 24 hours
# later, and the Renown cap did not increase after that reset. To simplify
# things (and because no human is going to be able to execute this command
# anytime before this start date), make the base date the first reset.
BASE_DATE = datetime(2020, 11, 24, 15)

HARD_CAP = 40

def get_cap(when: datetime) -> int:
    '''Get the Renown cap at a given point in time.'''

    if when < BASE_DATE:
        raise ValueError(f'given time is before {BASE_DATE}: {when}')

    delta = when - BASE_DATE
    resets = delta.days // 7
    cap = min(3 * (resets + 1), HARD_CAP)

    return cap

async def command(ctx: Context):
    '''`renown`: Track the current WoW Renown cap.

    **Usage:**
        `renown`
    '''
    cap = get_cap(datetime.utcnow())
    await ctx.send(f'The current Renown cap is **{cap}**.')
