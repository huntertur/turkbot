"""Command that allows users to check their balance"""
from turkbot.context import Context

ALIASES = ("money", "$")

async def command(ctx: Context) -> None:
    """`money`, `$`: Check your balance.

    **Usage:** `money`

    You can also check your balance by using the `stats` subcommand for any
    game command.
    """
    await ctx.send("You have ${}.".format(ctx.player.money))
