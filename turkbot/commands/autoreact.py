"""Command for configuring the bot's auto-react system in a Guild"""
import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.context import Context
from turkbot.exceptions import CommandError
from turkbot.paginator import Paginator
from turkbot.util import restrict

@restrict(min_args=2)
async def subcommand_add(ctx: Context) -> None:
    emoji = ctx.args.rpop()
    phrase = str(ctx.args).lower()

    # Test if the emoji is valid by trying to react with it
    try:
        await ctx.message.add_reaction(emoji)
    except discord.HTTPException:
        raise CommandError("{} is not a valid emoji.".format(emoji))
    else:
        await ctx.message.remove_reaction(emoji, ctx.guild.me)

    # Make sure this phrase isn't already in use
    ctx.cursor.execute("""
        SELECT phrase
        FROM autoreact
        WHERE phrase = ? AND guild_id = ?
        LIMIT 1
    """, (phrase, ctx.guild.id))
    if ctx.cursor.fetchone() is not None:
        raise CommandError("The phrase `{}` is already registered."
                           .format(phrase))

    ctx.cursor.execute("""
        INSERT INTO autoreact VALUES (?, ?, ?)
    """, (ctx.guild.id, phrase, emoji))

    await ctx.message.add_reaction("✅") # :white_check_mark:

@restrict(min_args=1)
async def subcommand_remove(ctx: Context) -> None:
    phrase = str(ctx.args).lower()

    ctx.cursor.execute("""
        DELETE FROM autoreact
        WHERE phrase = ? AND guild_id = ?
    """, (phrase, ctx.guild.id))

    if ctx.cursor.rowcount == 0:
        raise CommandError("The phrase `{}` is not registered.".format(phrase))

    await ctx.message.add_reaction("✅") # :white_check_mark:

async def subcommand_list(ctx: Context) -> None:
    lines = []
    ctx.cursor.execute("""
        SELECT phrase, emoji
        FROM autoreact
        WHERE guild_id = ?
    """, (ctx.guild.id,))

    for row in ctx.cursor.fetchall():
        lines.append("- *{0[phrase]}*: {0[emoji]}".format(row))

    if not lines:
        raise CommandError("There are no auto-reacted phrases in this server.")

    pag = Paginator.from_lines(
        lines=lines,
        title="Auto-reacted Phrases in {} ({})".format(
            ctx.guild.name,
            len(lines)
        ),
    )

    await pag.send(ctx.client, ctx.message)

SUBCOMMANDS = {
    "add": subcommand_add,
    "remove": subcommand_remove,
    "list": subcommand_list,
}

ALIASES = ("autoreact",)

def init(client: 'turkbot.bot.TurkBot') -> None:
    cursor = client.database.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS autoreact (
            guild_id INTEGER NOT NULL,
            phrase TEXT NOT NULL,
            emoji TEXT NOT NULL
        )
    """)

async def match(client: 'turkbot.bot.TurkBot',
                message: discord.Message) -> None:
    """Called whenever a message is sent to apply reactions"""
    cursor = client.database.cursor()
    cursor.execute("""
        SELECT phrase, emoji
        FROM autoreact
        WHERE guild_id = ?
    """, (message.guild.id,))

    for row in cursor.fetchall():
        if row["phrase"] in message.content:
            await message.add_reaction(row["emoji"])

@restrict(dms=False, min_args=1, perms=['manage_emojis'])
async def command(ctx: Context) -> None:
    """`autoreact`: Manage the automatic reaction system.

    You must have the Manage Emojis permission to interact with the system.

    All phrases that are reacted to are case-insensitive. To prevent confusion
    when commands add reactions of their own, automatic reactions are disabled
    on messages that are commands and messages from the bot itself.

    **Usage:**
        `autoreact add phrase emoji`: Register `phrase` so that any messages
            that contain it are automatically reacted to with `emoji`.
        `autoreact remove phrase`: Stop `phrase` from being automatically
            reacted to.
        `autoreact list`: List all phrases that are automatically reacted to
            and what emoji is associated with them.
    """
    sub = ctx.args.pop().lower()

    if sub not in SUBCOMMANDS:
        raise CommandError("Invalid subcommand, see `help autoreact` for "
                           "help.")

    await SUBCOMMANDS.get(sub)(ctx)
    