'''Route handling for the web app interface to turkbot.'''
# This import is for typing
# pylint: disable=unused-import
import turkbot.web

from turkbot.settings import get_config
from turkbot.web.views import (authorize, dataexport, index, help_detail,
                               help_index, login, logout, stats, memory)

def setup_routes(app: 'turkbot.web.TurkbotWeb'):
    '''Register the routes for the web app.'''
    app.router.add_static('/static/', 'turkbot/resources/web/static')
    app.router.add_get('/', index)
    app.router.add_get('/authorize', authorize)
    app.router.add_get('/dataexport', dataexport)
    app.router.add_get('/help', help_index)
    app.router.add_get('/help/{command}', help_detail)
    app.router.add_get('/login', login)
    app.router.add_get('/logout', logout)
    app.router.add_get('/stats', stats)

    if get_config('DebugEnabled'):
        app.router.add_get('/memory', memory)
