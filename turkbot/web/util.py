'''Utility methods for turkbot web.'''
from datetime import datetime, timedelta
import secrets
import sqlite3
from typing import Any, Optional
from urllib.parse import urlencode

from aiohttp import ClientSession

from turkbot.settings import get_config

TOKEN_URI = 'https://discord.com/api/oauth2/token'

class AccessToken:
    '''OAuth2 access token retrieved from Discord.'''
    def __init__(self, token_id: str, access_token: str, token_type: str,
                 expires_at: datetime, refresh_token: str, scope: str):
        self.token_id = token_id
        self.access_token = access_token
        self.token_type = token_type
        self.expires_at = expires_at
        self.refresh_token = refresh_token
        self.scope = scope

    @classmethod
    async def from_discord(cls, database: sqlite3.Connection, *,
                           state: Optional[str] = None,
                           code: Optional[str] = None,
                           code_verifier: Optional[str] = None,
                           token_id: Optional[str] = None):
        '''Parse an access token from Discord and insert it into the database.

        The token_id parameter is present if the token is being refreshed.
        Otherwise, the code parameter is present.
        '''
        if (not code and not token_id) or (code and token_id):
            raise ValueError('one of `code` or `token_id` must be present')

        post_data = {
            'client_id': get_config('DiscordClientID'),
            'client_secret': get_config('DiscordClientSecret'),
            'redirect_uri': get_config('TurkbotWebURL') + 'authorize',
            'scope': 'identify guilds',
            'state': state,
        }
        form_data: bytes
        data: dict[str, Any]

        cursor = database.cursor()

        if token_id:
            post_data['grant_type'] = 'refresh_token'
            post_data['refresh_token'] = \
                cls.from_database(database, token_id).refresh_token
            # DELETE instead of UPDATE because a new one will be INSERTed
            cursor.execute('''
                DELETE FROM web_tokens
                WHERE token_id = ?
            ''', (token_id,))
        else:
            token_id = secrets.token_urlsafe()
            post_data['grant_type'] = 'authorization_code'
            post_data['code'] = code
            post_data['code_verifier'] = code_verifier

        form_data = urlencode(post_data)

        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        async with ClientSession(headers=headers) as session:
            async with session.post(TOKEN_URI, data=form_data) as response:
                data = await response.json()

        expires_at = datetime.utcnow() + timedelta(seconds=data['expires_in'])

        token = cls(token_id, data['access_token'], data['token_type'],
                    expires_at, data['refresh_token'], data['scope'])

        cursor.execute('INSERT INTO web_tokens VALUES (?, ?, ?, ?, ?, ?)',
                       (token.token_id, token.access_token, token.token_type,
                        token.expires_at.isoformat(), token.refresh_token,
                        token.scope))

        return token

    @classmethod
    def from_database(cls, database: sqlite3.Connection, token_id: str):
        '''Get an access token from the database from the internal token ID.

        The token ID is what is stored in the client's cookie.

        Returns None if the tokend doesn't exist or is expired.
        '''
        cursor = database.cursor()
        cursor.execute('''
            SELECT access_token, token_type, expires_at, refresh_token, scope
            FROM web_tokens
            WHERE token_id = ?
            LIMIT 1
        ''', (token_id,))

        row = cursor.fetchone()

        if not row:
            return None

        expires_at = datetime.fromisoformat(row['expires_at'])

        if expires_at < datetime.utcnow():
            cursor.execute('''
                DELETE FROM web_tokens
                WHERE token_id = ?
            ''', (token_id,))
            return None

        return cls(token_id, row['access_token'], row['token_type'], expires_at,
                   row['refresh_token'], row['scope'])

    async def check(self, database: sqlite3.Connection) -> bool:
        '''Check if this token needs refreshing.

        If the token is about to expire and can be refreshed, it is refreshed.
        If it is too late, this returns False.

        The default cutoff for refreshing is 3 days.
        '''
        if self.expires_at < datetime.utcnow():
            return False

        if (datetime.utcnow() - self.expires_at).days <= 3:
            new_token = await AccessToken.from_discord(database,
                                                       token_id=self.token_id)
            self.access_token = new_token.access_token
            self.token_type = new_token.token_type
            self.expires_at = new_token.expires_at
            self.refresh_token = new_token.refresh_token
            self.scope = new_token.scope

        return True
