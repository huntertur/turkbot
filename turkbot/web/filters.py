'''Filters used for view templating'''
import emoji

def emojize(content: str):
    '''Convert :shorthand_emoji: to emoji characters.'''
    return emoji.emojize(content, use_aliases=True)

FILTERS = {
    'emojize': emojize,
}
