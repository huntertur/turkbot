'''Web app allowing some functions of the bot to be used in a browser.'''
import logging
from urllib.parse import quote_plus

import aiohttp_jinja2

from aiohttp import web
from jinja2 import FileSystemLoader

import turkbot.bot

from turkbot.settings import get_config
from turkbot.web.filters import FILTERS
from turkbot.web.routes import setup_routes

LOGGER = logging.getLogger(__name__)

class TurkbotWeb(web.Application):
    '''Web app interface to turkbot.'''
    def __init__(self, bot: 'turkbot.bot.TurkBot', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bot = bot
        setup_routes(self)
        loader = FileSystemLoader('turkbot/resources/web/templates')
        aiohttp_jinja2.setup(self, filters=FILTERS, loader=loader)
        self.runner = web.AppRunner(self)
        self.base: str = get_config('TurkbotWebBase')

        # Used for the PKCE portion of getting the access token. The key is
        # used as the `state`, and the value is the `code_verifier`.
        self.states: dict[str, str] = {}

        self.login_url = ('https://discord.com/api/oauth2/authorize'
                          '?client_id={}&redirect_uri={}'
                          '&response_type=code&scope=identify%20guilds').format(
                              get_config('DiscordClientID'),
                              quote_plus(get_config('TurkbotWebURL')
                                         + 'authorize'),
                          )

        self.init_database()

    async def launch(self):
        '''Properly launch the web app.'''
        if not get_config('TurkbotWebEnabled'):
            LOGGER.warning('turkbot web disabled by TurkbotWebEnabled config')
            return

        await self.runner.setup()
        port = get_config('TurkbotWebPort')
        await web.TCPSite(self.runner, port=port).start()

    async def stop(self):
        '''Properly stop the web app.'''
        if not get_config('TurkbotWebEnabled'):
            return

        await self.runner.shutdown()

    def init_database(self):
        '''Create database tables used by turkbot web.'''
        cursor = self.bot.database.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS web_tokens (
                token_id STRING NOT NULL UNIQUE, -- stored in client's cookie
                access_token STRING NOT NULL,
                token_type STRING NOT NULL,
                expires_at STRING NOT NULL, -- ISO-8601 datetime
                refresh_token STRING NOT NULL,
                scope STRING NOT NULL
            )
        ''')
