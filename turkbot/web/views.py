'''Views for the web app interface to turkbot.'''
from base64 import urlsafe_b64encode
import functools
import hashlib
import secrets
from typing import Any, Coroutine

from aiohttp import ClientSession, web
import aiohttp_jinja2

from turkbot.commands.dataexport import get_data
from turkbot.commands.help import COMMANDS_HELP, get_command_help, populate_help
from turkbot.settings import get_config
from turkbot.web.util import AccessToken

USER_URI = 'https://discord.com/api/v6/users/@me'

def view(template_name: str, needs_auth=False):
    '''Wrapper-of-sorts for views, passing essential variables to templates.'''
    def decorator(view_coro: Coroutine):
        @aiohttp_jinja2.template(template_name)
        @functools.wraps(view_coro)
        async def coro_wrapper(request: web.Request):
            template_vars: dict[str, Any]

            token_id = request.cookies.get('token_id')
            database = request.app.bot.database
            token = AccessToken.from_database(database, token_id)
            auth = token and await token.check(database)

            if (needs_auth and auth) or not needs_auth:
                template_vars = await view_coro(request)
            else:
                template_vars = {}

            # variables for turkbot web
            template_vars['auth'] = auth
            template_vars['base'] = request.app.base
            template_vars['bot'] = request.app.bot
            template_vars['debug'] = get_config('DebugEnabled')
            template_vars['login_url'] = request.app.login_url

            if auth:
                headers = {'Authorization': 'Bearer ' + token.access_token}
                async with ClientSession(headers=headers) as session:
                    async with session.get(USER_URI) as response:
                        template_vars['user'] = await response.json()

            template_vars['len'] = len
            template_vars['repr'] = repr

            return template_vars
        return coro_wrapper
    return decorator

@view('index.html')
async def index(request: web.Request):
    '''Display the index page.'''
    return {}

async def authorize(request: web.Request):
    '''OAuth2 endpoint called by Discord when authorizing a user.'''
    response = web.Response()

    code_verifier = request.app.states[request.query['state']]

    token = await AccessToken.from_discord(request.app.bot.database,
                                           state=request.query['state'],
                                           code=request.query['code'],
                                           code_verifier=code_verifier)
    del request.app.states[request.query['state']]
    response.set_cookie('token_id', token.token_id)
    response.headers.add('Location', get_config('TurkbotWebURL'))
    response.set_status(302) # Found
    return response

@view('dataexport.html', needs_auth=True)
async def dataexport(request: web.Request):
    '''Export the user's data through the web interface.'''
    return {'get_data': get_data}

async def login(request: web.Request):
    '''Take the user to the Discord OAuth2 request page.'''
    response = web.Response()

    # Used for PKCE.
    state = secrets.token_hex()
    code_verifier = secrets.token_hex()
    request.app.states[state] = code_verifier
    hasher = hashlib.sha256()
    hasher.update(code_verifier.encode())
    code_challenge = urlsafe_b64encode(hasher.digest()).decode().rstrip('=')

    login_url = ('{}&code_challenge={}&code_challenge_method={}&state={}'
                 .format(request.app.login_url, code_challenge, 'S256', state))

    response.headers.add('Location', login_url)
    response.set_status(302)
    return response

async def logout(request: web.Request):
    '''Unauthorize the user.'''
    response = web.Response()
    token_id = request.cookies.get('token_id')
    response.del_cookie('token_id')
    if token_id:
        database = request.app.bot.database
        cursor = database.cursor()
        cursor.execute('''
            DELETE FROM web_tokens
            WHERE token_id = ?
        ''', (token_id,))

    response.headers.add('Location', get_config('TurkbotWebURL'))
    response.set_status(302)
    return response

@view('help_index.html')
async def help_index(request: web.Request):
    '''Display the list of commands, with clickable help links.'''
    if COMMANDS_HELP == []:
        populate_help(request.app.bot)

    return {'commands': COMMANDS_HELP}

@view('help_detail.html')
async def help_detail(request: web.Request):
    '''Display the full help text for a command.'''
    if COMMANDS_HELP == []:
        populate_help(request.app.bot)

    return {'command': get_command_help(request.match_info['command'])}

@view('embed.html')
async def stats(request: web.Request):
    '''Display the bot's stats, right from the command.'''
    from turkbot.commands.stats import generate_embed
    return {'embed': generate_embed(request.app.bot)}

@view('memory.html')
async def memory(request: web.Request):
    '''Display a breakdown on bot memeory usage.'''
    import gc
    import statistics
    import sys

    objs = gc.get_objects()

    # {type: [size0, size1, ...]}
    by_type = {}

    for obj in objs:
        obj_type = type(obj)
        obj_size = sys.getsizeof(obj, 0)

        if obj_type not in by_type:
            by_type[obj_type] = []

        by_type[obj_type].append(obj_size)

    return {'data': by_type,
            'statistics': statistics}
