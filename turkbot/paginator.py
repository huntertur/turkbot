"""Helpers for creating Embeds with multiple pages, controlled by reactions"""
from typing import Union

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.settings import get_config

class Paginator:
    """Helper class for creating Discord embeds that have several pages.

    The embed shows one page of text at a time. The bot reacts to the embed with
    several emoji that the author of the command that created the Paginator can
    add reactions to as a way to control which page is currently shown.

    # Attributes
    - pages (List[str]): List of strings that make each page of the Paginator
    - title (str): Title of the embed, before the page count
    - color (Union[int, discord.Color]): Color of the embed
    - message (discord.Message): Message that resulted in Paginator creation
    - page (int): Current displayed page
    - reactor (discord.User): User that can control the Paginator with reactions
      (usually the message author)
    """
    __slots__ = ("pages", "title", "color", "message", "page", "reactor")

    def __init__(self, *, pages: list[str], title: str,
                 color: Union[int, discord.Color] = discord.Embed.Empty):
        self.pages = pages
        self.title = title
        self.color = color
        self.message = None # used for _check() from send()
        self.page = 0 # current page
        self.reactor = None # User that can control the Paginator

    @classmethod
    def from_lines(cls, *, lines: list[str], title: str,
                   color: Union[int, discord.Color] = discord.Embed.Empty):
        """Create a Paginator with pages divided on a per-line basis."""
        max_lines = get_config("PaginatorMaxLines")

        # If the number of lines is small enough to fill only one page, use it
        # right away.
        if len(lines) <= max_lines:
            return cls(pages=['\n'.join(lines)], title=title, color=color)

        # The lines need to be split into pages otherwise. The pages are
        # PaginatorMaxLines in length. The remainder of lines are placed into
        # the smaller final page, if there are any.
        pages = []

        for i in range(len(lines) // max_lines):
            pages.append('\n'.join(lines[i * max_lines:(i + 1) * max_lines]))

        pages.append('\n'.join(lines[len(lines) // max_lines * max_lines:]))

        if pages[-1] == '': # prevent empty last page
            del pages[-1]

        return cls(
            pages=pages,
            title=title,
            color=color,
        )

    @classmethod
    def from_text(cls, *, text: str, title: str, sep: str = ' ',
                  color: Union[int, discord.Color] = discord.Embed.Empty):
        """Create a Paginator with pages split on the word."""
        max_len = get_config("PaginatorMaxLength")

        # Like Paginator.from_lines, but by word and splits the text first.
        # If the text is already of a comfortable length, use it right away.
        if len(text) <= max_len:
            return cls(pages=[text], title=title, color=color)

        words = text.split(sep)
        pages = [""]

        for word in words:
            if len(pages[-1]) + len(word)+1 <= max_len:
                pages[-1] += word + sep
            else:
                pages.append(word + sep)

        return cls(pages=pages, title=title, color=color)

    def create_embed(self, page: int = 0) -> discord.Embed:
        """Create the embed for the specified page number."""
        embed = discord.Embed()
        embed.title = self.title

        # Don't show the page counter if there is only one page.
        if len(self.pages) > 1:
            embed.title += " (Page {}/{})".format(page + 1, len(self.pages))

        embed.description = self.pages[page]
        embed.colour = self.color

        # Don't show who can react to the embed if there is only one page since
        # it can't be interacted with anyway.
        if len(self.pages) > 1:
            embed.set_footer(
                text="{} can interact with this.".format(
                    self.reactor.display_name
                ),
                icon_url=self.reactor.avatar_url,
            )

        return embed

    def add_page(self, content: str):
        """Add a precomposed page to the Paginator.

        # Parameters
        - content (str): The precomposed content of the page to add
        """
        self.pages.append(content)

        return self

    def remove_page(self, index: int):
        """Remove a page from the Paginator by its index.

        The index can be any valid Python index, including negative (backward)
        indicies. The current page is updated to reflect the new page count if
        the page that is removed is located before the current page.

        # Parameters
        - index (int): The index of the page to remove
        """
        del self.pages[index]

        if self.page > index:
            self.page -= 1

        return self

    reactions = [
        # Reactions that act as controls
        "⏪", # :rewind: - Go to page 1
        "◀", # :arrow_backward: - Go back a page
        "▶", # :arrow_forward: - Go forward a page
        "⏩", # :fast_forward: - Go to last page
    ]

    def _check(self, reaction: discord.Reaction, user: discord.Member) -> bool:
        # Make sure that:
        # 1. The person who can control the Paginator is reacting
        # 2. The reaction is applied to the Paginator's message
        # 3. The reaction is a valid control
        if (user.id != self.reactor.id
                or (self.message.id != reaction.message.id)
                or (reaction.emoji not in self.reactions)):
            return False
        return True

    async def send(self, client: 'turkbot.bot.TurkBot',
                   message: discord.Message) -> None:
        """Send the embed as a message and set up the reaction system."""
        self.page = 0
        self.reactor = message.author
        self.message = await message.channel.send(embed=self.create_embed())

        # Don't generate controls if there is only one page.
        if len(self.pages) == 1:
            return

        for emoji in self.reactions:
            await self.message.add_reaction(emoji)

        while True:
            reaction, user = await client.wait_for(event="reaction_add",
                                                   check=self._check)

            # Edit the message, replacing the embed with the embed for the
            # requested page.

            if reaction.emoji == "⏪": # :rewind:
                if self.page != 0:
                    self.page = 0
                    await self.message.edit(
                        embed=self.create_embed(page=self.page))
            elif reaction.emoji == "◀": # :arrow_backward:
                if self.page != 0:
                    self.page -= 1
                    await self.message.edit(
                        embed=self.create_embed(page=self.page))
            elif reaction.emoji == "▶": # :arrow_forward:
                if self.page != len(self.pages) - 1:
                    self.page += 1
                    await self.message.edit(
                        embed=self.create_embed(page=self.page))
            elif reaction.emoji == "⏩": # :fast_forward:
                if self.page != len(self.pages) - 1:
                    self.page = len(self.pages) - 1
                    await self.message.edit(
                        embed=self.create_embed(page=self.page))

            if (not isinstance(message.channel, discord.DMChannel)
                    and (message.guild.me.permissions_in(message.channel)
                         .manage_messages)):
                # Remove the user's reaction if the bot has permission to make
                # it easier to select another page without manually removing it
                # before.
                await reaction.message.remove_reaction(reaction.emoji, user)
