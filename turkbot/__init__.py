"""A Discord bot created by Hunter Turcin"""
from turkbot.bot import TurkBot

__all__ = ["TurkBot"]
__version__ = '1.3.0'
