"""Convenience functions for easier use of aiohttp"""
import aiohttp # for typing
from aiohttp.client_exceptions import ClientError
import async_timeout

from turkbot.exceptions import CommandError
from turkbot.settings import get_config

class FetchTimeout(CommandError):
    '''The fetch timeout expired.'''
    default = 'The service timed out.'

async def fetch(session: aiohttp.ClientSession, *args, **kwargs) -> str:
    """Wrapper for aiohttp's ClientSession.get() that auto-sets the timeout"""
    with async_timeout.timeout(get_config("AiohttpTimeout")) as timer:
        try:
            async with session.get(*args, **kwargs) as response:
                output = await response.text()
                return output
        except ClientError:
            raise CommandError("The service did not provide a valid response.")

    if timer.expired:
        raise FetchTimeout
