'''Exceptions shared by multiple bot commands.

Exception hierarchy:
 - CommandError
    - NoPermissionError
    - MessageTooLargeError
    - BannedError
    - commands.roll.RollError
       - commands.roll.RollDndError
       - commands.roll.RollMinMaxError
       - commands.roll.RollMaxError
    - internet.FetchTimeout
    - util.MultipleMatches
    - util.NoMatches
'''
from typing import Optional

class CommandError(Exception):
    '''An error resulting in termination of a command.'''
    icon = '⚠' # :warning:
    default = 'An error occured.'

    def __init__(self, message: Optional[str] = None):
        self.message = "{} {}".format(self.icon, message or self.default)
        super().__init__(message)

class NoPermissionError(CommandError):
    '''The user does not have permission to use the command.'''
    icon = '🚫' # :no_entry_sign:
    default = 'You do not have permission to use this command.'

class MessageTooLargeError(CommandError):
    '''The resulting message is too large.'''
    icon = '🐷' # :pig:
    default = 'The output message was too large.'

class BannedError(CommandError):
    '''The user is banned from using the command.'''
    icon = '🛑' # :stop_sign:
    default = 'You are banned from using this command.'
