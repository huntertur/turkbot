"""Mega-object passed to commands"""
from typing import Optional
from zoneinfo import ZoneInfo, ZoneInfoNotFoundError

import discord

import turkbot.bot # pylint: disable=unused-import
from turkbot.exceptions import CommandError
from turkbot.util import get_last_text_message, NoMatches

class Whitespace:
    """Marks a whitespace token in an Args object."""
    __slots__ = ("what",)

    def __init__(self, whitespace: str):
        self.what = whitespace

    def __str__(self):
        return self.what

    def __repr__(self):
        return "Whitespace({!r})".format(self.what)

    def __eq__(self, other):
        return isinstance(other, Whitespace) and self.what == other.what

class Word:
    """Marks a non-whitespace token in an Args object."""
    __slots__ = ("what",)
    def __init__(self, word: str):
        self.what = word

    def __str__(self):
        return self.what

    def __repr__(self):
        return "Word({!r})".format(self.what)

    def __eq__(self, other):
        return isinstance(other, Word) and self.what == other.what

class Args:
    """Stores arguments to a command."""
    __slots__ = ("tokens",)

    def __init__(self, command_line: str):
        self.tokens = []

        command_line = command_line.strip()

        buffer = []
        current_token = None

        # Break command_line into a series of Word and Whitespace tokens
        for char in command_line:
            if char in " \t\n": # is whitespace
                if current_token == Word:
                    self.tokens.append(Word(''.join(buffer)))
                    buffer = []
                current_token = Whitespace
                buffer.append(char)
            else:
                if current_token == Whitespace:
                    self.tokens.append(Whitespace(''.join(buffer)))
                    buffer = []
                current_token = Word
                buffer.append(char)

        # Add final token
        if current_token == Word:
            self.tokens.append(Word(''.join(buffer)))
        elif current_token == Whitespace:
            self.tokens.append(Whitespace(''.join(buffer)))

    def __str__(self):
        return ''.join(str(token) for token in self.tokens)

    def __repr__(self):
        return "Args(" + ', '.join(repr(token) for token in self.tokens) + ')'

    def __len__(self):
        count = 0

        # Count number of Words (arguments)
        for token in self.tokens:
            if isinstance(token, Word):
                count += 1

        return count

    def __iter__(self):
        return self

    def __next__(self):
        item = self.pop()
        if item is not None:
            return item
        raise StopIteration

    def pop(self):
        """Pop the first argument and return its content."""
        if not self.tokens:
            return None

        arg = self.tokens[0].what
        del self.tokens[0] # delete the Word

        # If len(self) == 1, there would be no Whitespace token to delete
        if self.tokens:
            del self.tokens[0] # delete Whitespace after the Word

        return arg

    def rpop(self):
        """Pop the last argument and return its content."""
        if not self.tokens:
            return None

        arg = self.tokens[-1].what
        del self.tokens[-1] # delete the Word

        # If len(self) == 1, there would be no Whitespace token to delete
        if self.tokens:
            del self.tokens[-1] # delete Whitespace before the Word

        return arg

class Context:
    """Mega-object passed to commands.

    A single Context object is passed to commands instead of several arguments
    so that every command's definition doesn't need to be updated whenever a
    new thing is added to the Context object.
    """
    __slots__ = ("client", "message", "args", "author", "blizzapi",
                 "channel", "cursor", "guild", "player")

    def __init__(self, client: 'turkbot.bot.TurkBot', message: discord.Message,
                 command_line: str):
        # direct access to objects
        self.client = client
        self.message = message
        self.args = Args(command_line)
        # actually useful shortcuts
        self.author = message.author
        self.blizzapi = client.blizzapi
        self.channel = message.channel
        self.cursor = client.database.cursor()
        self.guild = message.guild
        self.player = client.players.get(message.author.id)

    async def send(self, *args, **kwargs):
        """Wrapper for self.message.channel.send"""
        return await self.message.channel.send(*args, **kwargs)

    def sub(self):
        """Modify arguments for passing to a subcommand."""
        self.args.pop()

        return self

    async def get_content(self):
        '''Returns a phrase for commands that transform text.

        Return the argument string. If there are no arguments, return the
        text of the most recently sent message in the channel. If there is
        not message, inform the user.
        '''
        if self.args:
            return str(self.args)

        try:
            return (await get_last_text_message(self.channel)).content
        except NoMatches:
            raise CommandError("Could not find a text message.")

    @property
    def timezone(self) -> Optional[str]:
        '''The guild's timezone, if one is set.'''
        row = self.cursor.execute('''
            SELECT timezone
            FROM guild_timezones
            WHERE guild_id = ?
        ''', (self.guild.id,)).fetchone()

        return row['timezone'] if row else None

    @timezone.setter
    def timezone(self, tzname: str):
        # Test if the timezone is valid
        try:
            ZoneInfo(tzname)
        except ZoneInfoNotFoundError:
            raise CommandError(f"The timezone `{tzname}` doesn't exist.")

        self.cursor.execute('''
            INSERT OR REPLACE INTO guild_timezones
            VALUES (?, ?)
        ''', (self.guild.id, tzname))

    @property
    def default_realm(self) -> Optional[str]:
        '''The guild's default WoW realm, if one is set.'''
        row = self.cursor.execute('''
            SELECT realm
            FROM default_realms
            WHERE guild_id = ?
        ''', (self.guild.id,)).fetchone()

        return row['realm'] if row else None

    @default_realm.setter
    def default_realm(self, realm: str):
        self.cursor.execute('''
            INSERT OR REPLACE INTO default_realms
            VALUES (?, ?)
        ''', (self.guild.id, realm))
