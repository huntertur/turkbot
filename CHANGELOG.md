# 1.3.0 - 2021-05-31
## Added
 - A web interface for some commands. See https://huntertur.net/turkbot-web/.
 - `wowrealm`, which allows the default `wow` realm to be set per-server.
 - `someone`, which emulates the behavior of Discord's old @someone joke.
 - `bat`, which fetches random bat images.
 - `renown`, which shows the current World of Warcraft: Shadowlands renown cap.
 - Support for running as a container, which makes bot deployments easier.

## Changed
 - Commands that don't work in DMs now display an error message.
 - `role list` now cleans up deleted roles from the self-assignable role list.
 - `cat` now gets images from several sources for redundancy.
 - `tag` now interprets tag names as case-insensitive.

## Fixed
 - `wow` now uses the 2020 API schema, letting it work again.
 - `wow` now uses the correct tier names for Cataclysm and Mists of Pandaria.
 - `random` no longer freezes the rest of the bot during dice rolls.
 - `leavelog` now escapes Markdown symbols in usernames.
 - `prefix` subcommands are now always accessible with the default prefix.

# 1.2.0 - 2019-10-11
## Added
 - A guide for implementing a system where members can pick the color of their
   name has been added to the `doc` folder.
 - You can now directly make tags from images with `tag add` and `tag update`
   by uploading the image with the command message. You no longer need to upload
   the image elsewhere and copy a URL.
 - New command: `cage`, which sends a random picture of Nicolas Cage.
 - New command: `leavelog`, which logs whenever someone leaves the server,
   including when they joined, how long they stayed, their nickname in the
   server, and their icon.

## Changed
 - In the `changelog` command, to make up for Discord's lack of headings,
   the version, date, and subheadings are bolded and/or underlined.

## Fixed
 - Questions given to `8ball` and `coin` that contain mentions will now be
   replaced with the user's or channel's name.

# 1.1.0 - 2019-10-05
## Added
 - New command: `time`, which gets the server's current time. This can be useful
   in accounting for timezone differences between you and the server.
 - New command: `changelog`, which allows you to view the bot's changelog from
   within Discord. The latest version's changes are shown first, but you can
   browse through time as well.

## Changed
 - `remind timezone` has been broken into its own command, `timezone`/`tz`. This
   is so there is a uniform method of getting and setting the server's timezone,
   now that `remind` is no longer the only command dealing with time.
 - You no longer need the Manage Messages permission to view the server's
   timezone. Setting it still requires the Manage Server permission.
