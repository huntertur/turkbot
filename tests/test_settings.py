import pytest

from turkbot.settings import PRIVATE, CONFIG_DEFAUTLS, get_config

FAKE_CONFIG_TEXT = '''\
[General]
DiscordAPIToken = asdf
TurkbotWebPort = 9000
DebugEnabled = True
EmissaryMinute = h
TestingBool = h
'''

@pytest.fixture(autouse=True)
def reset_config(monkeypatch, tmp_path):
    '''Make the config parser only see the fake config file.'''
    (tmp_path / 'config.ini').write_text(FAKE_CONFIG_TEXT)
    monkeypatch.chdir(tmp_path)
    PRIVATE.config.remove_section('General')
    PRIVATE.config.read('config.ini')

def test_file_str():
    assert get_config('DiscordAPIToken') == 'asdf'

def test_file_int():
    assert get_config('TurkbotWebPort') == 9000

def test_file_bool():
    assert get_config('DebugEnabled')

def test_default_str():
    assert get_config('PlayingMessage') == CONFIG_DEFAUTLS['PlayingMessage']

def test_default_int():
    assert get_config('EmissaryHour') == CONFIG_DEFAUTLS['EmissaryHour']

def test_default_bool():
    assert (get_config('TurkbotWebEnabled')
            == CONFIG_DEFAUTLS['TurkbotWebEnabled'])

def test_bad_int():
    assert get_config('EmissaryMinute') == CONFIG_DEFAUTLS['EmissaryMinute']

def test_bad_bool():
    CONFIG_DEFAUTLS['TestingBadBool'] = True
    assert get_config('TestingBadBool') == CONFIG_DEFAUTLS['TestingBadBool']

def test_cannot_get_nonexistent_setting():
    with pytest.raises(KeyError):
        get_config('h')
