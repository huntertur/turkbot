import sqlite3
from typing import Iterable

import pytest

from turkbot.commands.prefix import init
from turkbot.settings import PRIVATE
from turkbot.util import (get_last_text_message, get_prefix, is_image_url,
                          NoMatches)

SQUARE_CONFIG = '[General]\nCommandPrefix = ]'
ANGLE_CONFIG = '[General]\nCommandPrefix = >'

class MockMessage:
    def __init__(self, content: str):
        self.content = content

class MockHistory:
    def __init__(self, messages: Iterable[MockMessage]):
        self.iterator = iter(messages)

    def __aiter__(self):
        return self

    async def __anext__(self):
        try:
            return next(self.iterator)
        except StopIteration:
            raise StopAsyncIteration

class MockChannel:
    def __init__(self, messages: Iterable[MockMessage]):
        self.messages = messages

    def history(self):
        return MockHistory(self.messages)

class MockGuild:
    def __init__(self, guild_id: int):
        self.id = guild_id

class MockBot:
    def __init__(self, database: sqlite3.Connection):
        self.database = database

def change_config(monkeypatch, tmp_path, content: str):
    (tmp_path / 'config.ini').write_text(content)
    monkeypatch.chdir(tmp_path)
    PRIVATE.config.remove_section('General')
    PRIVATE.config.read('config.ini')

@pytest.fixture
def cursor() -> sqlite3.Cursor:
    '''Get a cursor to a database with test data.'''
    database = sqlite3.connect(':memory:')
    database.row_factory = sqlite3.Row
    init(MockBot(database))

    cursor = database.cursor()
    cursor.execute('''
        INSERT INTO prefixes
        VALUES (2, NULL), (3, '?')
    ''')

    return cursor

MOCK_HISTORY_SOME = (
    MockMessage('Msg 1'),
    MockMessage(''),
    MockMessage('Msg 3'),
    MockMessage('Msg 4'),
    MockMessage(''),
)

MOCK_HISTORY_ONE = (
    MockMessage('Msg 1'),
    MockMessage(''),
)

MOCK_HISTORY_ZERO = ()

def test_is_image_url():
    assert is_image_url("http://example.com/dog.jpg") # normal
    assert is_image_url("https://example.com/files/my_cat.JPEG") # capitals
    assert is_image_url("https://example.com/banner.png?color=red") # query
    assert is_image_url("http://example.com/Happy%20Birthday.GIF") # percent
    assert is_image_url("http://example.com/h.BMP?foo=2&bar=w") # multi-query
    assert is_image_url("https://cdn.example.com/users/5769.webp") # subdomain

    assert not is_image_url("ftp://example.com/public_html/Welcome.PNG") # ftp
    assert not is_image_url("People") # not a url
    assert not is_image_url("https://example.com/index.html?lang=en_US") # html

@pytest.mark.asyncio
async def test_get_last_text_message_some():
    channel = MockChannel(MOCK_HISTORY_SOME)
    message = await get_last_text_message(channel, skip_first=False)
    assert message.content == 'Msg 1'

@pytest.mark.asyncio
async def test_get_last_text_message_some_skip():
    channel = MockChannel(MOCK_HISTORY_SOME)
    message = await get_last_text_message(channel, skip_first=True)
    assert message.content == 'Msg 3'

@pytest.mark.asyncio
async def test_get_last_text_message_one():
    channel = MockChannel(MOCK_HISTORY_ONE)
    message = await get_last_text_message(channel, skip_first=False)
    assert message.content == 'Msg 1'

@pytest.mark.asyncio
async def test_get_last_text_message_one_skip():
    channel = MockChannel(MOCK_HISTORY_ONE)
    with pytest.raises(NoMatches):
        await get_last_text_message(channel, skip_first=True)

@pytest.mark.asyncio
async def test_get_last_text_message_zero():
    channel = MockChannel(MOCK_HISTORY_ZERO)
    with pytest.raises(NoMatches):
        await get_last_text_message(channel, skip_first=False)

@pytest.mark.asyncio
async def test_get_last_text_message_zero_skip():
    channel = MockChannel(MOCK_HISTORY_ZERO)
    with pytest.raises(NoMatches):
        await get_last_text_message(channel, skip_first=True)

def test_get_prefix_unknown(monkeypatch, tmp_path, cursor):
    change_config(monkeypatch, tmp_path, SQUARE_CONFIG)
    assert get_prefix(cursor, MockGuild(1)) == ']'

def test_get_prefix_default(monkeypatch, tmp_path, cursor):
    change_config(monkeypatch, tmp_path, SQUARE_CONFIG)
    assert get_prefix(cursor, MockGuild(2)) == ']'

def test_get_prefix_set(monkeypatch, tmp_path, cursor):
    change_config(monkeypatch, tmp_path, SQUARE_CONFIG)
    assert get_prefix(cursor, MockGuild(3)) == '?'

def test_get_prefix_unknown_does_change(monkeypatch, tmp_path, cursor):
    test_get_prefix_unknown(monkeypatch, tmp_path, cursor)
    change_config(monkeypatch, tmp_path, ANGLE_CONFIG)
    assert get_prefix(cursor, MockGuild(1)) == '>'

def test_get_prefix_default_does_change(monkeypatch, tmp_path, cursor):
    test_get_prefix_default(monkeypatch, tmp_path, cursor)
    change_config(monkeypatch, tmp_path, ANGLE_CONFIG)
    assert get_prefix(cursor, MockGuild(2)) == '>'

def test_get_prefix_set_does_not_change(monkeypatch, tmp_path, cursor):
    test_get_prefix_set(monkeypatch, tmp_path, cursor)
    change_config(monkeypatch, tmp_path, ANGLE_CONFIG)
    assert get_prefix(cursor, MockGuild(3)) == '?'
