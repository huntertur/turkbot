from pytest import raises

from turkbot.cli import cli_main

def test_help(capsys):
    with raises(SystemExit):
        cli_main(['-h'])
    assert capsys.readouterr().out.startswith('usage: turkbot')

    with raises(SystemExit):
        cli_main(['--help'])
    assert capsys.readouterr().out.startswith('usage: turkbot')

def test_version(capsys):
    with raises(SystemExit):
        cli_main(['-V'])
    assert capsys.readouterr().out.startswith('turkbot ')

    with raises(SystemExit):
        cli_main(['--version'])
    assert capsys.readouterr().out.startswith('turkbot ')

def test_invalid():
    with raises(SystemExit) as exc:
        cli_main(['--invalid'])
    assert exc.value != 0 # get exit status code

def test_config_path():
    args = cli_main(['-C', 'config_example.ini'])
    assert args.config_path == 'config_example.ini'

def test_config_path_default():
    args = cli_main([])
    assert args.config_path == 'config.ini'
