from turkbot.context import Args, Whitespace, Word

def test_constructor():
    line = " The quick\nbrown\tfox \tjumps\n\n \tover the \n lazy dog. "
    args = Args(line)

    assert len(args) == 9
    assert len(args.tokens) == 17
    assert args.tokens[0] == Word("The")
    assert args.tokens[1] == Whitespace(' ')
    assert args.tokens[2] == Word("quick")
    assert args.tokens[3] == Whitespace('\n')
    assert args.tokens[4] == Word("brown")
    assert args.tokens[5] == Whitespace('\t')
    assert args.tokens[6] == Word("fox")
    assert args.tokens[7] == Whitespace(" \t")
    assert args.tokens[8] == Word("jumps")
    assert args.tokens[9] == Whitespace("\n\n \t")
    assert args.tokens[10] == Word("over")
    assert args.tokens[11] == Whitespace(' ')
    assert args.tokens[12] == Word("the")
    assert args.tokens[13] == Whitespace(" \n ")
    assert args.tokens[14] == Word("lazy")
    assert args.tokens[15] == Whitespace(' ')
    assert args.tokens[16] == Word("dog.")

def test_manipulation():
    line = "Please\n\n\tExcuse  My\tDear \t\n Aunt Sally"
    args = Args(line)

    assert len(args) == 6
    assert str(args) == "Please\n\n\tExcuse  My\tDear \t\n Aunt Sally"

    assert args.pop() == "Please"
    assert str(args) == "Excuse  My\tDear \t\n Aunt Sally"
    assert len(args) == 5

    assert args.rpop() == "Sally"
    assert str(args) == "Excuse  My\tDear \t\n Aunt"
    assert len(args) == 4

    assert repr(args) == (r"Args(Word('Excuse'), "
                          r"Whitespace('  '), "
                          r"Word('My'), "
                          r"Whitespace('\t'), "
                          r"Word('Dear'), "
                          r"Whitespace(' \t\n '), "
                          r"Word('Aunt'))")
