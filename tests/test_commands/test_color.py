import pytest

from turkbot.commands.color import RGBColor

def test_rgb():
    with pytest.raises(ValueError):
        RGBColor(-2, 0, 0)
    with pytest.raises(ValueError):
        RGBColor(0, 999, 0)
    with pytest.raises(ValueError):
        RGBColor(0, 0, 256)

    aliceblue = RGBColor(240, 248, 255)

    assert int(aliceblue) == 0xF0F8FF
    assert str(aliceblue) == '(240, 248, 255)'
    assert repr(aliceblue) == 'RGBColor(red=240, green=248, blue=255)'
    assert aliceblue.get_name() == 'aliceblue'

def test_from_str():
    zero = RGBColor.from_str('0x123456')
    pound = RGBColor.from_str('#789aBC')
    three = RGBColor.from_str('#DeF')
    raw = RGBColor.from_str('231405')
    name = RGBColor.from_str('green')
    with pytest.raises(ValueError):
        RGBColor.from_str('jkdsfjsadlkf')

    assert int(zero) == 0x123456
    assert int(pound) == 0x789ABC
    assert int(three) == 0xDDEEFF
    assert int(raw) == 0x231405
    assert int(name) == 0x008000

def test_get_name():
    assert RGBColor(0x00, 0x80, 0x00).get_name() == 'green'
    assert RGBColor.from_str('E6E6FA').get_name() == 'lavender'
    assert RGBColor.from_str('lawngreen').get_name() == 'lawngreen'
    assert RGBColor.from_str('575757').get_name() is None
