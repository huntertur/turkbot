from turkbot.commands.changelog import ChangelogPrivate

TEST_LOG = '''\
# 7.3.5 - 2018-01-26
## Added
 - Sword, but not shield.
 - other things
### Subheading

####### Invalid Subheading
 ## Floating

## Changed
 - Clothes

# 5.7
Text. Text text.
## Removed
 - that means moved again, not deleted
'''

EXPECTED = ['''\
**__7.3.5 - 2018-01-26__**
**Added**
 - Sword, but not shield.
 - other things
**Subheading**

####### Invalid Subheading
 ## Floating

**Changed**
 - Clothes
'''.strip(), '''\
**__5.7__**
Text. Text text.
**Removed**
 - that means moved again, not deleted
'''.strip()]

def test_formatting(monkeypatch, tmp_path):
    '''Check if changelogs are being formatted properly.

    This includes:
     - each version having its own page
     - headings being bolded and/or underlined
    '''
    (tmp_path / 'CHANGELOG.md').write_text(TEST_LOG)
    monkeypatch.chdir(tmp_path)
    private = ChangelogPrivate()
    private.try_enable()
    assert private.log == EXPECTED
