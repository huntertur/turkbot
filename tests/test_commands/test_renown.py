from datetime import datetime

import pytest

from turkbot.commands.renown import get_cap, HARD_CAP

def test_get_cap_throws_in_the_past():
    with pytest.raises(ValueError):
        get_cap(datetime(2020, 10, 26))

def test_get_cap_before_reset():
    cap = get_cap(datetime(2020, 12, 1, 14))
    assert cap == 3

def test_get_cap_after_reset():
    cap = get_cap(datetime(2020, 12, 1, 15))
    assert cap == 6

def test_get_cap_after_max():
    cap = get_cap(datetime(2022, 12, 31))
    assert cap == HARD_CAP
