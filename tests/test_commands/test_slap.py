from turkbot.commands.slap import OBJECTS

def test_item_list():
    '''Check if the list of slapping items is formatted correctly.'''
    assert OBJECTS
    for item in OBJECTS:
        assert len(item) == 2
        assert isinstance(item[0], str) # name
        assert isinstance(item[1], str) # emoji
        assert item[1].startswith(':')
        assert item[1].endswith(':')
