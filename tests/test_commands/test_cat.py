from urllib.parse import ParseResult, urlparse

from aioresponses import aioresponses
import pytest

from turkbot.commands.cat import (CataasProvider, RandomCatProvider,
                                  TheCatApiProvider)
from turkbot.settings import get_config
from turkbot.util import is_image_url

MOCK_RANDOM_CAT = '{"file":"https://example.com/cat.jpg"}'
MOCK_THE_CAT_API = ('[{"breeds":[],"id":"x","url":"http://x.com/x.jpg",'
                    '"width":640,"height":480}]')

@pytest.mark.asyncio
async def test_cataas_fetch():
    '''Check if the cataas.com provider can make a correct URL.'''
    url = await CataasProvider().get_next_url()
    parsed = urlparse(url)

    assert parsed.scheme.startswith('http')
    assert parsed.netloc
    assert parsed.path
    assert parsed.query

@pytest.mark.asyncio
async def test_random_cat_fetch():
    '''Check if the random.cat provider can handle the response.'''
    with aioresponses() as mock:
        mock.get(get_config('RandomCatSource'), body=MOCK_RANDOM_CAT)
        provider = RandomCatProvider()
        assert is_image_url(await provider.get_next_url())

@pytest.mark.asyncio
async def test_the_cat_api_fetch():
    '''Check if the thecatapi.com provider can handle the response.'''
    with aioresponses() as mock:
        mock.get(get_config('TheCatApiSource'), body=MOCK_THE_CAT_API)
        provider = TheCatApiProvider()
        assert is_image_url(await provider.get_next_url())
