import pytest

from turkbot.commands.roll import determine_roll_function, RollError

async def roll(input_text: str) -> int:
    '''Wrapper for determine_roll_function.'''
    return await determine_roll_function(input_text)(input_text)

@pytest.mark.asyncio
async def test_dnd_blank_dice():
    result = await roll('d6')
    assert 1 <= result <= 6

@pytest.mark.asyncio
async def test_dnd_one_die():
    result = await roll('1d6')
    assert 1 <= result <= 6

@pytest.mark.asyncio
async def test_dnd_some_dice():
    result = await roll('6d6')
    assert 6 <= result <= 36

@pytest.mark.asyncio
async def test_dnd_blank_dice_one_side():
    result = await roll('d1')
    assert result == 1

@pytest.mark.asyncio
async def test_dnd_one_die_one_side():
    result = await roll('1d1')
    assert result == 1

@pytest.mark.asyncio
async def test_dnd_some_dice_one_side():
    result = await roll('6d1')
    assert result == 6

@pytest.mark.asyncio
async def test_dnd_plus():
    result = await roll('d1+3')
    assert result == 4

@pytest.mark.asyncio
async def test_dnd_minus():
    result = await roll('d1-3')
    assert result == -2

@pytest.mark.asyncio
async def test_dnd_cannot_have_plus_minus():
    with pytest.raises(RollError):
        await roll('d1+-3')

@pytest.mark.asyncio
async def test_dnd_cannot_have_blank_alter():
    with pytest.raises(RollError):
        await roll('d1+')

@pytest.mark.asyncio
async def test_dnd_cannot_have_invalid_alter():
    with pytest.raises(RollError):
        await roll('d1+h')

@pytest.mark.asyncio
async def test_dnd_cannot_have_blank_sides():
    with pytest.raises(RollError):
        await roll('d+3')

@pytest.mark.asyncio
async def test_dnd_cannot_have_invalid_sides():
    with pytest.raises(RollError):
        await roll('dh+3')

@pytest.mark.asyncio
async def test_dnd_cannot_have_invalid_dice():
    with pytest.raises(RollError):
        await roll('hd1+3')

@pytest.mark.asyncio
async def test_dnd_cannot_have_zero_dice():
    with pytest.raises(RollError):
        await roll('0d1+3')

@pytest.mark.asyncio
async def test_dnd_cannot_have_zero_sides():
    with pytest.raises(RollError):
        await roll('d0+3')

@pytest.mark.asyncio
async def test_minmax_loose_bounds():
    result = await roll('1-10')
    assert 1 <= result <= 10

@pytest.mark.asyncio
async def test_minmax_tight_bounds():
    result = await roll('5-5')
    assert result == 5

@pytest.mark.asyncio
async def test_minmax_cannot_have_flipped_bounds():
    with pytest.raises(RollError):
        await roll('10-1')

@pytest.mark.asyncio
async def test_minmax_cannot_have_blank_min():
    with pytest.raises(RollError):
        await roll('-10')

@pytest.mark.asyncio
async def test_minmax_cannot_have_blank_max():
    with pytest.raises(RollError):
        await roll('1-')

@pytest.mark.asyncio
async def test_minmax_cannot_have_invalid_min():
    with pytest.raises(RollError):
        await roll('h-10')

@pytest.mark.asyncio
async def test_minmax_cannot_have_invalid_max():
    with pytest.raises(RollError):
        await roll('1-h')

@pytest.mark.asyncio
async def test_max_some_sides():
    result = await roll('10')
    assert 1 <= result <= 10

@pytest.mark.asyncio
async def test_max_one_side():
    result = await roll('1')
    assert result == 1

@pytest.mark.asyncio
async def test_max_cannot_have_zero_sides():
    with pytest.raises(RollError):
        await roll('0')

@pytest.mark.asyncio
async def test_max_cannot_have_invalid_sides():
    with pytest.raises(RollError):
        await roll('h')
