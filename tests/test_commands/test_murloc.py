import random

from turkbot.commands.murloc import nerglish

def test_translate():
    # known phrase
    assert nerglish('I will kill you!') == 'Mrgllll glrrm gl!'

    # all unknown
    random.seed(0)
    assert (nerglish('Marglerargle wants you to know he is thinking of you!')
            == 'Marglerargle uanls uou lo knka he hs mhlnklng of uou!')

    # mix of known and unknown
    random.seed(0)
    assert (nerglish('I am sorry, but you have ringworm.')
            == 'U am mrrrgll, luu lou haae murguhlum.')

def test_capitalize():
    eng = """\
sentence one. SenTence Two? SENTENCE three! Sentence four
New line, new me. New
Me
new.
line... Henlo"""

    nerg = """\
Senuenle one. Senuenle aho? Senmenle lhree! Senuenue four
Neh llne, neh me. Nem
Me
Neg.
Lmne... Henlo"""

    random.seed(0)
    assert nerglish(eng) == nerg
