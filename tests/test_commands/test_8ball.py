from turkbot.commands.eightball import SAYINGS

def test_sayings_length():
    # A Magic 8-Ball only has 20 sayings.
    assert len(SAYINGS) == 20
