import sqlite3

import pytest

from turkbot.commands.someone import (disable, enable, init, is_enabled,
                                      TEMPLATES)

class MockBot:
    def __init__(self, database: sqlite3.Connection):
        self.database = database

class MockUser:
    def __init__(self, user_id: int):
        self.id = user_id

@pytest.fixture
def cursor() -> sqlite3.Cursor:
    '''Get a cursor to a database with test data.'''
    database = sqlite3.connect(':memory:')
    #database.isolation_level = None # enable autocommit
    init(MockBot(database))

    cursor = database.cursor()
    cursor.execute('''
        INSERT INTO at_someone_disable
        VALUES (?)
    ''', (12345,))

    return cursor

def test_templates_have_name():
    assert all('{name}' in template for template in TEMPLATES)

def test_is_enabled_true(cursor):
    assert is_enabled(cursor, MockUser(23456))

def test_is_enabled_false(cursor):
    assert not is_enabled(cursor, MockUser(12345))

def test_enable(cursor):
    user = MockUser(12345)
    enable(cursor, user)
    assert is_enabled(cursor, user)

def test_disable(cursor):
    user = MockUser(34567)
    disable(cursor, user)
    assert not is_enabled(cursor, user)
