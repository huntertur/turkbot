import sqlite3
from typing import Iterable

import pytest

from turkbot.commands.role import (get_registered_roles, get_role_by_name,
                                   init, register_role, unregister_role)

class MockBot:
    def __init__(self, database: sqlite3.Connection):
        self.database = database

class MockRole:
    def __init__(self, role_id: int, name: str):
        self.id = role_id
        self.name = name

    def __eq__(self, other):
        return self.id == other.id and self.name == other.name

class MockGuild:
    def __init__(self, guild_id: int, roles: Iterable[MockRole]):
        self.id = guild_id
        self.roles = roles

    def get_role(self, role_id: int) -> MockRole:
        for role in self.roles:
            if role.id == role_id:
                return role

        return None

GUILD_0_ROLES = [
    MockRole(100, 'Red'),
    MockRole(101, 'Green'),
    MockRole(102, 'Blue'),
]

GUILD_1_ROLES = [
    MockRole(103, 'Red'),
    MockRole(104, 'Stone'),
    MockRole(105, 'Quartz'),
]

GUILD_0 = MockGuild(0, GUILD_0_ROLES)
GUILD_1 = MockGuild(1, GUILD_1_ROLES)

@pytest.fixture
def cursor() -> sqlite3.Cursor:
    database = sqlite3.connect(':memory:')
    database.row_factory = sqlite3.Row
    init(MockBot(database))

    cursor = database.cursor()
    cursor.execute('''
        INSERT INTO roles (role_id, guild_id)
        VALUES (100, 0), (101, 0), (103, 1), (999, 1)
    ''')

    return cursor

def test_get_role_by_name_unambiguous(cursor):
    role = get_role_by_name(GUILD_0, 'Blue')
    assert role.id == 102

def test_get_role_by_name_ambiguous_first(cursor):
    role = get_role_by_name(GUILD_0, 'Red')
    assert role.id == 100

def test_get_role_by_name_ambiguous_second(cursor):
    role = get_role_by_name(GUILD_1, 'Red')
    assert role.id == 103

def test_get_role_by_name_nonexistent(cursor):
    role = get_role_by_name(GUILD_0, 'Magenta')
    assert role is None

def test_get_registered_roles_first(cursor):
    roles = get_registered_roles(cursor, GUILD_0)
    assert roles == [MockRole(100, 'Red'), MockRole(101, 'Green')]

def test_get_registered_roles_second(cursor):
    roles = get_registered_roles(cursor, GUILD_1)
    assert roles == [MockRole(103, 'Red')]

def test_get_registered_roles_purge(cursor):
    select = 'SELECT * FROM roles WHERE role_id = 999'
    assert cursor.execute(select).fetchone() is not None
    get_registered_roles(cursor, GUILD_1)
    assert cursor.execute(select).fetchone() is None

def test_register_role_new(cursor):
    magenta = MockRole(106, 'Magenta')
    GUILD_0.roles.append(magenta)

    roles = get_registered_roles(cursor, GUILD_0)
    assert not any(magenta == role for role in roles)

    assert register_role(cursor, GUILD_0, magenta)

    roles = get_registered_roles(cursor, GUILD_0)
    assert any(magenta == role for role in roles)

    roles = get_registered_roles(cursor, GUILD_1)
    assert not any(magenta == role for role in roles)

def test_register_role_duplicate(cursor):
    red = MockRole(100, 'Red')

    roles = get_registered_roles(cursor, GUILD_0)
    assert any(red == role for role in roles)

    assert not register_role(cursor, GUILD_0, red)

    roles = get_registered_roles(cursor, GUILD_0)
    assert any(red == role for role in roles)

    roles = get_registered_roles(cursor, GUILD_1)
    assert not any(red == role for role in roles)
    assert any(MockRole(103, 'Red') == role for role in roles)

def test_unregister_role_existing(cursor):
    red = MockRole(100, 'Red')

    roles = get_registered_roles(cursor, GUILD_0)
    assert any(red == role for role in roles)

    assert unregister_role(cursor, GUILD_0, red.id)

    roles = get_registered_roles(cursor, GUILD_0)
    assert not any(red == role for role in roles)

    roles = get_registered_roles(cursor, GUILD_1)
    assert not any(red == role for role in roles)
    assert any(MockRole(103, 'Red') for role in roles)

def test_unregister_role_nonexistent(cursor):
    magenta = MockRole(106, 'Magenta')

    roles = get_registered_roles(cursor, GUILD_0)
    assert not any(magenta == role for role in roles)

    assert not unregister_role(cursor, GUILD_0, magenta.id)

    roles = get_registered_roles(cursor, GUILD_0)
    assert not any(magenta == role for role in roles)

    roles = get_registered_roles(cursor, GUILD_1)
    assert not any(magenta == role for role in roles)
