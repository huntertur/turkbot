The bot contains a variety of commands, which can be organized into five
distinct categories.

The `debug` command, disabled by default, is omitted from this document.

# General Utilities
- `box`: Generate a 3D box from the input text.
- `changelog`: View the bot's changelog.
- `color`: Look up a color by its value.
- `dataexport`: Export your data from the bot.
- `echo`: Repeat a message.
- `help`, `?`: Access help for the bot's commands.
- `indicators`, `i`: Create blocky emoji text.
- `invite`: Get the bot's invite link.
- `ping`: Measure the bot's latency to Discord.
- `qrcode`, `qr`: Generate a QR (Quick Response) code.
- `stats`: Display stats about the bot.
- `time`: Get the server's current time.

# Fun and Silliness
- `bird`, `tweet`: Fetch a random picture of a bird.
- `cage`: Fetch a random picture of Nicolas Cage.
- `cat`, `meow`: Fetch a random picture of a cat.
- `chuck`, `norris`: Fetch a random Chuck Norris joke.
- `dog`, `woof`: Fetch a random picture or video of a dog.
- `don`: Play a "Double-or-Nothing" game.
- `give`: Give money to another user.
- `joke`: Tell a joke.
- `money`, `$`: Check your balance.
- `murloc`: 'Translate' a message to Nerglish.
- `owo`, `uwu`: Furry-ize a message.
- `slap`: Try to slap someone silly.
- `upperlower`, `ul`: Play a simple guessing card game.
- `woke`: Show how woke your Awoken Titan Essence is.

# Game Utilities
- `emissary`: Post the current WoW emissary schedule each day.
- `hscard`, `hs`: Find Hearthstone card by name.
- `renown`: Track the current WoW Renown cap.
- `sslweapon`, `ssl`: Look up a ShellShock Live weapon.
- `wowarmory`, `wow`: Retrieve information about a WoW character.
- `wowtoken`: Query the current WoW Token price.

# Server and Moderation Utilities
- `autoassign`: Add roles to new users that join the server.
- `autoreact`: Manage the automatic reaction system.
- `avatar`, `icon`: Get the highest-resolution avatar for a user.
- `leavelog`: Log whenever someone leaves the server.
- `pinlast`: Automatically unpin the previous message after some time.
- `prefix`: Query or change the bot's command prefix in the server.
- `remind`: Send a message in a channel at certain times.
- `role`: Subscribe or unsubscribe from a role.
- `servericon`, `sicon`: Get a high-resolution version of the server icon.
- `sub`, `iam`: Subscribe to a role.
- `tag`: View and create message macros.
- `timezone`: Get or set the server's timezone.
- `unsub`, `iamnot`: Unsubscribe from a role.
- `user`, `userlookup`: Look up a Discord user in the server.
- `wowrealm`: Set the server's default WoW realm.

# Random Selection
- `5manlol`, `lolteam`: Create a League of Legends team.
- `8ball`, `8b`: Consults the Magic 8-Ball.
- `choose`: Make a decision from a list of choices.
- `coin`, `flip`: Peform a coin flip.
- `roll`, `dice`: Roll dice.
