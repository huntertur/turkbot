Want to allow the members of your server to be able to choose the color of their
name? The `role` command can be used to implement this.

# Commands and subcommands to know
 - `role add ROLE`: Allow members to join or leave ROLE.
 - `role remove ROLE`: Prevent members from joining or leaving ROLE.
 - `role list`: See what roles people can join or leave on their own.
 - `iam ROLE`: Give yourself ROLE. (Also possible as `role join` or `sub`)
 - `iamnot ROLE`: Leave ROLE. (Also possible as `role leave` or `unsub`)

# Setup

You must have the Manage Roles permission in your server in order to add or
remove self-assignable-and-leavable roles.

## Step 1: Making the color roles

Make as many roles in Discord's role editor as colors you want. Name them the
color, and make their color that color. All-lowercase names work best, since
the bot respects case when it comes to roles.

Place these roles in an appropriate place in your role hierarchy. Near the
bottom would be the best. Make sure any widespread "Member" roles don't have
colors associated with them; if they do, put your colorful roles above them.

## Step 2: Registering the roles

For every colorful role you want to make available, run `]role add ROLE`,
substituting ROLE for the color and `]` for whatever command prefix you have
in your server, if one other than the default is set.

After you are done, you can verify the registration worked by viewing the list
with `]role list`. Now would be a good time to also try assigning and removing
yourself from one of these color roles using `iam` and `iamnot` to make sure
the bot has sufficient permissions to modify roles. If the bot cannot add or
remove roles, make sure it has a role above all the roles you want to make
available in the role hierarchy.

## Step 3: Informing your members

In an announcements channel, post a message with the list of available colors.
While `role join`, `sub`, `role leave`, and `unsub` can be used in any examples
you provide, `iam` and `iamnot` are most recommended, as saying you are or are
not, say, orange, makes more sense than subscribing or unsubscribing from being
orange.
