FROM python:3.9.11-alpine3.15

WORKDIR /usr/src/app

COPY requirements.txt ./

# Needed for building some PyPI packages, like Pillow.
# Do all this in one step to keep intermediate size low.
RUN apk add --no-cache build-base zlib zlib-dev jpeg jpeg-dev linux-headers \
    freetype freetype-dev \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del build-base zlib-dev jpeg-dev linux-headers freetype-dev

COPY turkbot/ turkbot/
COPY CHANGELOG.md ./

CMD [ "python", "-m", "turkbot", "-C", "/usr/src/app/config/config.ini" ]
