# turkbot
a multipurpose Discord bot with no real goal in mind

Features are typically added if they seem like a neat idea or if the need for
it arises.

Requires Python 3.9 or newer.
[Invite Me](https://discordapp.com/oauth2/authorize?client_id=341779489614856193&scope=bot&permissions=268453952)

## Roles used

### Required

| Role          | Why |
| ------------- | --- |
| Read Messages | Required for the bot to process commands |
| Embed Links   | Required for some commands that send rich embeds |
| Send Messages | Required for commands to say their results and report certain errors |
| Add Reactions | Required for the bot to acknowledge that some commands completed or failed |

### Optional

| Role            | Why |
| --------------- | --- |
| Manage Roles    | Allow users to add or remove roles from themselves with the `role` command |
| Manage Messages | Makes commands that use reactions for controls easier to use by automatically deleting the user's reaction so they can re-react without un-reacting first and allows use of temporary pinning |

## Quick start

You can run turkbot either natively or as a container.

### Native

1. (Optional) Create a virtual environment for Python.
```sh
python3 -m venv env
. env/bin/activate
```
2. Install required packages with `pip`.
```sh
pip3 install -r requirements.txt
```
3. Copy `config_example.ini` to `config.ini`.
4. Paste your Discord API token into `config.ini` after `DiscordAPIToken = `
5. Start the bot.
```sh
python3 -m turkbot
```

If you want to use a different config file, append `-C path/to/config.ini`.

### Container

1. Build the container image
```sh
docker build . -t turkbot
```
2. Create the `config/` directory
3. Copy `config.ini` to `config/`
4. Prepend `config/` to the values of `DatabaseFilename` and `GamesSaveFilename`
   in `config/config.ini`
5. Paste your Discord API token into `config.ini` after `DiscordAPIToken = `
6. Run the container image
```sh
docker run -v $(pwd)/config:/usr/src/app/config -p 8080:8080 turkbot
```

## Testing

Unit tests are run natively instead of in a container.

1. (Optional) Create a virtual environment
```sh
python3 -m venv env
. env/bin/activate
```
2. Install runtime and unit testing dependencies
```sh
pip3 install -r requirements.txt
pip3 install -r requirements-test.txt
```
3. (Optional) Provide a dummy `config.ini` file if there isn't a real one
```sh
echo '[General]' > config.ini
```
4. Run unit tests
```sh
python3 -m pytest -vv
```
